// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package main

import (
	"context"
	"crypto/tls"
	"log"
	"net/http"
	"time"

	"github.com/jessevdk/go-flags"
	_ "github.com/lib/pq"
	"go.uber.org/zap"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/common/cmd"
	"go.nlx.io/nlx/common/logoptions"
	"go.nlx.io/nlx/common/process"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/transactionlog"
	"go.nlx.io/nlx/common/version"
	controllerapi "go.nlx.io/nlx/controller/ports/internalrest/api/server"
	api "go.nlx.io/nlx/manager/ports/int/rest/api/server"
	"go.nlx.io/nlx/outway"
	restconfig "go.nlx.io/nlx/outway/adapters/config/rest"
	restcontroller "go.nlx.io/nlx/outway/adapters/controller/rest"
	txlogapi "go.nlx.io/nlx/txlog-api/ports/rest/api/server"
)

var options struct {
	Name                        string `long:"name" env:"OUTWAY_NAME" description:"Name of the outway. Every outway should have a unique name within the organization." required:"true"`
	GroupID                     string `long:"group-id" env:"GROUP_ID" description:"Group ID of the FSC Group" required:"true"`
	ManagerInternalAddress      string `long:"manager-internal-address" env:"MANAGER_INTERNAL_ADDRESS" description:"Manager address to communicate with for dealing with contracts." required:"true"`
	ListenAddress               string `long:"listen-address" env:"LISTEN_ADDRESS" default:"127.0.0.1:8080" description:"Address for the outway to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	ListenHTTPS                 bool   `long:"listen-https" env:"LISTEN_HTTPS" description:"Enable HTTPS on the ListenAddress" required:"false"`
	MonitoringAddress           string `long:"monitoring-address" env:"MONITORING_ADDRESS" default:"127.0.0.1:8081" description:"Address for the outway monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs."`
	ControllerAPIAddress        string `long:"controller-api-address" env:"CONTROLLER_API_ADDRESS" description:"The address of the Controller API" required:"true"`
	TxLogAPIAddress             string `long:"tx-log-api-address" env:"TX_LOG_API_ADDRESS" description:"The address of the transaction log API" required:"true"`
	AuthorizationServiceAddress string `long:"authorization-service-address" env:"AUTHORIZATION_SERVICE_ADDRESS" description:"Address of the authorization service. If set calls will go through the authorization service before being send to the inway"`
	AuthorizationCA             string `long:"authorization-root-ca" env:"AUTHORIZATION_ROOT_CA" description:"absolute path to root CA used to verify auth service certificate"`
	ServerCertFile              string `long:"tls-server-cert" env:"TLS_SERVER_CERT" description:"Path to a cert .pem, used for the HTTPS server" required:"false"`
	ServerKeyFile               string `long:"tls-server-key" env:"TLS_SERVER_KEY" description:"Path the a key .pem, used for the HTTPS server" required:"false"`

	logoptions.LogOptions
	cmd.TLSGroupOptions
	cmd.TLSOptions
}

func main() {
	parseOptions()

	p := process.NewProcess()

	// Setup new zap logger
	logger, err := options.LogOptions.ZapConfig().Build()
	if err != nil {
		log.Fatalf("failed to create new zap logger: %v", err)
	}

	logger.Info("version info", zap.String("version", version.BuildVersion), zap.String("source-hash", version.BuildSourceHash))
	logger = logger.With(zap.String("version", version.BuildVersion))

	if errValidate := common_tls.VerifyPrivateKeyPermissions(options.GroupKeyFile); errValidate != nil {
		logger.Warn("invalid organization key permissions", zap.Error(errValidate), zap.String("file-path", options.GroupCertFile))
	}

	if errValidate := common_tls.VerifyPrivateKeyPermissions(options.KeyFile); errValidate != nil {
		logger.Warn("invalid internal PKI key permissions", zap.Error(errValidate), zap.String("file-path", options.KeyFile))
	}

	groupCert, err := common_tls.NewBundleFromFiles(options.GroupCertFile, options.GroupKeyFile, options.GroupRootCert)
	if err != nil {
		logger.Fatal("unable to load organization certificate and key", zap.Error(err))
	}

	internalCert, err := common_tls.NewBundleFromFiles(options.CertFile, options.KeyFile, options.RootCertFile)
	if err != nil {
		logger.Fatal("unable to load internal PKI certificate and key", zap.Error(err))
	}

	txlogClient, err := txlogapi.NewClientWithResponses(options.TxLogAPIAddress, func(c *txlogapi.Client) error {
		t := http.DefaultTransport.(*http.Transport).Clone()
		t.TLSClientConfig = internalCert.TLSConfig()

		c.Client = &http.Client{Transport: t}
		return nil
	})
	if err != nil {
		logger.Fatal("could not create txlog client", zap.Error(err))
	}

	txLogger, err := transactionlog.NewRestAPITransactionLogger(&transactionlog.NewRestAPITransactionLoggerArgs{
		Logger: logger,
		Client: txlogClient,
	})
	if err != nil {
		logger.Fatal("unable to setup the transaction logger", zap.Error(err))
	}

	var serverCertificate *tls.Certificate

	if options.ListenHTTPS {
		if options.ServerCertFile == "" || options.ServerKeyFile == "" {
			logger.Fatal("server certificate and key are required")
		}

		cert, certErr := tls.LoadX509KeyPair(options.ServerCertFile, options.ServerKeyFile)
		if certErr != nil {
			logger.Fatal("failed to load server certificate", zap.Error(err))
		}

		serverCertificate = &cert
	}

	controllerClient, err := controllerapi.NewClientWithResponses(options.ControllerAPIAddress, func(c *controllerapi.Client) error {
		t := http.DefaultTransport.(*http.Transport).Clone()
		t.TLSClientConfig = internalCert.TLSConfig()

		c.Client = &http.Client{Transport: t}
		return nil
	})
	if err != nil {
		logger.Fatal("could not create rest controller client", zap.Error(err))
	}

	restController, err := restcontroller.New(controllerClient)
	if err != nil {
		logger.Fatal("could not create rest controller", zap.Error(err))
	}

	managerInternalClient, err := api.NewClientWithResponses(options.ManagerInternalAddress, func(c *api.Client) error {
		t := http.DefaultTransport.(*http.Transport).Clone()
		t.TLSClientConfig = internalCert.TLSConfig()

		c.Client = &http.Client{Transport: t}

		return nil
	})
	if err != nil {
		logger.Fatal("could not create manager internal client", zap.Error(err))
	}

	restConfigRepository, err := restconfig.New(context.Background(), managerInternalClient, groupCert.CertificateThumbprint())
	if err != nil {
		logger.Fatal("failed to setup manager rest config repository", zap.Error(err))
	}

	ow, err := outway.New(&outway.NewOutwayArgs{
		Clock:             clock.New(),
		GroupID:           options.GroupID,
		Name:              options.Name,
		Ctx:               context.Background(),
		Logger:            logger,
		Txlogger:          txLogger,
		Controller:        restController,
		MonitoringAddress: options.MonitoringAddress,
		ExternalCert:      groupCert,
		InternalCert:      internalCert,
		ConfigRepository:  restConfigRepository,
		AuthServiceURL:    options.AuthorizationServiceAddress,
		AuthCAPath:        options.AuthorizationCA,
	})
	if err != nil {
		logger.Fatal("failed to initialize the outway", zap.Error(err))
	}

	ctxAnnouncementsCancel, cancelAnnouncements := context.WithCancel(context.Background())
	go func() {
		err = ow.Run(ctxAnnouncementsCancel)
		if err != nil {
			logger.Fatal("error running outway", zap.Error(err))
		}

		err = ow.RunServer(options.ListenAddress, serverCertificate)
		if err != nil {
			logger.Fatal("error running outway server", zap.Error(err))
		}
	}()

	p.Wait()

	logger.Info("starting graceful shutdown")
	cancelAnnouncements()

	gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
	defer cancel()

	ow.Shutdown(gracefulCtx)

	err = restConfigRepository.Close()
	if err != nil {
		logger.Error("could not close config repository", zap.Error(err))
	}
}

func parseOptions() {
	args, err := flags.Parse(&options)
	if err != nil {
		if et, ok := err.(*flags.Error); ok {
			if et.Type == flags.ErrHelp {
				return
			}
		}

		log.Fatalf("error parsing flags: %v", err)
	}

	if len(args) > 0 {
		log.Fatalf("unexpected arguments: %v", args)
	}
}
