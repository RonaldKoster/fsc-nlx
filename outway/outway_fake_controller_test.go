// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package outway_test

import (
	"context"

	"go.nlx.io/nlx/outway/adapters/controller"
)

type fakeController struct{}

func newFakeController() *fakeController {
	return &fakeController{}
}

func (m *fakeController) RegisterOutway(_ context.Context, _ *controller.RegisterOutwayArgs) error {
	return nil
}
