// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package outway

import (
	"context"

	"go.uber.org/zap"
)

func (o *Outway) Shutdown(ctx context.Context) {
	o.logger.Debug("shutting down")

	o.monitorService.SetNotReady()

	err := o.httpServer.Shutdown(ctx)
	if err != nil {
		o.logger.Error("error shutting down server", zap.Error(err))
	}

	err = o.monitorService.Stop()
	if err != nil {
		o.logger.Error("error shutting down monitoring service", zap.Error(err))
	}
}
