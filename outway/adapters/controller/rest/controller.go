// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restcontroller

import (
	"context"
	"fmt"
	"net/http"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/ports/internalrest/api/models"
	api "go.nlx.io/nlx/controller/ports/internalrest/api/server"
	"go.nlx.io/nlx/outway/adapters/controller"
)

type restController struct {
	controller api.ClientWithResponsesInterface
}

func New(client api.ClientWithResponsesInterface) (*restController, error) {
	if client == nil {
		return nil, fmt.Errorf("client is required")
	}

	return &restController{
		controller: client,
	}, nil
}

func (m *restController) RegisterOutway(ctx context.Context, args *controller.RegisterOutwayArgs) error {
	res, err := m.controller.RegisterOutwayWithResponse(ctx, args.GroupID, args.Name, models.RegisterOutwayJSONRequestBody{
		CertificateThumbprint: args.CertificateThumbprint,
	})
	if err != nil {
		return errors.Wrap(err, "could not register outway in rest controller")
	}

	if res.StatusCode() != http.StatusNoContent {
		return fmt.Errorf("could not register outway in rest controller, received invalid status code %d: %s", res.StatusCode(), string(res.Body))
	}

	return nil
}
