// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restconfig

import (
	"context"
	"fmt"
	"net/http"
	"time"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/common/accesstoken"
	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
	"go.nlx.io/nlx/outway/domain/config"
)

func (r *Repository) GetServices(ctx context.Context) (config.Services, error) {
	res, err := r.managerClient.GetOutwayServicesWithResponse(ctx, r.externalCertThumbprint)
	if err != nil {
		return nil, errors.Wrap(err, "could not get outway services from manager")
	}

	if res.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("could not get outway services, received invalid status code %d: %s", res.StatusCode(), string(res.Body))
	}

	services := make(config.Services)
	for grantHash, s := range res.JSON200.Services {
		services[grantHash] = &config.Service{
			PeerID: s.PeerId,
			Name:   s.Name,
		}
	}

	return services, nil
}

func (r *Repository) GetToken(ctx context.Context, grantHash string) (*config.TokenInfo, error) {
	res, err := r.managerClient.GetTokenWithResponse(ctx, &models.GetTokenParams{
		GrantHash:                   grantHash,
		OutwayCertificateThumbprint: r.externalCertThumbprint,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not get token from manager")
	}

	if res.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("could not get token, received invalid status code %d: %s", res.StatusCode(), string(res.Body))
	}

	return &config.TokenInfo{
		Token: res.JSON200.Token,
		UnsafeDecodedToken: &accesstoken.UnsafeDecodedToken{
			GroupID:                     res.JSON200.TokenInfo.GroupId,
			GrantHash:                   res.JSON200.TokenInfo.GrantHash,
			OutwayPeerID:                res.JSON200.TokenInfo.OutwayPeerId,
			OutwayDelegatorPeerID:       res.JSON200.TokenInfo.OutwayDelegatorPeerId,
			OutwayCertificateThumbprint: res.JSON200.TokenInfo.OutwayCertificateThumbprint,
			ServiceName:                 res.JSON200.TokenInfo.ServiceName,
			ServiceInwayAddress:         res.JSON200.TokenInfo.ServiceInwayAddress,
			ServicePeerID:               res.JSON200.TokenInfo.ServicePeerId,
			ServiceDelegatorPeerID:      res.JSON200.TokenInfo.ServiceDelegatorPeerId,
			ExpiryDate:                  time.Unix(res.JSON200.TokenInfo.ExpiryDate, 0),
		},
	}, nil
}
