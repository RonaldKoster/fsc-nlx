/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package outway_test

import (
	"context"
	"log"
	"net/http/httptest"
	"path/filepath"
	"testing"
	"time"

	"go.uber.org/zap"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/transactionlog"
	"go.nlx.io/nlx/outway"
	"go.nlx.io/nlx/outway/domain/config"
	"go.nlx.io/nlx/testing/testingutils"
)

var (
	orgACertBundle *tls.CertificateBundle
	orgBCertBundle *tls.CertificateBundle
	orgCCertBundle *tls.CertificateBundle
	testClock      = clock.NewMock(time.Date(2023, 11, 6, 14, 10, 5, 0, time.UTC))
)

func TestMain(m *testing.M) {
	var err error

	orgACertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestOrgA)
	if err != nil {
		log.Fatal(err)
	}

	orgBCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestOrgB)
	if err != nil {
		log.Fatal(err)
	}

	orgCCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestOrgC)
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

func newOutway(repository config.Repository, transactionLogger transactionlog.TransactionLogger) *httptest.Server {
	if transactionLogger == nil {
		transactionLogger = newFakeTransactionLogger()
	}

	if repository == nil {
		repository = newFakeEmptyRepository()
	}

	controller := newFakeController()

	outwayArgs := &outway.NewOutwayArgs{
		Clock:             testClock,
		Name:              "testOutway",
		GroupID:           "fsc-local",
		Ctx:               context.Background(),
		Logger:            zap.NewNop(),
		Txlogger:          transactionLogger,
		Controller:        controller,
		ConfigRepository:  repository,
		MonitoringAddress: "localhost:8080",
		ExternalCert:      orgACertBundle,
		InternalCert:      orgACertBundle,
	}

	testOutway, err := outway.New(outwayArgs)
	if err != nil {
		log.Fatal("cannot create outway from args")
	}

	srv := httptest.NewUnstartedServer(testOutway)
	srv.Start()

	return srv
}
