// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package transactionlog

import "context"

type DiscardTransactionLogger struct{}

func NewDiscardTransactionLogger() TransactionLogger {
	return &DiscardTransactionLogger{}
}

func (txl *DiscardTransactionLogger) AddRecords(_ context.Context, _ []*Record) error {
	return nil
}

func (txl *DiscardTransactionLogger) Close() error {
	return nil
}
