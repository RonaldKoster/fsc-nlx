// Copyright © VNG Realisatie 2020
// Licensed under the EUPL
package tls

import (
	"crypto/x509"
	"fmt"
	"os"
	"path/filepath"

	"github.com/pkg/errors"
)

func NewCertPoolFromFile(filePath string) (*x509.CertPool, error) {
	pems, err := os.ReadFile(filepath.Clean(filePath))
	if err != nil {
		return nil, errors.Wrap(err, "failed to read certificate file")
	}

	return NewCertPool(pems)
}

func NewCertPool(pems []byte) (*x509.CertPool, error) {
	p := x509.NewCertPool()

	ok := p.AppendCertsFromPEM(pems)
	if !ok {
		return nil, fmt.Errorf("could not append certs from PEM")
	}

	return p, nil
}
