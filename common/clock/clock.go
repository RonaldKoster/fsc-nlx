// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package clock

import "time"

type Clock interface {
	Now() time.Time
}

type MockClock struct {
	t time.Time
}

// NewMock creates a mock clock that can be used in tests
func NewMock(mockTime time.Time) *MockClock {
	return &MockClock{t: mockTime}
}

// Now will always return fixed time that's passed into the NewMock function
func (c *MockClock) Now() time.Time {
	return c.t
}

type RealClock struct{}

// New returns a real clock
func New() *RealClock {
	return &RealClock{}
}

// Now will return the current time
func (c *RealClock) Now() time.Time {
	return time.Now()
}
