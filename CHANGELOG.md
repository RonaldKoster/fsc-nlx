## [0.13.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.12.0...v0.13.0) (2023-12-21)


### Build System

* **controller:** add internal REST API to Earthly configuration + regenerate ([8eb8f64](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8eb8f646253da3e063c8c073b2b18caf1afb66bf)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **docs:** upgrade React to v18 ([65eb0ee](https://gitlab.com/commonground/nlx/fsc-nlx/commit/65eb0ee210c98032e3ea7358224910b48763ee17)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **docs:** use non privileged nginx image ([9d27f0f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9d27f0f031dcfdb90b22a5ab23d740dd321745cc)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)
* regenerate REST API models using OpenAPI generator v2 ([5e69a54](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5e69a54b36cbeac5c9591934a7b9df0e6939d8be)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* tidy and update Go dependencies ([8a37890](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8a37890689a843f729c13babde193969e2f67dd4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* tidy Go module ([ec05047](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ec050471a18e1589ac7c5c7ad4b7effc7733918f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update alpine docker tag to v3.19.0 ([b7013bd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b7013bd95abd29c97a882daaeb5f62eed18a4296)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @axe-core/webdriverjs to v4.8.2 ([9457246](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9457246535bfd027b6ad89feae5cdf7858d16af8)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @cucumber/cucumber to v10 ([8f2d125](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8f2d125206c331ae81ae95750870aafcfb7ecbef)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @cucumber/html-formatter to v21 ([feda082](https://gitlab.com/commonground/nlx/fsc-nlx/commit/feda08278c5232cb2e0650010f1cc0d387599288)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @cucumber/html-formatter to v21.1.0 ([e4bcfe6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e4bcfe6efbafe18082f27f592197f95edde03344)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @swc/core to v1.3.101 ([b0a1cdb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b0a1cdb278d913bd62fd8f552b1124455c049a43)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v18.19.0 ([a1a0264](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a1a0264f78ed94c2fedb4f34775775ddd66bb2b1)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v18.19.1 ([2d5f3d0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2d5f3d063d53e4e1f7536149707e0de077a0b2af)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v20.10.3 ([21b8142](https://gitlab.com/commonground/nlx/fsc-nlx/commit/21b8142af1c58ae320bab094b57bd04d037b7644)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v20.10.4 ([5706816](https://gitlab.com/commonground/nlx/fsc-nlx/commit/570681640eed5be3829a53dbae89d745c4628f9b)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v20.10.5 ([c225c03](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c225c038d7722af9fdd906357d5a302398cb474c)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint to v8.55.0 ([4f7d4e0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4f7d4e0632d87ce7b3e17f720a772f29a6adc39d)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint to v8.56.0 ([9e74f61](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9e74f61e2eab01c93803c4fa0f6e180350f510ec)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-config-prettier to v9.1.0 ([2b80c31](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2b80c31f67a8b123f2e86bccec1c3eb216148f6f)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-plugin-import to v2.29.1 ([53bfd54](https://gitlab.com/commonground/nlx/fsc-nlx/commit/53bfd541d72c744fed3d3af99139aaf8bbb799bb)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-plugin-prettier to v5.1.0 ([4f19fad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4f19fad65076b33702ba0603b2bb51392d3b5c74)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency prettier to v3.1.1 ([3b17dd3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3b17dd3e55a4bcadb6cb7956aaf308da55acdf01)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency selenium-webdriver to v4.16.0 ([5415d96](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5415d9609330fdbf7b0d1145f5992685262858fc)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency semantic-release to v22.0.10 ([fbcac82](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fbcac821cf2608eb0a207ec415c3d307b88decd1)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency semantic-release to v22.0.12 ([fe60fc8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fe60fc89356e3a9396310f8e316f7d651cc4805b)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency semantic-release to v22.0.9 ([2b4b8ab](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2b4b8ab8bce070564aeeb0a7189101bec963a2c3)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency ts-node to v10.9.2 ([9b2184b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9b2184babb51ff381f2159a3a225a3e3fcb94961)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency typescript to v5.3.3 ([d2de7f6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d2de7f6b979f0e33ecb0f46895772d694a90b9e2)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update docker.io/curlimages/curl docker tag to v8.5.0 ([196e560](https://gitlab.com/commonground/nlx/fsc-nlx/commit/196e56075dea7446f7666602c50a726ff293fffa)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update docusaurus monorepo to v3 ([56023d6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/56023d6647c9a34998b497888f88433c8e081d2a)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update Go dependencies ([efab1f4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/efab1f4417433df566b97c85c94148d08ffe7ff6)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update golang docker tag to v1.21.5 ([883a393](https://gitlab.com/commonground/nlx/fsc-nlx/commit/883a393b7a624ee59983d173de2399727a2e5731)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to aacd6d4 ([a1150a4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a1150a45fd8b831d135aee66ca0bd86a3cc5ce0d)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to dc181d7 ([98f45c5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/98f45c510e958703b143048067040d0b2f441c57)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to f3f8817 ([f4584b3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f4584b327e3dd4359752d52dd7972f1822a10d07)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update Helm version for Semantic Release ([afb2b0d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/afb2b0d4f306228019c6ff6bff129bc44bdd4dea)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update module github.com/coreos/go-oidc/v3 to v3.9.0 ([cb37e4f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cb37e4fe1df0853f75ba7f908dafe49aa8f8b4ed)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/data-dog/go-txdb to v0.1.8 ([0fdc11b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0fdc11b4f36c65147f2dd5c153d768c100e8d3d8)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/go-chi/chi/v5 to v5.0.11 ([fbd0ab8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fbd0ab8142018e9e60202050860dccf038e95747)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/golang-jwt/jwt/v5 to v5.2.0 ([be75275](https://gitlab.com/commonground/nlx/fsc-nlx/commit/be752752318bd341caddda0d60a0746a174f16ac)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/golang-migrate/migrate/v4 to v4.17.0 ([5c88a8a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5c88a8ada13d31f25ec2ab5d22d4766235931bb1)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/google/uuid to v1.5.0 ([6c729f6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6c729f677a08c3ee71fc3a8eba4ee8eadad7d09f)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/lestrrat-go/jwx to v1.2.27 ([477d387](https://gitlab.com/commonground/nlx/fsc-nlx/commit/477d3873bdfef802a391a983373906f8e3dbc811)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/lestrrat-go/jwx to v2 ([f033cc9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f033cc9e5e74f4e63b90bbd1fa09cd54a3bc6890)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module golang.org/x/crypto to v0.17.0 ([bfc1ca9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bfc1ca9447e96edf117e44c6e0805039b08801f1)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update node.js to v20.10.0 ([bd3cac3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bd3cac37ec172acb3179bed831a4120a50a75f38)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update openpolicyagent/opa docker tag to v0.59.0 ([71b6e0f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/71b6e0fa0c22285e483f5faab790295dd664f1ee)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update openpolicyagent/opa docker tag to v0.60.0 ([027d6f6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/027d6f6e93a3a7895e1f58238173407da503fd30)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update postgres docker tag to v16 ([de68a37](https://gitlab.com/commonground/nlx/fsc-nlx/commit/de68a372d25b54c913e47874aa7cad6ad72e15ff)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update selenium/standalone-chrome docker tag to v4.15.0 ([9feafc6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9feafc651bf58348d3d9b13110970406031adbd3)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update selenium/standalone-chrome docker tag to v4.16.0 ([f8671b6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f8671b6afacd2e00b2809d3504bef46bc5242ca5)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update selenium/standalone-chrome docker tag to v4.16.1 ([d8edecb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d8edecb4359cc8c2760ad345806a92f93a8bce09)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update semantic-release monorepo ([64f9b88](https://gitlab.com/commonground/nlx/fsc-nlx/commit/64f9b88e43a20452cf2be7787256b479b9d03bcf)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update typescript-eslint monorepo to v6.13.1 ([fd0d740](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fd0d74086ce345586cc758fb9bb9b6bd885e2d40)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update typescript-eslint monorepo to v6.13.2 ([d8362cc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d8362cc4e64197405c0a3081ad33b5c206f614ef)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update typescript-eslint monorepo to v6.14.0 ([1f26a79](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f26a797c977d17914c4ea1c32e48c6d69b4dd37)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update typescript-eslint monorepo to v6.15.0 ([b4301b2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b4301b29f7d1bc2953bbc6757dcf80ea232cc4d0)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)


### Continuous Integration

* **directory:** introduce /health endpoint for the liveness and readyness probe ([07b2c2e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/07b2c2e7f07be0497820f1b4762f1e0abf7b3586)), closes [fsc-nlx#98](https://gitlab.com/commonground/fsc-nlx/issues/98)
* **directory:** introduce startup probe for the UI ([d5fec50](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d5fec50cdd80d316186a723fabb48e7b6d5f8ee9)), closes [fsc-nlx#98](https://gitlab.com/commonground/fsc-nlx/issues/98)
* enable ci jobs for external merge requests from forks ([0e536f2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0e536f283c2b4ebdda602ce8438ca9bac78d9cd3)), closes [fsc-nlx#134](https://gitlab.com/commonground/fsc-nlx/issues/134)
* remove tx-log-api from the Directory deployment ([d4e2a1b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d4e2a1b59c4eb107cf5ecbd78674d5063f73025a)), closes [fsc-nlx#133](https://gitlab.com/commonground/fsc-nlx/issues/133)
* remove unused tags ([45dbf34](https://gitlab.com/commonground/nlx/fsc-nlx/commit/45dbf34c909624eda79cec551328fd367eeea02e)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)
* run review deployment for external merge requests of forks ([b486004](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b4860046dcaf29d2c1bd17ab4d69ce3ebb628d6f)), closes [fsc-nlx#134](https://gitlab.com/commonground/fsc-nlx/issues/134)
* update pipeline to use the kubernetes based gitlab runners ([cd1865d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cd1865d69de609a4b9c1026a1def1e6a01873f70)), closes [fsc-nlx#140](https://gitlab.com/commonground/fsc-nlx/issues/140)


### Documentation

* **helm:** add fsc prefix to install commands in the readme's ([3075e64](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3075e640e9d72dd68b89edf2dc525f24c6092585)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** update txlogAPIAddress flag description in the Manager chart ([59ef524](https://gitlab.com/commonground/nlx/fsc-nlx/commit/59ef5246b751c4e081ff9283aa269c82be202430)), closes [fsc-nlx#133](https://gitlab.com/commonground/fsc-nlx/issues/133)
* update issue template for user stories ([415af2f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/415af2f90634fd67f6440a90a4ca5f59ccaf29c1)), closes [fsc-nlx#118](https://gitlab.com/commonground/fsc-nlx/issues/118)


### Features

* add `delete_service` call to controller's REST API ([5e340b9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5e340b9237c4be5519c919d85aae5aae1735cd31))
* add `get_services` call to controller's REST API ([a49144e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a49144ecb19c155a2186d927731ee7ba40a1a37f))
* add `update_service` call to controller's REST API ([dde732e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dde732e8e1e2c7deaf4a4471f09a0eca1e0b2e33))
* add health endpoints to controller and manager ([35b0f8e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/35b0f8ed80b9f753320ede3b7049c52dfd8ee113)), closes [fsc-nlx#99](https://gitlab.com/commonground/fsc-nlx/issues/99)
* add internal unauthenticated jwks endpoint ([7203a56](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7203a56478470696d34bd481082226ecdea087d8)), closes [fsc-nlx#42](https://gitlab.com/commonground/fsc-nlx/issues/42)
* **controller:** display Outway names besides the Certificate Thumbprint ([52e8d42](https://gitlab.com/commonground/nlx/fsc-nlx/commit/52e8d429f4b0e936b4db730e9914b1b760592455)), closes [fsc-nlx#88](https://gitlab.com/commonground/fsc-nlx/issues/88)
* **controller:** log error when the Add Contract page fails to render ([801b1f5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/801b1f504883435f119118307d2d9952334b55b0)), closes [fsc-nlx#88](https://gitlab.com/commonground/fsc-nlx/issues/88)
* **controller:** show 100 latest transaction logs in descending order ([aaf20df](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aaf20dfe4a76d72be917ea1ff79e06db3fb76e8a)), closes [fsc-nlx#129](https://gitlab.com/commonground/fsc-nlx/issues/129)
* **directory:** remove environment toggle from the UI ([d837c05](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d837c0533cf1c1a843c8095391aa88753c3c1493)), closes [fsc-nlx#143](https://gitlab.com/commonground/fsc-nlx/issues/143)
* **manager:** add logging when retrieving a certificate for a Peer fails ([77c704f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/77c704f464d560f7f5a0c31134db35481a9aa3ba)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** enable optional txlog-api when deploying as a directory ([0a75efa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0a75efaad1b79667fb1f8507366403baca162884)), closes [fsc-nlx#133](https://gitlab.com/commonground/fsc-nlx/issues/133)
* **manager:** rename transaction_id filter to transaction_ids ([c38e806](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c38e80628f5efaa656570bebe9090fc21592c9fd)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **manager:** return error if string to decode is empty ([0f7843d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0f7843dab5c0d1c55d040bfa12263cf1ce977e3a)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **manager:** surround Peer ID with quotes in error messages ([6f70a43](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6f70a430d118ebc78d9ced4de33b436c36f2e863)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** update logging endpoints OAS and implementation according to the Standard ([13c068c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/13c068c1a9f01eda4c673363101ea0d2564cc27c)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* remove PeerRegistration Grant ([fb05cb0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fb05cb06bfa7691becc29abec3baf2eee2ac450d)), closes [fsc-nlx#131](https://gitlab.com/commonground/fsc-nlx/issues/131)
* **txlog:** rename transaction_id filter to transaction_ids ([f85de34](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f85de344dff40a1eb9391b576b09aa477a818df4)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** update OAS and implementation to be consistent with the standard ([39cb5d2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/39cb5d2340b45d53b06f8a1b7c9fb72a61d82c40)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)


### Bug Fixes

* added missing logging errors to Inway and Outway ([32714e7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/32714e78bb4f8297ce51833288c4bd8ab7db63dc)), closes [fsc-nlx#122](https://gitlab.com/commonground/fsc-nlx/issues/122)
* error http status codes Inway and Outway ([4c6abb9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4c6abb9e42e6e2a1d07d2efb0a8eee43130bbd60)), closes [fsc-nlx#123](https://gitlab.com/commonground/fsc-nlx/issues/123)
* **inway:** add error code as HTTP response header ([4fb25a0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/4fb25a02864e411f83fee3a84e9101a3cb6bada3)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **inway:** add missing http error TRANSACTION_LOG_WRITE_ERROR ([c7d9ab4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c7d9ab4c8d744df88d8203aef9d0036fceac62be)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **inway:** always use authorization plugin ([2f369d6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2f369d6e2696eb608d6ddc80699c241d7892b354)), closes [fsc-nlx#42](https://gitlab.com/commonground/fsc-nlx/issues/42)
* **manager:** ensure list of peers is stable sorted ([11577c1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/11577c121e324f35d29aaff156088948e5b11da4)), closes [fsc-nlx#131](https://gitlab.com/commonground/fsc-nlx/issues/131)
* **manager:** get peers default limit and tests ([75b6ddc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/75b6ddc73f053424847d5188f0802f79dfa2fcb3)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **txlog:** get_logs transactionID filter must ignore other filters ([04bc854](https://gitlab.com/commonground/nlx/fsc-nlx/commit/04bc8545cffdd383f6325e56f081c330d4b337fc)), closes [fsc-nlx#127](https://gitlab.com/commonground/fsc-nlx/issues/127)
* **txlog:** prevent panic when retrieving logs with a filter by Transaction ID ([6fd9bdf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6fd9bdf982446b2730a20a365d2aa7eca5015532)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)


### Code Refactoring

* add group ID to register Inway and Outway call ([bf29994](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bf2999402c11b9030e0e715557a8a1284d5b3c48)), closes [fsc-nlx#103](https://gitlab.com/commonground/fsc-nlx/issues/103)
* **manager:** introduce fetchLimit property per handler ([e4a9502](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e4a950212c2d4bf8154048eed1ec9b52d84b8e9d)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **manager:** replace transaction log stub with check if transaction log is disabled ([f5efe58](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f5efe586add0f7b61ec3ceebcd8b5586e6b03c83)), closes [fsc-nlx#133](https://gitlab.com/commonground/fsc-nlx/issues/133)
* **manager:** use interface as return type for the fake txlog constructor ([95d1eff](https://gitlab.com/commonground/nlx/fsc-nlx/commit/95d1eff0684a9169cdcccec5b67cc009fbc307c8)), closes [fsc-nlx#135](https://gitlab.com/commonground/fsc-nlx/issues/135)
* merge getServiceEndpointURL and getInwayAddressForService into one api call ([0792954](https://gitlab.com/commonground/nlx/fsc-nlx/commit/07929540bafe218905e914927f3b5e48f879991a)), closes [fsc-nlx#103](https://gitlab.com/commonground/fsc-nlx/issues/103)
* remove deprecated getCertificates internal manager api call ([fd196cd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fd196cd3ead93d7fc837df6a491f99c34bccd7ee)), closes [fsc-nlx#42](https://gitlab.com/commonground/fsc-nlx/issues/42)
* use protocol enum instead of string and add protocol to manager client ([1942534](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1942534b171deef72989db00459d9c871135b02b)), closes [fsc-nlx#139](https://gitlab.com/commonground/fsc-nlx/issues/139)


### Tests

* **inway:** add transactionlog and delegation test cases to Inway tests ([45837d3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/45837d3d6960915dc6c30a40bfef69f615830e8b)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **manager:** provided missing tests from testsuite in manager ([2f7822e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2f7822e08fd76cf804ea30dd0ea60f9c4c7cc508)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **manager:** replace query tests with port tests for the internal manager ([b470162](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b470162173ddf98fecdb136bb2698df2795cd268)), closes [fsc-nlx#135](https://gitlab.com/commonground/fsc-nlx/issues/135)
* **outway:** added tests for the Outway ([aa998d4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/aa998d406a0aa69d762b46a544b1f70767dc6b13)), closes [fsc-nlx#110](https://gitlab.com/commonground/fsc-nlx/issues/110)
* **txlog:** add test for the Create Records usecase ([d64d87d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d64d87d85c699837f40f17c02d08c5614a382d07)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** add test for the List Records for a Delegated Service ([0dedd2b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0dedd2b35a361aaebb805375ec70377889973fc6)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** add test for the List Records for a request on behalf of another Peer ([c8ce624](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c8ce62448dc3e7f6c241ec05aae1ab76054cc8ed)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** add test for the List Records usecase ([b25d6c5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b25d6c5f13055fb5ea72d6836457ae4b912622b8)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** fix flaky test ([07f8963](https://gitlab.com/commonground/nlx/fsc-nlx/commit/07f8963d69af965bd6445497ac4adbacc6702f18)), closes [fsc-nlx#135](https://gitlab.com/commonground/fsc-nlx/issues/135)
* **txlog:** remove redundant test ([81c41ff](https://gitlab.com/commonground/nlx/fsc-nlx/commit/81c41ff9d79c85a83f4e995f0f859883573938a6)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** remove redundant test ([57118f1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/57118f158a90aab8dc47f0a96ad03377d03882f6)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)
* **txlog:** use unique transaction ids per test ([ba175aa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ba175aab993c125e52dc02741f84d6e076af3e93)), closes [fsc-nlx#113](https://gitlab.com/commonground/fsc-nlx/issues/113)

## [0.12.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.11.0...v0.12.0) (2023-12-01)


### Build System

* add the Earthly Helm generate docs target to the all target ([80b4a77](https://gitlab.com/commonground/nlx/fsc-nlx/commit/80b4a775de597c5c995d2eed37b6aa51f8ac1521)), closes [fsc-nlx#120](https://gitlab.com/commonground/fsc-nlx/issues/120)
* pin dependencies ([9f96083](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9f96083ce58578a31db38a2eb8066fde52fe2d32)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update alpine docker tag to v3.18.4 ([33bca9c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/33bca9cb5d1e8bff82d3aa0758c2e5faf2bcaa64)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update alpine docker tag to v3.18.5 ([0a1cced](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0a1ccedaa960b25c871a99e5645f96fa8a7f816a)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update commitlint monorepo to v18.4.3 ([f201dc9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f201dc9d361533c3bb92fdb5e8b09aa5a7e748e2)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @axe-core/webdriverjs to v4.8.1 ([b251968](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b25196844850f0607982d8817092e49a771d24de)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @cucumber/cucumber to v9.6.0 ([dfd0f3f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dfd0f3fc82ef0bcd962304419c9e18d2bc08605a)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @swc/core to v1.3.100 ([f20212c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f20212c2f0e1732be669da41afad44f71e23bc0b)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @swc/core to v1.3.99 ([d7eddb4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d7eddb4236d65c8a61cf0ea87607e199bd8af9c2)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v18.18.13 ([a7356ea](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a7356ea0c83e69ad49210dd41921ff3648e88e75)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency @types/node to v18.18.14 ([0d2b6a2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0d2b6a2ff5326d3d0a2fd6966dea945d20283fb3)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency dayjs to v1.11.10 ([35a84de](https://gitlab.com/commonground/nlx/fsc-nlx/commit/35a84de935089a71bd049b3d4755390e40fe273f)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint to v8.54.0 ([6e73fc6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6e73fc629b6baeffa9268f380f1fd28f60330ff9)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-plugin-import to v2.29.0 ([20a2a0b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/20a2a0b2a2ba85fa582925b4ea36071f9d372fbf)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency eslint-plugin-prettier to v5.0.1 ([93da4bd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/93da4bd06c5c189839576d854b605b6467b78a16)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency htmx.org to v1.9.9 ([3d5b19c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3d5b19c8dc672f600d6e46ef5ee3ccb4fbdb6bc8)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency hyperscript.org to v0.9.12 ([250cc46](https://gitlab.com/commonground/nlx/fsc-nlx/commit/250cc4611a4fe97e77cc1430d76eb1f21079c39a)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency prettier to v3.1.0 ([f4fa792](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f4fa7922567cadbc654198238a40b5767b4b3a34)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency replace-in-file to v7.0.2 ([cdb31e8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cdb31e82b27cb28864df26b55d351a61a9b8b659)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency selenium-webdriver to v4.15.0 ([930a243](https://gitlab.com/commonground/nlx/fsc-nlx/commit/930a243533c209800b30df6cf977b9d5db6fe6c5)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dependency typescript to v5.3.2 ([3826197](https://gitlab.com/commonground/nlx/fsc-nlx/commit/38261976b70e568f335253646861b97603dac731)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update dexidp/dex docker tag to v2.37.0 ([fceff52](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fceff52e9fa45332f1a5b80535aee9956f11b298)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update docker docker tag to v24.0.7 ([e53e7c4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e53e7c4ebc28e045eaad2b51237aa193ac0bfef8)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update docker.io/curlimages/curl docker tag to v8.4.0 ([ac78117](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ac78117bb171fe3213cc1a2eed7be8b1f0c5d2dd)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update docusaurus monorepo to v2.4.3 ([3ac416c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3ac416c34cb0a797b22dc9661557b6d07dc639b4)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang docker tag to v1.21.4 ([c279e1d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c279e1d3d562b23b2a11a8eb0f66ec073fad78ee)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to 6522937 ([b418aaf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b418aaf81792121c9d523ba16b91227f89cf6e4e)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update golang.org/x/exp digest to 9a3e603 ([101b354](https://gitlab.com/commonground/nlx/fsc-nlx/commit/101b3549bf4c7b37d2ded831710733136545558c)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update helm release basisregister-fictieve-kentekens to v0.1.1 ([36b9333](https://gitlab.com/commonground/nlx/fsc-nlx/commit/36b93330eb1308f7ab47f6188e5e311328eb4ed4)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/coreos/go-oidc/v3 to v3.8.0 ([42bb6cc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/42bb6cc4e3c252f450ac0d71d34549f048cb8d34)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/getkin/kin-openapi to v0.122.0 ([19faa9f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/19faa9f475d6ee252ec2c7a2ef459cf12bb6cbbd)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/golang-jwt/jwt/v5 to v5.1.0 ([6b93c55](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6b93c553c94ec89dcbc43bfd8d702ceb62dc7822)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/google/uuid to v1.4.0 ([c1f89cb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c1f89cbf78f716e0a66267213c19fc455c0fdbda)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/gorilla/securecookie to v1.1.2 ([dd8ffec](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dd8ffec494fb1e1319f8feebff2eb775f3bdbe92)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/gorilla/sessions to v1.2.2 ([7d59d47](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7d59d47b7460f7059b08febcb64af381b1178e97)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/oapi-codegen/runtime to v1.1.0 ([fbe3a17](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fbe3a17f6b937dcf4619f717a631df31542a7338)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module github.com/spf13/cobra to v1.8.0 ([5f5cd76](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5f5cd768f57093e3336464890b460c768d23f601)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module golang.org/x/net to v0.19.0 ([5b0cf97](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5b0cf97573cf2da64767b8a5b6a80a537361fd4a)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module golang.org/x/oauth2 to v0.15.0 ([b420c2d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b420c2d537aa437adbfdf2ef04a6025ec684a364)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update module golang.org/x/sync to v0.5.0 ([34dc1d3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/34dc1d30e927db4a1ab1ec64dda933147923059b)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update openpolicyagent/opa docker tag to v0.58.0 ([1f2668d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f2668d80e916ad3215ec3e536fdaf948edfd926)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update parcel monorepo to v2.10.3 ([43dbf45](https://gitlab.com/commonground/nlx/fsc-nlx/commit/43dbf45a6de1e24f41528a58711348d236a10091)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)
* update postgres docker tag to v15.5 ([5451f1c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5451f1cc6e84c6ab6608550efe1ed7703cfba54c)), closes [nlx#1171](https://gitlab.com/commonground/nlx/issues/1171)


### Continuous Integration

* enable Container Scanning using Trivy ([0ea6a5c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0ea6a5c3973e6bb6bcfcb63c6cd280a15987e37e)), closes [fsc-nlx#105](https://gitlab.com/commonground/fsc-nlx/issues/105)
* update container scanning rule so it is enabled again ([721989d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/721989d6087eb004ea17c7c9ace7ce99b58a7db6)), closes [fsc-nlx#105](https://gitlab.com/commonground/fsc-nlx/issues/105)


### Features

* add 'Bearer' scheme to Fsc-Authorization header ([6dfb126](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6dfb1263c7487295034b37907b2fe51f201f9908)), closes [fsc-nlx#49](https://gitlab.com/commonground/fsc-nlx/issues/49)
* **directory:** enable serving the Directory UI from a custom URL Path ([a9a7c77](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a9a7c77c146cdfc8588813a8d48b2974bb3abba2)), closes [fsc-nlx#120](https://gitlab.com/commonground/fsc-nlx/issues/120)
* **helm:** add serviceProxyCacheSize setting to the Inway chart ([7780c31](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7780c319f42f4f2b927ea289d369ce575004f305)), closes [fsc-nlx#107](https://gitlab.com/commonground/fsc-nlx/issues/107)
* **inway:** ensure requests from the Inway to the Service URL contain at least a root slash ([3446344](https://gitlab.com/commonground/nlx/fsc-nlx/commit/34463446d798bbe2078a58227e3553cc94ab86b6)), closes [/www.rfc-editor.org/rfc/rfc2616#section-3](https://gitlab.com/commonground//www.rfc-editor.org/rfc/rfc2616/issues/section-3) [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **manager:** add specific algorithm errors for hash and signature ([8fb686e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8fb686ea3d90bb65d713e2ae8db630adb0dc1766)), closes [fsc-nlx#97](https://gitlab.com/commonground/fsc-nlx/issues/97)
* **outway,inway,manager,controller:** respect HTTP_PROXY and HTTPS_PROXY environment variables ([7761832](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7761832528ef6b72252e0adbea76644feb3dc7d4)), closes [fsc-nlx#121](https://gitlab.com/commonground/fsc-nlx/issues/121)


### Bug Fixes

* **controller:** prevent resetting form fields when adding a new Grant to a contract ([22214b8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/22214b819b1e7bd2e87bb994cb6cb26b969adbda)), closes [fsc-nlx#111](https://gitlab.com/commonground/fsc-nlx/issues/111)
* **inway:** reuse connections to Services ([fda9ccd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fda9ccd4241951665a56a1e2f8ea0987ef5ceea6)), closes [fsc-nlx#107](https://gitlab.com/commonground/fsc-nlx/issues/107)
* permissions for OPA auth containers ([114845c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/114845c3cec6a7319cc28789a2f27933d004cb00)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Code Refactoring

* **inway:** rename MockRepository -> fakeConfigRepository ([68da028](https://gitlab.com/commonground/nlx/fsc-nlx/commit/68da0289ba49be1e83698512f50efd2c5e77b561)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** rework creating mock config repository to improve readability ([825ccd7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/825ccd7823251926696f96254a319a340e1ab09f)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **manager:** use globally available organization certificates ([ebee2ef](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ebee2ef195cc3a10ffd6edfc17e02c53ee3831e1)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **outway:** rename context to requestContext ([fc9f5bf](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fc9f5bf4e95da5085975a90f1f6431f782024d6a)), closes [fsc-nlx#107](https://gitlab.com/commonground/fsc-nlx/issues/107)


### Styles

* **manager:** improve formatting ([8966c3b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8966c3b8c21d2820a9de7f27dd7462e20e5ec347)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)


### Tests

* **inway:** add assertion for the response body if the expected status code does not match ([1e25b7b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1e25b7b3bc4b305c503351bbfd44c8e50878b75a)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** enable instantiating test Inway with a fake ConfigRepository ([9cd0c2f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9cd0c2f404b0cebaf4620a03e6b502fd224d75f9)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** enable passing custom cert for the Inway -> Outway connection ([f7b965b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f7b965bc1997cb325383335c6ac9b1181cb40c04)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** extract fake implementations into separate files ([0cdbefe](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0cdbefe4335cefb1091bd397669d2b35245c460a)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** move transaction log assertions to the act phase ([1e3d9f0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1e3d9f0156ffdd315f2b775a59f03701cee84763)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** rename fscTransactionID -> validFSCTransactionID to reveal its intent ([378f6cc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/378f6ccafbc2574762c467a5caf0684d270a1fa3)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** rename variables to improve readability ([e224542](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e22454266966d6b08c0ab5e15864f7f34c13350d)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **inway:** replace mockService with serviceEndpointURL ([adcbece](https://gitlab.com/commonground/nlx/fsc-nlx/commit/adcbecee734f0183b7583470b684210a9f8e39c5)), closes [fsc-nlx#112](https://gitlab.com/commonground/fsc-nlx/issues/112)
* **manager:** add test ids from FSC Test Suite to corresponding tests ([f12f0be](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f12f0bec6facb02c4d05915fc96a6246324b0dca)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)

## [0.11.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.10.0...v0.11.0) (2023-11-23)


### Features

* **helm:** add env var to inway helm chart ([cbd27fa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cbd27fa055059be28942111dc28e26b86e297463)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)

## [0.10.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.9.0...v0.10.0) (2023-11-23)


### Features

* **helm:** add env var to outway helm chart ([ef55169](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ef5516948f0e41dccf9d09e05d757f64e6b89b3d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Bug Fixes

* **manager:** return error details according to FSC for test scenario Manager-RejectContract 1 - 6 ([5fbb5cd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5fbb5cd82b8e77737ab9c3fa022560a5cee5b91d)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** return error details according to FSC for test scenario Manager-RevokeContract 1 - 6 ([1b6facb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1b6facbd04d2e76b48725128ef20f0b77143d7b9)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* permissions for testing key material more permissive ([e1cc8a9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e1cc8a9f2ef5383655ed690b1725aff0ccbac28c)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Tests

* **manager:** add logging for failing assertions ([67047d3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/67047d380cf0bc753c68a507f222a762035c4d2b)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)

## [0.9.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.8.0...v0.9.0) (2023-11-22)


### Continuous Integration

* make the redeploy to ACC job independent of the previous jobs ([2828b45](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2828b45ef21ce05840590ea12687eea4c98455a3)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Documentation

* corrected dns name in outway internal certificate kubernetes manifest ([35a8b40](https://gitlab.com/commonground/nlx/fsc-nlx/commit/35a8b40dec3ec04d7c8056767df531afe69d2796)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Features

* add rest api to tx log api and let inway outway and manager use rest api ([42c2e6a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/42c2e6a40dfa4f2f3e5b98b03bccaa1bc431bf9c)), closes [fsc-nlx#91](https://gitlab.com/commonground/fsc-nlx/issues/91)
* add rest controller adapter to inway, outway and manager ([31b868d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/31b868dccea3290fdf21d6e482f830a9f950836a)), closes [fsc-nlx#90](https://gitlab.com/commonground/fsc-nlx/issues/90)
* **controller:** add internal rest port ([91b0b0a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/91b0b0a8cc5a03d884316fd03db5d32ed04a04ea)), closes [fsc-nlx#90](https://gitlab.com/commonground/fsc-nlx/issues/90)


### Bug Fixes

* **helm:** rename inconsistent volume name for the internal certificate of the Outway ([b9b25df](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b9b25df3a8ca7ff2cb68e41264fcfdf319765bf8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **manager:** error responses of external manager conforms FSC ([756b290](https://gitlab.com/commonground/nlx/fsc-nlx/commit/756b290f32ab5e2f894a9c46eeb732ef2782d4ea)), closes [fsc-nlx#73](https://gitlab.com/commonground/fsc-nlx/issues/73)
* **manager:** return error details according to FSC for test scenario Manager-AcceptContract-2 ([7e2fbfb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7e2fbfb9f75ff52b3dfb2eeefba3277c853c264a)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** return error details according to FSC for test scenario Manager-AcceptContract-3 ([1b455d9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1b455d904fd50349534a316a23b8a7586523ee80)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** return error details according to FSC for test scenario Manager-AcceptContract-4 ([9b966a5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9b966a5921feee23ad5187c5c5603d047aaa2c7d)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** return error details according to FSC for test scenario Manager-AcceptContract-5 ([9e8ee5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9e8ee5e2b72c78edde2a9aa1d78914d5811e6db2)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** return error details according to FSC for test scenario Manager-AcceptContract-6 ([ac5dc17](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ac5dc177ef11161523c58b7b133b9cb49e640860)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** use correct error type for type casting error ([354b2d0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/354b2d0c68b8d580984c9a1156cb95ece6ca7029)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Code Refactoring

* **manager:** reduce complexity for the generation of test hashes ([821ea62](https://gitlab.com/commonground/nlx/fsc-nlx/commit/821ea6293804444fc12ff51aede33a198b490c48)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** remove unused code ([9db5c6a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9db5c6ad5fee088ffe9da430535865a9bfcb28cb)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** reuse app errors for error handling ([1ccaf6d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1ccaf6d82eaa67d54eb5bfc02a06d07104bc7162)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* remove grpc related files and adapters ([6ae883c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6ae883cc6e294803267e8311cc697fe5708b0045)), closes [fsc-nlx#90](https://gitlab.com/commonground/fsc-nlx/issues/90)
* remove grpc txlog server and adapters ([7763ba3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7763ba30fc82d4b98ad2b3cfbf58ae360a762362)), closes [fsc-nlx#91](https://gitlab.com/commonground/fsc-nlx/issues/91)


### Tests

* **manager:** add tests for the AcceptContract behaviour ([bcd3adc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bcd3adc675e2fcdb6928f36f49bac8f4363244db)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** pass the actual contract content hash when signing a contract ([e022b62](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e022b62b7233a8dfdc5780bcfea876d8aa4d3701)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** replace custom struct with command arguments to create internal contract ([b796792](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b796792073049cf66b346fa5ac06202aa992bb87)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** replace custom struct with JSON model struct to approve contract ([2039b67](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2039b67a05d434201eaa4e49651ef375290f9cb1)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)

## [0.8.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.7.1...v0.8.0) (2023-11-16)


### Build System

* **manager:** add updated FSC OAS with protocol for Service Publication Grants ([17f323e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/17f323e6d4ae7a63a47253153b0b5bf6d33fca64)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** regenerate external Manager models based on updated OAS ([1f17dc0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1f17dc0a2a190e638980e91df33f0ef2854de51d)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* remove obsolete admin config from modd ([b3e85ec](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b3e85ec29abcc4b7f5dfb2983d782501eed03118)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* tidy Go mod file ([71beed2](https://gitlab.com/commonground/nlx/fsc-nlx/commit/71beed2c6cbbd0ff3bfffd597cee5398a14afbf8)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Continuous Integration

* enable Go mod tidy test if Go files change ([bc7e955](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bc7e955b478df1a6d981f41392968785c0935557)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **helm:** add default security context to charts ([c9bf3dd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c9bf3dde800be113fdbabc82011b5413b874142d)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* remove deployments of the preprod and prod environments ([0ff1214](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0ff1214be2a09199a3ebdabde904f1cf08765738)), closes [fsc-nlx#86](https://gitlab.com/commonground/fsc-nlx/issues/86)
* remove directory chart ([92b2b6c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/92b2b6c603f930b50239de00b6b9474d1774753f)), closes [fsc-nlx#86](https://gitlab.com/commonground/fsc-nlx/issues/86)
* remove directory chart from helm update script ([46c7afc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/46c7afc6b53b7800364eb104ed1ca2f93d6adbc4)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Documentation

* add GroupID to the helm value files used in try nlx guide ([0b85af4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0b85af4163071fbf340f78460290e84aa4f0c40c)), closes [fsc-nlx#87](https://gitlab.com/commonground/fsc-nlx/issues/87)


### Features

* **controller:** simplfy service endpoint url validation ([9b5920b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9b5920b4665534ceec3e2ab52e428ddd0d4aad31)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **directory:** add preprod and prod environments ([19648a9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/19648a9daae6d05213427777c7f933db67e6e594)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* **directory:** only support the demo environment ([f1db784](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f1db7848196150befc0ecacce7e890b972624257)), closes [fsc-nlx#86](https://gitlab.com/commonground/fsc-nlx/issues/86)
* implement protocol in service publication grants ([7af2c7d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7af2c7dd23995932077c2a9310c3b01f8cbc8c03)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **inway:** add specific error response for expired tokens ([a944f25](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a944f25371d64c1d88b5317574e8ca1faf3ecae9)), closes [fsc-nlx#93](https://gitlab.com/commonground/fsc-nlx/issues/93)
* **manager:** add rest json internal port ([47ab3c7](https://gitlab.com/commonground/nlx/fsc-nlx/commit/47ab3c7863aeb137c9712f63049d5ac9bf8e6cf2)), closes [fsc-nlx#62](https://gitlab.com/commonground/fsc-nlx/issues/62)
* **manager:** implement errors for the token endpoint ([16aa062](https://gitlab.com/commonground/nlx/fsc-nlx/commit/16aa0621a7dd99bdea4b6fa8543581e33a761002)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** implement protocol in delegated service publication grant ([62eaa79](https://gitlab.com/commonground/nlx/fsc-nlx/commit/62eaa796174fde05b49abf14efab2750ea47f9e7)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** reorder fields in the delegated service connection grant ([b21685f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b21685fbaabdac192506f9b111b28666d4b3a936)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Content Hash algorithm ([e9c9784](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e9c9784c611f8f8bb2a9007b267275e0fb4654b2)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Delegated Service Connection Hash algorithm ([7b0d5b3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7b0d5b3408a87e4642c64be2005ae246dc039c02)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Delegated Service Publication Hash algorithm ([35248b3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/35248b33186a94ab3ff43bf1e6de627ca59b7877)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Peer Registration Hash algorithm ([401f967](https://gitlab.com/commonground/nlx/fsc-nlx/commit/401f96795bbc7e203cf1b0e8dc0aced7248c8572)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Service Connection Hash algorithm ([eea050a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/eea050a79d105ee97696363caf8f270e3157c99a)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rework the implementation of the Service Publication Hash algorithm ([1d20a8a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1d20a8ac8fb13fae3fdbfb5c04361b866d87e207)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** support protocol property for the Delegated Service Publication Grant ([8b61a73](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8b61a735db9dd49fb0b170933481a9438cbc3289)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** support protocol property for the Service Publication Grant ([315f11a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/315f11ae2369a2729acd9ff8fa51d19fff057bae)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)


### Bug Fixes

* **inway:** invalid Fsc-Transaction-Id header returned the wrong error ([08596f5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/08596f514928cd993dc6c06db0044f2541180e36)), closes [fsc-nlx#93](https://gitlab.com/commonground/fsc-nlx/issues/93)


### Code Refactoring

* **inway:** aligned bootstrap with manager for easier testing ([f9bfb82](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f9bfb82e3ce109e81999e90d939a99570e432d7f)), closes [fsc-nlx#93](https://gitlab.com/commonground/fsc-nlx/issues/93)
* **manager:** move the protocol field to the service object ([2bde72a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2bde72ac2211e00d1166d4e5c2e75a23effce06d)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** move the protocol field to the service object ([863c419](https://gitlab.com/commonground/nlx/fsc-nlx/commit/863c41989a9dd8070db5dadc2d249c0dd1f6c14f)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** remove redundant log ([cfb49e4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cfb49e43d586d3825cdf6fc8fc70f8d4a078cbbd)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** rename hash test files ([f4256d5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f4256d5db241670defceb051113f914587931f8d)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** use value method of protocol domain model ([24180d4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/24180d4278ace94fcf92bbc9d1f5ad36ba7ff6a6)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* remove admin tool, not needed anymore ([3a6ac2a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3a6ac2ac8c79049c391b26aa3cf9b8cc19e8a043)), closes [fsc-nlx#62](https://gitlab.com/commonground/fsc-nlx/issues/62)
* remove grpc adapters and ports ([e924afd](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e924afd41034355b072d2acb92dbdb07204bcf37)), closes [fsc-nlx#62](https://gitlab.com/commonground/fsc-nlx/issues/62)
* use manager rest endpoint in inway, outway and controller ([5f41b28](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5f41b286b8d74b95d2acd609d7573700e241239e)), closes [fsc-nlx#62](https://gitlab.com/commonground/fsc-nlx/issues/62)


### Tests

* **inway:** added Inway test suite to go tests ([dd6645b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dd6645b0d5be54873f87ad61a087468b083c3265)), closes [fsc-nlx#93](https://gitlab.com/commonground/fsc-nlx/issues/93)
* **manager:** add test suite tests for token endpoint ([74f0aaa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/74f0aaa574774ea092cea80127bf9240571e39d3)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** annotate get peer info test with testsuite test id ([7554847](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7554847de0ab12e9816584f2c6de6be02498eee7)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** fix integration tests using the missing protocol property ([3e5440a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3e5440a6f96b4cae19b3a3e75acba47ffb82981b)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)
* **manager:** pass protocol when using (delegated) Service Publication Grants ([32bca5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/32bca5e7c36d0290da21b9d77ac49c14de45873d)), closes [fsc-nlx#89](https://gitlab.com/commonground/fsc-nlx/issues/89)

## [0.7.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.7.0...v0.7.1) (2023-11-06)


### Continuous Integration

* add group id to the values for the preprod and prod deploments ([3d5fca3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3d5fca3a1a3d1085eaffa368a7d7c2720dfb5e1a)), closes [fsc-nlx#27](https://gitlab.com/commonground/fsc-nlx/issues/27)

## [0.7.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.6.1...v0.7.0) (2023-11-06)


### Build System

* **manager:** add HTTP status codes from standard into the core OAS ([a691bb4](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a691bb459f425a2f79d01711686d8d6599e9bb6c)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* **manager:** regenerate REST server from the FSC Core OAS ([cb4ea6f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cb4ea6f3e11135295127a45e91c58553de210181)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* regenerate rest server from core OAS ([7cecb7a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7cecb7af59a368d7ecdfc0b7caea44c24d1c1d0d)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* upgrade Semantic Release & Commitlint packages ([dad6577](https://gitlab.com/commonground/nlx/fsc-nlx/commit/dad6577b7a4ccebf58bc1d661865a4f34d4cd2cb)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Continuous Integration

* add release rule for every allowed commit type ([b7590e6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b7590e63d04c8d9106d92549dc8440f6d0fc607f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* explicitly include every commit type in our CHANGELOG ([003f438](https://gitlab.com/commonground/nlx/fsc-nlx/commit/003f438bd5a41e2828c73fa8a8994c3dfcd570df)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)
* update node version ([f8e9331](https://gitlab.com/commonground/nlx/fsc-nlx/commit/f8e9331496475947fac3827ed0734ef21f7a886f)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Documentation

* reword release strategy to be more specific ([a30af9c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a30af9c9e663269f810a04bb1c756e67857e3dcc)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Features

* **controller:** added validation checks when submitting service name ([e5eaa47](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e5eaa474cd31c265cd53d7c95561827b3b17b1e8)), closes [fsc-nlx#71](https://gitlab.com/commonground/fsc-nlx/issues/71)
* **controller:** peer id can be selected with a datalist ([8ecbe76](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8ecbe7696ab7dfd6135fabf488604bc3e47be501))
* **controller:** show confirmation dialog before signing a contract ([b9a5cea](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b9a5cea5ebecb0eaef59cc4c756364057d7ff811)), closes [fsc-nlx#67](https://gitlab.com/commonground/fsc-nlx/issues/67)
* **controller:** use group id domain model instead of string ([ba7c419](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ba7c4197a3dd5fff56556663233bbda687b72757)), closes [fsc-nlx#80](https://gitlab.com/commonground/fsc-nlx/issues/80)
* **manager:** return error code and domain according to FSC for Manager-SubmitContract-2 ([6dde658](https://gitlab.com/commonground/nlx/fsc-nlx/commit/6dde658f9940e74bccb31d28f66ed299f0e62af0)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* **manager:** rework the SubmitContract endpoint to return 422 with the corresponding error code ([e1923e0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/e1923e022a4aab93dcde2a2a01daba2e73995b77)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* **manager:** use domain model to pass the group ID to the external app ([d589fad](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d589fad8f411b9487aed4c55b40a8aac13c95702)), closes [fsc-nlx#80](https://gitlab.com/commonground/fsc-nlx/issues/80)


### Bug Fixes

* **manager:** add required group id to internal request to the txlog api ([1dbb278](https://gitlab.com/commonground/nlx/fsc-nlx/commit/1dbb2789591eb6c60b1465ed687952b7bf49b936)), closes [fsc-nlx#80](https://gitlab.com/commonground/fsc-nlx/issues/80)
* **manager:** ensure decoded access tokens use UTC instead of local timezone ([3da621b](https://gitlab.com/commonground/nlx/fsc-nlx/commit/3da621b843529be4811fd33d6f1d9f95aa3ddcd6)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)


### Code Refactoring

* **common:** remove unused error Location code 'M1' ([54a8d7a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/54a8d7a404c32f1de31cd0ab86553f50499deaa6)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* **controller:** move the resource ID of the contract to the URL ([d4d7de9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d4d7de955c9173c769e9e3d7ecb2c689883675c1)), closes [fsc-nlx#67](https://gitlab.com/commonground/fsc-nlx/issues/67)
* **controller:** remove unused modal styling ([b34b243](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b34b2434f46ef724fde8b23c1814d1410a39accf)), closes [fsc-nlx#67](https://gitlab.com/commonground/fsc-nlx/issues/67)
* remove redundant println statements ([0082907](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0082907887401b693e5da5634c07d88cf4dfab0e)), closes [fsc-nlx#32](https://gitlab.com/commonground/fsc-nlx/issues/32)


### Styles

* **controller:** fix typo's in logs ([fde6cfa](https://gitlab.com/commonground/nlx/fsc-nlx/commit/fde6cfab6df657f48359b6eefe81a783b4bdf0b9)), closes [fsc-nlx#67](https://gitlab.com/commonground/fsc-nlx/issues/67)
* **manager:** cleanup structs ([c00d52a](https://gitlab.com/commonground/nlx/fsc-nlx/commit/c00d52ac0263986aec2cf53715340e62362fc383)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)


### Tests

* **manager:** implement tests from the FSC test suite for submitting an invalid contract ([066c7be](https://gitlab.com/commonground/nlx/fsc-nlx/commit/066c7be6cfcd3c7b19119f8184eafb4fafe7a368)), closes [fsc-nlx#72](https://gitlab.com/commonground/fsc-nlx/issues/72)
* **manager:** move declared variable inline ([2739b49](https://gitlab.com/commonground/nlx/fsc-nlx/commit/2739b496caac4e91e08568c166a0ab2ca6bb1911)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)
* **manager:** rename expected -> want to be consistent with other tests ([a11191d](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a11191d1f0695adb5beb4b1774ac3600cf4c1866)), closes [fsc-nlx#77](https://gitlab.com/commonground/fsc-nlx/issues/77)

## [0.6.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.6.0...v0.6.1) (2023-10-30)

# [0.6.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.5.0...v0.6.0) (2023-10-27)


### Bug Fixes

* **controller:** add dummy avatar to the user menu ([23c3f59](https://gitlab.com/commonground/nlx/fsc-nlx/commit/23c3f5964dfc77059509ca24d7bcf29768267b27)), closes [fsc-nlx#64](https://gitlab.com/fsc-nlx/issues/64)
* **manager:** pass the Group ID to the Controller gRPC adapter ([9338557](https://gitlab.com/commonground/nlx/fsc-nlx/commit/93385575e65f4789728bc682dc5a53f21200e080)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **manager:** realigned OAS with FSC standard ([0eca77f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/0eca77fd395efa9d8cb13d5c9cb09997ae02b8e9)), closes [fsc-nlx#61](https://gitlab.com/fsc-nlx/issues/61)
* **outway:** prevent stripping the X-NLX-Authorization header ([008f699](https://gitlab.com/commonground/nlx/fsc-nlx/commit/008f699ea1102db410db476c20915bcc8e5696c5)), closes [fsc-nlx#32](https://gitlab.com/fsc-nlx/issues/32)


### Features

* **controller:** add check for required flags on startup ([50de330](https://gitlab.com/commonground/nlx/fsc-nlx/commit/50de3303162e8a837d18990b3a4a9b5a275cfa4f)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **controller:** increase with of Outway Certificate Thumbprint field ([ef265df](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ef265dfe0a61660ae86dd4801030232c236e872d)), closes [fsc-nlx#68](https://gitlab.com/fsc-nlx/issues/68)
* **controller:** remove default value for GroupID ([d56ad40](https://gitlab.com/commonground/nlx/fsc-nlx/commit/d56ad40924fce12752fc6841b026072d03971dec)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **controller:** remove unused Settings button from the primary navigation ([508fce3](https://gitlab.com/commonground/nlx/fsc-nlx/commit/508fce3ff7a6f1a582caa835498e3bb847346e08)), closes [fsc-nlx#65](https://gitlab.com/fsc-nlx/issues/65)
* **controller:** rename flag manager-address to manager-address-internal ([708b9a0](https://gitlab.com/commonground/nlx/fsc-nlx/commit/708b9a043f23d17d4ba526b41d786b15ea55e2f1)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **controller:** replace select with datalist element for Outway Cert Thumbprint ([172f6c1](https://gitlab.com/commonground/nlx/fsc-nlx/commit/172f6c106a668451f9f436522042dfddbe80865b)), closes [fsc-nlx#68](https://gitlab.com/fsc-nlx/issues/68)
* **controller:** replace select with datalist element for Outway Cert Thumbprint ([a661121](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a661121b6cc2a7df44575b21715929914d106cc3)), closes [fsc-nlx#68](https://gitlab.com/fsc-nlx/issues/68)
* **controller:** replace the Contract modal to a Contract detail page ([610bd4e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/610bd4ef342d4dd7a1d14d1d02a835a3880e6bce)), closes [fsc-nlx#66](https://gitlab.com/fsc-nlx/issues/66)
* **controller:** use debug as default log level ([84ca50f](https://gitlab.com/commonground/nlx/fsc-nlx/commit/84ca50f17464b75cb898fdf2038abce54182f008)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)
* **manager:** add FSC Logging extension version to the /get_peer_info endpoint ([674689e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/674689ece181e8eacd42795b3b82c0382747198e)), closes [fsc-nlx#53](https://gitlab.com/fsc-nlx/issues/53)
* **manager:** add Peer name filter to external /peers endpoint ([ffa8edb](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ffa8edbf8ba7c647e762043395683ae5b48bef86)), closes [nlx#54](https://gitlab.com/nlx/issues/54)
* **manager:** add support for other algorithms in the /get_jwks endpoint ([a0afc17](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a0afc17ef9098608014ca07569fd7c9614cc6c63)), closes [fsc-nlx#55](https://gitlab.com/fsc-nlx/issues/55)
* use base64URL encoding without padding for Contract and Grant hashes ([cf37765](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cf3776570032f0ceb6c6a771787b9ee556c83403)), closes [fsc-nlx#27](https://gitlab.com/fsc-nlx/issues/27)

# [0.5.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.5...v0.5.0) (2023-10-11)


### Bug Fixes

* update outway and inway entries on database conflict ([9a1c1e8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/9a1c1e83529ff3987e9f41cc3263034c45e2641a)), closes [fsc-nlx#40](https://gitlab.com/fsc-nlx/issues/40)


### Features

* **docs:** use introduction page as entry page ([bd00a1c](https://gitlab.com/commonground/nlx/fsc-nlx/commit/bd00a1cb73932b34d073d68e07e2195acdb75522)), closes [fsc-nlx#20](https://gitlab.com/fsc-nlx/issues/20)
* **manager:** add logging on error when fetching services from directory ([99dcff8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/99dcff857634dabc433c1e8ecef7685866b99e65)), closes [fsc-nlx#9](https://gitlab.com/fsc-nlx/issues/9)

## [0.4.5](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.4...v0.4.5) (2023-10-03)


### Bug Fixes

* **controller:** add AuthData to service-add-handler ([8cc4f90](https://gitlab.com/commonground/nlx/fsc-nlx/commit/8cc4f906e665b41632fbaaefbf325bd5cf88c2c4)), closes [#32](https://gitlab.com/commonground/nlx/fsc-nlx/issues/32)

## [0.4.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.3...v0.4.4) (2023-10-02)


### Bug Fixes

* **docs:** use correct commonname for txlog api certificate ([a39db53](https://gitlab.com/commonground/nlx/fsc-nlx/commit/a39db53461062feeea252aba3f39ae13d2bd6c85)), closes [fsc-nlx#35](https://gitlab.com/fsc-nlx/issues/35)

## [0.4.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.2...v0.4.3) (2023-09-29)


### Bug Fixes

* **controller:** pass authentication data when adding a new contract ([b7cbcf6](https://gitlab.com/commonground/nlx/fsc-nlx/commit/b7cbcf63373d9f5c403b5224323f321c723ef963)), closes [fsc-nlx#32](https://gitlab.com/fsc-nlx/issues/32)

## [0.4.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.1...v0.4.2) (2023-09-28)

## [0.4.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.4.0...v0.4.1) (2023-09-28)

# [0.4.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.3.0...v0.4.0) (2023-09-28)


### Features

* **outway:** improve logging ([ed0a913](https://gitlab.com/commonground/nlx/fsc-nlx/commit/ed0a91308414fe858b45a829abd2e593927666fd)), closes [fsc-nlx#32](https://gitlab.com/fsc-nlx/issues/32)

# [0.3.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.7...v0.3.0) (2023-09-27)


### Features

* **controller:** add oidc authentication mechanism ([efad656](https://gitlab.com/commonground/nlx/fsc-nlx/commit/efad6563b05a6c03bac4bd7905911e93b5f6bb8d)), closes [fsc-nlx#1795](https://gitlab.com/fsc-nlx/issues/1795)

## [0.2.7](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.6...v0.2.7) (2023-09-27)

## [0.2.6](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.5...v0.2.6) (2023-09-27)

## [0.2.5](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.4...v0.2.5) (2023-09-27)

## [0.2.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.3...v0.2.4) (2023-09-26)

## [0.2.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.2...v0.2.3) (2023-09-26)

## [0.2.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.1...v0.2.2) (2023-09-15)

## [0.2.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.2.0...v0.2.1) (2023-09-15)

# [0.2.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.1.1...v0.2.0) (2023-09-14)


### Features

* **directory:** remove preprod and prod options from the Directory UI filter list ([86e36e5](https://gitlab.com/commonground/nlx/fsc-nlx/commit/86e36e5bf24651503ff9cfe4091932fb88622799)), closes [fsc-nlx#24](https://gitlab.com/fsc-nlx/issues/24)
* **directory:** update NLX content to reflect FSC NLX ([82d4a5e](https://gitlab.com/commonground/nlx/fsc-nlx/commit/82d4a5e2ad0c4bf6eb56b42e62d73b86586df033)), closes [fsc-nlx#24](https://gitlab.com/fsc-nlx/issues/24)

## [0.1.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.1.0...v0.1.1) (2023-09-13)

# [0.1.0](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.5...v0.1.0) (2023-09-12)


### Features

* rename listen-address to listen-address-ui in controller ([5faf247](https://gitlab.com/commonground/nlx/fsc-nlx/commit/5faf247dcaa39b2ca5d1ac0d2e227e1475b96882)), closes [fsc-nlx#12](https://gitlab.com/fsc-nlx/issues/12)


### Reverts

* update node.js to v20.6.0 ([803abd9](https://gitlab.com/commonground/nlx/fsc-nlx/commit/803abd9c9ab9283f63438e196c0d95b8f87c9c5b)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)

## [0.0.5](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.4...v0.0.5) (2023-09-08)


### Bug Fixes

* **helm:** add fsc- prefix to hostnames for the Demo deployment ([7f718a8](https://gitlab.com/commonground/nlx/fsc-nlx/commit/7f718a8297887655aee726b976844d5effda758a)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)
* **manager:** show message if Directory Peer Manager address is not passed to Manager ([13756fc](https://gitlab.com/commonground/nlx/fsc-nlx/commit/13756fc37ac462f47d9ede167b5a13f1dda911de)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)
* **manager:** show message if Self Address is not passed to Manager ([cbb6693](https://gitlab.com/commonground/nlx/fsc-nlx/commit/cbb6693557b7f6c49322cdc85b3ae00a3f756ff5)), closes [nlx#1818](https://gitlab.com/nlx/issues/1818)

## [0.0.4](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.3...v0.0.4) (2023-09-07)

## [0.0.3](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.2...v0.0.3) (2023-09-07)

## [0.0.2](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.1...v0.0.2) (2023-09-07)

## [0.0.1](https://gitlab.com/commonground/nlx/fsc-nlx/compare/v0.0.0...v0.0.1) (2023-09-07)
