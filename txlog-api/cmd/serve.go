// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package cmd

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/spf13/cobra"

	"go.nlx.io/nlx/common/cmd"
	zaplogger "go.nlx.io/nlx/common/logger/zap"
	"go.nlx.io/nlx/common/logoptions"
	"go.nlx.io/nlx/common/monitoring"
	"go.nlx.io/nlx/common/process"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/version"
	postgresadapter "go.nlx.io/nlx/txlog-api/adapters/storage/postgres"
	"go.nlx.io/nlx/txlog-api/ports/rest"
	"go.nlx.io/nlx/txlog-api/service"
)

var serveOpts struct {
	ListenAddress      string
	MonitoringAddress  string
	StoragePostgresDSN string

	logoptions.LogOptions
	cmd.TLSOptions
}

type clock struct{}

func (c *clock) Now() time.Time {
	return time.Now()
}

//nolint:gochecknoinits,funlen,gocyclo // this is the recommended way to use cobra, also a lot of flags..
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddress, "listen-address", "", "127.0.0.1:8443", "Address for the api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.MonitoringAddress, "monitoring-address", "", "127.0.0.1:8081", "Address for the txlog monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.StoragePostgresDSN, "storage-postgres-dsn", "", "", "Postgres Connection URL")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "", "Set loglevel")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.RootCertFile, "tls-root-cert", "", "", "Absolute or relative path to the CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.CertFile, "tls-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.KeyFile, "tls-key", "", "", "Absolute or relative path to the key .pem")
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the API",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		logger, err := zaplogger.New(serveOpts.LogOptions.LogLevel, serveOpts.LogOptions.LogType)
		if err != nil {
			log.Fatalf("failed to create logger: %v", err)
		}

		logger.Info(fmt.Sprintf("version info: version: %s source-hash: %s", version.BuildVersion, version.BuildSourceHash))

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.KeyFile); errValidate != nil {
			logger.Warn(fmt.Sprintf("invalid internal PKI key permissions: file-path: %s", serveOpts.KeyFile), err)
		}

		certificate, err := common_tls.NewBundleFromFiles(serveOpts.CertFile, serveOpts.KeyFile, serveOpts.RootCertFile)
		if err != nil {
			logger.Fatal("loading TLS files", err)
		}

		zapLogger, err := serveOpts.LogOptions.ZapConfig().Build()
		if err != nil {
			log.Fatalf("failed to create new zap logger: %v", err)
		}

		monitorService, err := monitoring.NewMonitoringService(serveOpts.MonitoringAddress, zapLogger)
		if err != nil {
			logger.Fatal("unable to create monitoring service", err)
		}

		go func() {
			err = monitorService.Start()
			if err != nil {
				logger.Fatal("error listening on monitoring service", err)
			}
		}()

		db, err := postgresadapter.NewConnection(serveOpts.StoragePostgresDSN)
		if err != nil {
			logger.Fatal("can not create db connection:", err)
		}

		storage, err := postgresadapter.New(db)
		if err != nil {
			logger.Fatal("failed to setup postgresql txlog database", err)
		}

		ctx := context.Background()

		app, err := service.NewApplication(&service.NewApplicationArgs{
			Context:    ctx,
			Logger:     logger,
			Repository: storage,
			Clock:      &clock{},
		})
		if err != nil {
			logger.Fatal("could not create application", err)
		}

		restServer, err := rest.New(&rest.NewArgs{
			Logger: logger.Logger,
			App:    app,
			Cert:   certificate,
		})
		if err != nil {
			logger.Fatal("could not create rest server", err)
		}

		var readHeaderTimeout = 5 * time.Second

		srv := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           restServer.Handler(),
			Addr:              serveOpts.ListenAddress,
			TLSConfig:         certificate.TLSConfig(certificate.WithTLSClientAuth()),
		}

		go func() {
			logger.Info(fmt.Sprintf("starting rest server. listen address: %q", serveOpts.ListenAddress))

			err = srv.ListenAndServeTLS(serveOpts.CertFile, serveOpts.KeyFile)
			if err != nil {
				logger.Fatal("could not listen and serve rest server", err)
			}
		}()

		monitorService.SetReady()

		p.Wait()

		logger.Info("starting graceful shutdown")

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		monitorService.SetNotReady()

		err = srv.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown rest server", err)
		}

		err = storage.Shutdown()
		if err != nil {
			logger.Error("could not shutdown storage", err)
		}

		err = monitorService.Stop()
		if err != nil {
			logger.Error("could not stop monitoring", err)
		}
	},
}
