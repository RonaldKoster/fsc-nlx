// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package cmd

import (
	"context"
	"crypto/tls"
	"errors"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/spf13/cobra"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/common/cmd"
	zaplogger "go.nlx.io/nlx/common/logger/zap"
	"go.nlx.io/nlx/common/logoptions"
	"go.nlx.io/nlx/common/monitoring"
	"go.nlx.io/nlx/common/process"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/version"
	controllerapi "go.nlx.io/nlx/controller/ports/internalrest/api/server"
	restcontroller "go.nlx.io/nlx/manager/adapters/controller/rest"
	"go.nlx.io/nlx/manager/adapters/manager/rest"
	postgresadapter "go.nlx.io/nlx/manager/adapters/storage/postgres"
	"go.nlx.io/nlx/manager/adapters/txlog"
	resttxlog "go.nlx.io/nlx/manager/adapters/txlog/rest"
	"go.nlx.io/nlx/manager/apps/ext/command"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/internal/peers"
	externalrest "go.nlx.io/nlx/manager/ports/ext/rest"
	internalrest "go.nlx.io/nlx/manager/ports/int/rest"
	internalunauthenticatedrest "go.nlx.io/nlx/manager/ports/intunauthenticated/rest"
	externalservice "go.nlx.io/nlx/manager/services/ext"
	internalservice "go.nlx.io/nlx/manager/services/int"
	txlogapi "go.nlx.io/nlx/txlog-api/ports/rest/api/server"
)

var grantTypes = []string{"servicePublication", "serviceConnection", "delegatedServicePublication", "delegatedServiceConnection"}

type TLSInternalOptions struct {
	InternalRootCertFile string
	InternalCertFile     string
	InternalKeyFile      string
}

var serveOpts struct {
	ListenAddressInternal                string
	ListenAddressInternalUnauthenticated string
	ListenAddressExternal                string
	MonitoringAddress                    string
	SelfAddress                          string
	ControllerAPIAddress                 string
	StoragePostgresDSN                   string
	GroupID                              string
	DirectoryPeerManagerAddress          string
	DirectoryPeerID                      string
	AutoSignGrants                       []string
	TxLogAPIAddress                      string
	TokenTTL                             string

	logoptions.LogOptions
	cmd.TLSGroupOptions
	cmd.TLSOptions
	TLSInternalOptions

	GroupTokenCertFile string
	GroupTokenKeyFile  string

	GroupContractCertFile string
	GroupContractKeyFile  string
}

//nolint:gochecknoinits,funlen,gocyclo // this is the recommended way to use cobra
func init() {
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressInternal, "listen-address-internal", "", "127.0.0.1:443", "Address for the internal api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressInternalUnauthenticated, "listen-address-internal-unauthenticated", "", "127.0.0.1:444", "Address for the internal unauthenticated api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.ListenAddressExternal, "listen-address-external", "", "127.0.0.1:8443", "Address for the external api to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.MonitoringAddress, "monitoring-address", "", "127.0.0.1:8081", "Address for the monitoring endpoints to listen on. Read https://golang.org/pkg/net/#Dial for possible tcp address specs.")
	serveCommand.Flags().StringVarP(&serveOpts.SelfAddress, "self-address", "", "", "Manager address of this instance, must be publicly routable and either on port 443 or port 8443, e.g. 'https://manager.example.com:443'")
	serveCommand.Flags().StringVarP(&serveOpts.StoragePostgresDSN, "storage-postgres-dsn", "", "", "Postgres Connection URL")
	serveCommand.Flags().StringVarP(&serveOpts.GroupID, "group-id", "", "", "Group ID of the FSC Group")
	serveCommand.Flags().StringVarP(&serveOpts.DirectoryPeerManagerAddress, "directory-peer-manager-address", "", "", "Manager address of directory peer")
	serveCommand.Flags().StringVarP(&serveOpts.DirectoryPeerID, "directory-peer-id", "", "", "ID of directory peer")
	serveCommand.Flags().StringVarP(&serveOpts.ControllerAPIAddress, "controller-api-address", "", "", "Address of the Controller API")
	serveCommand.Flags().StringVarP(&serveOpts.TxLogAPIAddress, "tx-log-api-address", "", "", "Address of the Transaction Log API")
	serveCommand.Flags().StringSliceVar(&serveOpts.AutoSignGrants, "auto-sign-grants", []string{}, fmt.Sprintf("Auto sign grants determines which contraces containing these grants should be automatically signed, possible values are: %s", strings.Join(grantTypes, ", ")))
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogType, "log-type", "", "live", "Set the logging config. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger.")
	serveCommand.Flags().StringVarP(&serveOpts.LogOptions.LogLevel, "log-level", "", "", "Set loglevel")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.RootCertFile, "tls-root-cert", "", "", "Absolute or relative path to the CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.CertFile, "tls-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSOptions.KeyFile, "tls-key", "", "", "Absolute or relative path to the key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSInternalOptions.InternalRootCertFile, "tls-internal-unauthenticated-root-cert", "", "", "Absolute or relative path to the CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSInternalOptions.InternalCertFile, "tls-internal-unauthenticated-cert", "", "", "Absolute or relative path to the cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSInternalOptions.InternalKeyFile, "tls-internal-unauthenticated-key", "", "", "Absolute or relative path to the key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupRootCert, "tls-group-root-cert", "", "", "Absolute or relative path to the NLX CA root cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupCertFile, "tls-group-cert", "", "", "Absolute or relative path to the FSC Group cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TLSGroupOptions.GroupKeyFile, "tls-group-key", "", "", "Absolute or relative path to the FSC Group key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.GroupTokenCertFile, "tls-group-token-cert", "", "", "Absolute or relative path to the FSC Group token cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.GroupTokenKeyFile, "tls-group-token-key", "", "", "Absolute or relative path to the FSC Group token key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.GroupContractCertFile, "tls-group-contract-cert", "", "", "Absolute or relative path to the FSC Group contract cert .pem")
	serveCommand.Flags().StringVarP(&serveOpts.GroupContractKeyFile, "tls-group-contract-key", "", "", "Absolute or relative path to the FSC Group contract key .pem")
	serveCommand.Flags().StringVarP(&serveOpts.TokenTTL, "token-ttl", "", "1h", "Duration of token validity, how long is the time to live for the generated tokens, format is specified in string, e.g. '1h', '300s', or '5m'")

	if err := serveCommand.MarkFlagRequired("listen-address-internal"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("listen-address-internal-unauthenticated"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("listen-address-external"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("storage-postgres-dsn"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-root-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-key"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-internal-unauthenticated-root-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-internal-unauthenticated-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-internal-unauthenticated-key"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-root-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-key"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-token-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-token-key"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-contract-cert"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("tls-group-contract-key"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("directory-peer-manager-address"); err != nil {
		log.Fatal(err)
	}

	if err := serveCommand.MarkFlagRequired("self-address"); err != nil {
		log.Fatal(err)
	}
}

var serveCommand = &cobra.Command{
	Use:   "serve",
	Short: "Start the API",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		logger, err := zaplogger.New(serveOpts.LogOptions.LogLevel, serveOpts.LogOptions.LogType)
		if err != nil {
			log.Fatalf("failed to create logger: %v", err)
		}

		logger.Info(fmt.Sprintf("version info: version: %s source-hash: %s", version.BuildVersion, version.BuildSourceHash))

		zapLogger, err := serveOpts.LogOptions.ZapConfig().Build()
		if err != nil {
			log.Fatalf("failed to create new zap logger: %v", err)
		}

		monitoringService, err := monitoring.NewMonitoringService(serveOpts.MonitoringAddress, zapLogger)
		if err != nil {
			logger.Fatal("unable to create monitoring service", err)
		}

		go func() {
			if err = monitoringService.Start(); err != nil {
				if !errors.Is(err, http.ErrServerClosed) {
					logger.Fatal("error listening on monitoring service", err)
				}

				logger.Fatal("cannot start monitoringService", err)
			}
		}()

		if errValidate := common_tls.VerifyPrivateKeyPermissions(serveOpts.KeyFile); errValidate != nil {
			logger.Warn(fmt.Sprintf("invalid internal PKI key permissions: file-path: %s", serveOpts.KeyFile), err)
		}

		internalCertificate, err := common_tls.NewBundleFromFiles(serveOpts.CertFile, serveOpts.KeyFile, serveOpts.RootCertFile)
		if err != nil {
			logger.Fatal("loading internal TLS files", err)
		}

		internalUnauthenticatedCertificate, err := common_tls.NewBundleFromFiles(serveOpts.InternalCertFile, serveOpts.InternalKeyFile, serveOpts.InternalRootCertFile)
		if err != nil {
			logger.Fatal("loading internal unauthenticated TLS files", err)
		}

		externalCertificate, err := common_tls.NewBundleFromFiles(serveOpts.GroupCertFile, serveOpts.GroupKeyFile, serveOpts.GroupRootCert)
		if err != nil {
			logger.Fatal("loading external TLS files", err)
		}

		tokenCertificate, err := common_tls.NewBundleFromFiles(serveOpts.GroupTokenCertFile, serveOpts.GroupTokenKeyFile, serveOpts.GroupRootCert)
		if err != nil {
			logger.Fatal("loading token TLS files", err)
		}

		signatureCertificate, err := common_tls.NewBundleFromFiles(serveOpts.GroupContractCertFile, serveOpts.GroupContractKeyFile, serveOpts.GroupRootCert)
		if err != nil {
			logger.Fatal("loading contract signature TLS files", err)
		}

		db, err := postgresadapter.NewConnection(serveOpts.StoragePostgresDSN)
		if err != nil {
			logger.Fatal("can not create db connection:", err)
		}

		storage, err := postgresadapter.New(externalCertificate.RootCAs(), db)
		if err != nil {
			logger.Fatal("failed to setup postgresql database", err)
		}

		if serveOpts.SelfAddress != serveOpts.DirectoryPeerManagerAddress && serveOpts.TxLogAPIAddress == "" {
			logger.Fatal("tx-log-api-address is required when the manager does not function as the directory", errors.New("tx-log-api-address not set"))
		}

		ctx := context.Background()

		managerFactory := rest.NewFactory(externalCertificate, serveOpts.SelfAddress)

		peersAdapter, err := peers.New(&peers.NewPeersArgs{
			Ctx:                     ctx,
			Logger:                  logger,
			PeerCert:                externalCertificate,
			Repository:              storage,
			SelfAddress:             serveOpts.SelfAddress,
			DirectoryManagerAddress: serveOpts.DirectoryPeerManagerAddress,
			ManagerFactory:          managerFactory,
		})
		if err != nil {
			logger.Fatal("could not create peers adapter", err)
		}

		controllerClient, err := controllerapi.NewClientWithResponses(serveOpts.ControllerAPIAddress, func(c *controllerapi.Client) error {
			t := http.DefaultTransport.(*http.Transport).Clone()
			t.TLSClientConfig = internalCertificate.TLSConfig()

			c.Client = &http.Client{Transport: t}
			return nil
		})
		if err != nil {
			logger.Fatal("could not create rest controller client", err)
		}

		controllerRepository, err := restcontroller.New(ctx, serveOpts.GroupID, controllerClient)
		if err != nil {
			logger.Fatal("could not create rest controller repository", err)
		}

		var txLogRepository txlog.TXLog

		if serveOpts.TxLogAPIAddress != "" {
			txLogRepository, err = createTxLogRepository(serveOpts.TxLogAPIAddress, internalCertificate.TLSConfig())
			if err != nil {
				logger.Fatal("could not create txlog repository", err)
			}
		}

		selfPeerID := contract.PeerID(externalCertificate.GetOrganizationInfo().SerialNumber)

		tokenTTL, err := time.ParseDuration(serveOpts.TokenTTL)
		if err != nil {
			logger.Fatal("could not parse token-ttl duration", err)
		}

		groupID, err := contract.NewGroupID(serveOpts.GroupID)
		if err != nil {
			logger.Fatal(fmt.Sprintf("invalid group ID provided '%s'", serveOpts.GroupID), err)
		}

		internalApp, err := internalservice.NewApplication(&internalservice.NewApplicationArgs{
			Context:                     ctx,
			Logger:                      logger,
			GroupID:                     groupID,
			Repository:                  storage,
			ControllerRepository:        controllerRepository,
			Peers:                       peersAdapter,
			TXLog:                       txLogRepository,
			Clock:                       clock.New(),
			SelfPeerID:                  selfPeerID,
			SelfPeerName:                externalCertificate.GetOrganizationInfo().Name,
			TrustedExternalRootCAs:      externalCertificate.RootCAs(),
			SignatureCertificate:        signatureCertificate.Cert(),
			ManagerFactory:              managerFactory,
			DirectoryPeerManagerAddress: serveOpts.DirectoryPeerManagerAddress,
			IsTxlogDisabled:             txLogRepository == nil,
		})
		if err != nil {
			logger.Fatal("could not create application internal", err)
		}

		err = internalApp.Commands.CreateCertificate.Handle(ctx, tokenCertificate.Cert().Certificate)
		if err != nil {
			logger.Fatal("could not create token certificate", err)
		}

		err = internalApp.Commands.CreateCertificate.Handle(ctx, signatureCertificate.Cert().Certificate)
		if err != nil {
			logger.Fatal("could not create signature certificate", err)
		}

		externalApp, err := externalservice.NewApplication(&externalservice.NewApplicationArgs{
			Context:              ctx,
			Logger:               logger,
			Repository:           storage,
			ControllerRepository: controllerRepository,
			TXLogRepository:      txLogRepository,
			Clock:                clock.New(),
			Peers:                peersAdapter,
			GroupID:              groupID,
			SelfPeerID:           selfPeerID,
			TrustedRootCAs:       externalCertificate.RootCAs(),
			AutoSignGrants:       serveOpts.AutoSignGrants,
			AutoSignCertificate:  signatureCertificate.Cert(),
			TokenSignCertificate: tokenCertificate.Cert(),
			TokenTTL:             tokenTTL,
			IsTxlogDisabled:      txLogRepository == nil,
		})
		if err != nil {
			logger.Fatal("could not create application external", err)
		}

		restServerExternal, err := externalrest.New(&externalrest.NewArgs{
			Logger:      logger,
			App:         externalApp,
			Cert:        externalCertificate,
			SelfAddress: serveOpts.SelfAddress,
		})
		if err != nil {
			logger.Fatal("could not setup external rest server", err)
		}

		var readHeaderTimeout = 5 * time.Second

		srv := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           restServerExternal.Handler(),
			Addr:              serveOpts.ListenAddressExternal,
			TLSConfig:         externalCertificate.TLSConfig(externalCertificate.WithTLSClientAuth()),
		}

		go func() {
			err = srv.ListenAndServeTLS(serveOpts.GroupCertFile, serveOpts.GroupKeyFile)
			if err != nil {
				logger.Fatal("could not listen and serve external rest server", err)
			}
		}()

		restServerInternal, err := internalrest.New(&internalrest.NewArgs{
			Logger: logger,
			App:    internalApp,
			Cert:   internalCertificate,
		})
		if err != nil {
			logger.Fatal("could not create internal rest server", err)
		}

		srvInternal := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           restServerInternal.Handler(),
			Addr:              serveOpts.ListenAddressInternal,
			TLSConfig:         internalCertificate.TLSConfig(internalCertificate.WithTLSClientAuth()),
		}

		go func() {
			err = srvInternal.ListenAndServeTLS(serveOpts.CertFile, serveOpts.KeyFile)
			if err != nil {
				logger.Fatal("could not listen and serve internal rest server", err)
			}
		}()

		restServerInternalUnauthenticated, err := internalunauthenticatedrest.New(&internalunauthenticatedrest.NewArgs{
			Logger: logger,
			App:    internalApp,
		})
		if err != nil {
			logger.Fatal("could not create internal unauthenticated rest server", err)
		}

		srvInternalUnauthenticated := &http.Server{
			ReadHeaderTimeout: readHeaderTimeout,
			Handler:           restServerInternalUnauthenticated.Handler(),
			Addr:              serveOpts.ListenAddressInternalUnauthenticated,
			TLSConfig:         internalUnauthenticatedCertificate.TLSConfig(),
		}

		go func() {
			err = srvInternalUnauthenticated.ListenAndServeTLS(serveOpts.InternalCertFile, serveOpts.InternalKeyFile)
			if err != nil {
				logger.Fatal("could not listen and serve internal unauthenticated rest server", err)
			}
		}()

		announcePeerContext, cancelPeerAnnouncementContext := context.WithCancel(context.Background())

		go func() {
			// Register ourselves first
			err = externalApp.Commands.Announce.Handle(ctx, &command.PeerArgs{
				ID:             externalCertificate.GetOrganizationInfo().SerialNumber,
				Name:           externalCertificate.GetOrganizationInfo().Name,
				ManagerAddress: serveOpts.SelfAddress,
			})
			if err != nil {
				logger.Fatal("could not announce to ourselves", err)
			}

			// Announce to directory peer
			announce := func() error {
				err = internalApp.Commands.AnnouncePeer.Handle(ctx, []string{
					serveOpts.DirectoryPeerID,
				})
				if err != nil {
					logger.Error(fmt.Sprintf("could not announce to directory peer: %s", serveOpts.DirectoryPeerManagerAddress), err)
					return err
				}

				return nil
			}

			err = backoff.Retry(announce, backoff.WithContext(backoff.NewExponentialBackOff(), announcePeerContext))
			if err != nil {
				logger.Error("announcement to directory peer permanently failed", err)
				return
			}

			logger.Info(fmt.Sprintf("successfully announced to directory peer: %s", serveOpts.DirectoryPeerManagerAddress))
		}()

		monitoringService.SetReady()

		p.Wait()

		cancelPeerAnnouncementContext()

		logger.Info("starting graceful shutdown")

		gracefulCtx, cancel := context.WithTimeout(context.Background(), time.Minute)
		defer cancel()

		monitoringService.SetNotReady()

		err = srvInternal.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown internal rest server", err)
		}

		err = srvInternalUnauthenticated.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown internal unauthenticated rest server", err)
		}

		err = srv.Shutdown(gracefulCtx)
		if err != nil {
			logger.Error("could not shutdown external rest server", err)
		}

		err = storage.Close()
		if err != nil {
			logger.Error("could not shutdown storage", err)
		}

		if err := monitoringService.Stop(); err != nil {
			logger.Error("could not shutdown monitoringService", err)
		}
	},
}

func createTxLogRepository(txLogAPIAddress string, tlsConfig *tls.Config) (txlog.TXLog, error) {
	txLogClient, err := txlogapi.NewClientWithResponses(txLogAPIAddress, func(c *txlogapi.Client) error {
		t := http.DefaultTransport.(*http.Transport).Clone()
		t.TLSClientConfig = tlsConfig

		c.Client = &http.Client{Transport: t}
		return nil
	})
	if err != nil {
		return nil, err
	}

	txLogRepository, err := resttxlog.New(txLogClient)
	if err != nil {
		return nil, err
	}

	return txLogRepository, nil
}
