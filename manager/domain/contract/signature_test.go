// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package contract_test

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gopkg.in/square/go-jose.v2"

	"go.nlx.io/nlx/manager/domain/contract"
)

func TestNewSignature(t *testing.T) {
	// Signing with HS256 which is not supported
	signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.HS256, Key: []byte("testkey")}, nil)
	assert.NoError(t, err)

	object, err := signer.Sign([]byte("Lorem ipsum dolor sit amet"))
	assert.NoError(t, err)

	serialized := object.FullSerialize()

	testArgs := &contract.NewSignatureArgs{
		SigType:   contract.SignatureTypeAccept,
		Signature: serialized,
	}

	_, err = contract.NewSignature(testArgs)
	assert.ErrorContains(t, err, "the algorithm HS256 in the signature is not supported")
}
