// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package contract

import (
	"bytes"
	"fmt"
	"sort"

	"github.com/pkg/errors"
)

type ContentHash Hash

func NewContentHashFromString(h string) (*ContentHash, error) {
	hVal, err := DecodeHashFromString(h)
	if err != nil {
		return nil, errors.Wrap(err, "could not decode content hash from string")
	}

	return (*ContentHash)(hVal), nil
}

func newContentHash(c *Content, alg HashAlg) (*ContentHash, error) {
	pBytes, err := getSortedContentBytes(c)
	if err != nil {
		return nil, errors.Wrap(err, "could not get sorted bytes from content")
	}

	h, err := newHash(alg, HashTypeContent, pBytes)
	if err != nil {
		return nil, errors.Wrap(err, "could not create grant peer registration hash")
	}

	return (*ContentHash)(h), nil
}

func getSortedContentBytes(content *Content) ([]byte, error) {
	contentBytes := make([]byte, 0)

	contentBytes = append(contentBytes, []byte(content.groupID)...)
	contentBytes = append(contentBytes, content.id.Bytes()...)
	contentBytes = append(contentBytes, bytesFromInt64(content.validity.notBefore.Unix())...)
	contentBytes = append(contentBytes, bytesFromInt64(content.validity.notAfter.Unix())...)
	contentBytes = append(contentBytes, bytesFromInt64(content.createdAt.Unix())...)

	grantByteArrays := make([][]byte, 0)

	for _, grant := range content.grants {
		switch g := grant.(type) {
		case *GrantServicePublication:
			grantByteArrays = append(grantByteArrays, []byte(g.Hash().String()))
		case *GrantServiceConnection:
			grantByteArrays = append(grantByteArrays, []byte(g.Hash().String()))
		case *GrantDelegatedServiceConnection:
			grantByteArrays = append(grantByteArrays, []byte(g.Hash().String()))
		case *GrantDelegatedServicePublication:
			grantByteArrays = append(grantByteArrays, []byte(g.Hash().String()))
		default:
			return nil, fmt.Errorf("unsupported grant type, %T", g)
		}
	}

	sort.Slice(grantByteArrays, func(i, j int) bool {
		return bytes.Compare(grantByteArrays[i], grantByteArrays[j]) < 0
	})

	for _, byteArray := range grantByteArrays {
		contentBytes = append(contentBytes, byteArray...)
	}

	return contentBytes, nil
}

func (h ContentHash) String() string {
	return Hash(h).String()
}

func (h ContentHash) Algorithm() HashAlg {
	return Hash(h).Algorithm()
}

func (h *ContentHash) isEqual(other *ContentHash) bool {
	return (*Hash)(h).isEqual((*Hash)(other))
}
