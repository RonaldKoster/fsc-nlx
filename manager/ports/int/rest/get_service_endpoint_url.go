// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"go.nlx.io/nlx/manager/apps/int/query"
	api "go.nlx.io/nlx/manager/ports/int/rest/api/server"
)

func (s *Server) GetServiceEndpointURL(ctx context.Context, req api.GetServiceEndpointURLRequestObject) (api.GetServiceEndpointURLResponseObject, error) {
	s.logger.Info("rest request GetServiceEndpointURLRequest")

	endpointURL, err := s.app.Queries.GetServiceEndpointURL.Handle(ctx, &query.GetServiceEndpointURLHandlerArgs{
		InwayAddress: req.InwayAddress,
		ServiceName:  req.ServiceName,
	})
	if err != nil {
		return nil, err
	}

	return api.GetServiceEndpointURL200JSONResponse{ServiceEndpointUrl: endpointURL}, nil
}
