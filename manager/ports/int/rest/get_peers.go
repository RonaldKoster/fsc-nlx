// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
	api "go.nlx.io/nlx/manager/ports/int/rest/api/server"
)

func (s *Server) GetPeers(ctx context.Context, _ api.GetPeersRequestObject) (api.GetPeersResponseObject, error) {
	s.logger.Info("rest request GetPeerInfo")

	res, err := s.app.Queries.ListPeers.Handle(ctx)
	if err != nil {
		s.logger.Error("error getting peers from query", err)
		return nil, err
	}

	peers := make([]models.Peer, 0, len(res))

	for _, p := range res {
		peers = append(peers, models.Peer{
			Id:   p.ID,
			Name: p.Name,
		})
	}

	return api.GetPeers200JSONResponse{
		Peers: peers,
	}, nil
}
