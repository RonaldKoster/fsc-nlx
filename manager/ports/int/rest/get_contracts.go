// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"github.com/google/uuid"

	"go.nlx.io/nlx/manager/apps/int/query"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
	api "go.nlx.io/nlx/manager/ports/int/rest/api/server"
)

func (s *Server) GetContracts(ctx context.Context, req api.GetContractsRequestObject) (api.GetContractsResponseObject, error) {
	s.logger.Info("rest request GetContracts")

	filters := make([]*query.ListContractsFilter, 0)

	if req.Params.GrantType != nil {
		for _, h := range *req.Params.GrantType {
			if h == "" {
				continue
			}

			filters = append(filters, &query.ListContractsFilter{
				GrantType: convertGrantType(h),
			})
		}
	}

	if req.Params.ContentHash != nil {
		for _, h := range *req.Params.ContentHash {
			if h == "" {
				continue
			}

			filters = append(filters, &query.ListContractsFilter{
				ContentHash: h,
			})
		}
	}

	res, err := s.app.Queries.GetContracts.Handle(ctx, filters)
	if err != nil {
		s.logger.Error("get contracts query", err)
		return nil, err
	}

	response := api.GetContracts200JSONResponse{
		Contracts: make([]models.Contract, len(res)),
	}

	for i, contract := range res {
		var convertedContract *models.Contract

		convertedContract, err = convertContract(contract)
		if err != nil {
			s.logger.Error("get contracts converting contract", err)
			return nil, err
		}

		response.Contracts[i] = *convertedContract
	}

	return response, nil
}

func convertContract(contract *query.ListContractsContract) (*models.Contract, error) {
	grants, err := convertGrants(contract)
	if err != nil {
		return nil, err
	}

	id, err := uuid.FromBytes(contract.ID)
	if err != nil {
		return nil, err
	}

	content := models.ContractContent{
		Id:            id.String(),
		CreatedAt:     contract.CreatedAt.Unix(),
		Grants:        grants,
		GroupId:       contract.GroupID,
		HashAlgorithm: models.HASHALGORITHMSHA3512,
		Validity: models.Validity{
			NotAfter:  contract.NotAfter.Unix(),
			NotBefore: contract.NotBefore.Unix(),
		},
	}

	peers := make(map[string]models.Peer)

	for _, p := range contract.Peers {
		peers[p.ID] = models.Peer{
			Id:   p.ID,
			Name: p.Name,
		}
	}

	return &models.Contract{
		Hash:        contract.Hash,
		Content:     content,
		Peers:       peers,
		HasAccepted: contract.HasAccepted,
		HasRejected: contract.HasRejected,
		HasRevoked:  contract.HasRevoked,
		Signatures: models.Signatures{
			Accept: mapSignatureToRest(contract.AcceptSignatures),
			Reject: mapSignatureToRest(contract.RejectSignatures),
			Revoke: mapSignatureToRest(contract.RevokeSignatures),
		},
	}, nil
}

func mapSignatureToRest(sigs map[string]query.Signature) models.SignatureMap {
	s := make(models.SignatureMap)

	for k, v := range sigs {
		s[k] = models.Signature{
			Peer: models.Peer{
				Id:   v.Peer.ID,
				Name: v.Peer.Name,
			},
			SignedAt: v.SignedAt.Unix(),
		}
	}

	return s
}

//nolint:gocyclo // complex function because of the number of grant types
func convertGrants(contract *query.ListContractsContract) ([]models.Grant, error) {
	grants := make([]models.Grant, 0)

	for _, g := range contract.ServiceConnectionGrants {
		grant, err := convertServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.ServicePublicationGrants {
		grant, err := convertServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.DelegatedServiceConnectionGrants {
		grant, err := convertDelegatedServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.DelegatedServicePublicationGrants {
		grant, err := convertDelegatedServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	return grants, nil
}

func convertServicePublicationGrant(grant *query.ServicePublicationGrant) (*models.Grant, error) {
	data := models.Grant{}

	err := data.FromGrantServicePublication(models.GrantServicePublication{
		Type: models.GRANTTYPESERVICEPUBLICATION,
		Hash: grant.Hash,
		Directory: models.DirectoryPeer{
			PeerId: grant.DirectoryPeerID,
		},
		Service: models.ServicePublicationPeer{
			Name:     grant.ServiceName,
			PeerId:   grant.ServicePeerID,
			Protocol: mapServiceProtocolToRest(grant.ServiceProtocol),
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertServiceConnectionGrant(grant *query.ServiceConnectionGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantServiceConnection(models.GrantServiceConnection{
		Type: models.GRANTTYPESERVICECONNECTION,
		Hash: grant.Hash,
		Outway: models.OutwayPeer{
			PeerId:                grant.OutwayPeerID,
			CertificateThumbprint: grant.OutwayCertificateThumbprint,
		},
		Service: models.ServicePeer{
			PeerId: grant.ServicePeerID,
			Name:   grant.ServiceName,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertDelegatedServiceConnectionGrant(grant *query.DelegatedServiceConnectionGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantDelegatedServiceConnection(models.GrantDelegatedServiceConnection{
		Type: models.GRANTTYPEDELEGATEDSERVICECONNECTION,
		Hash: grant.Hash,
		Delegator: models.DelegatorPeer{
			PeerId: grant.DelegatorPeerID,
		},
		Service: models.ServicePeer{
			PeerId: grant.ServicePeerID,
			Name:   grant.ServiceName,
		},
		Outway: models.OutwayPeer{
			PeerId:                grant.OutwayPeerID,
			CertificateThumbprint: grant.OutwayCertificateThumbprint,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertDelegatedServicePublicationGrant(grant *query.DelegatedServicePublicationGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantDelegatedServicePublication(models.GrantDelegatedServicePublication{
		Type: models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
		Hash: grant.Hash,
		Delegator: models.DelegatorPeer{
			PeerId: grant.DelegatorPeerID,
		},
		Service: models.ServicePublicationPeer{
			Name:     grant.ServiceName,
			PeerId:   grant.ServicePeerID,
			Protocol: mapServiceProtocolToRest(grant.ServiceProtocol),
		},
		Directory: models.DirectoryPeer{
			PeerId: grant.DelegatorPeerID,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertGrantType(grantType models.GrantType) query.GrantType {
	switch grantType {
	case models.GRANTTYPESERVICECONNECTION:
		return query.GrantTypeServiceConnection
	case models.GRANTTYPESERVICEPUBLICATION:
		return query.GrantTypeServicePublication
	case models.GRANTTYPEDELEGATEDSERVICECONNECTION:
		return query.GrantTypeDelegatedServiceConnection
	case models.GRANTTYPEDELEGATEDSERVICEPUBLICATION:
		return query.GrantTypeDelegatedServicePublication
	default:
		return query.GrantTypeUnspecified
	}
}

func mapServiceProtocolToRest(p contract.ServiceProtocol) models.Protocol {
	switch p {
	case contract.ServiceProtocolTCPHTTP1_1:
		return models.PROTOCOLTCPHTTP11
	case contract.ServiceProtocolTCPHTTP2:
		return models.PROTOCOLTCPHTTP2
	default:
		return ""
	}
}
