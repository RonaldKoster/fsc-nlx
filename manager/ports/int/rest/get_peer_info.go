// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
	api "go.nlx.io/nlx/manager/ports/int/rest/api/server"
)

func (s *Server) GetPeer(ctx context.Context, _ api.GetPeerRequestObject) (api.GetPeerResponseObject, error) {
	s.logger.Info("rest request GetPeerInfo")

	peer := s.app.Queries.GetPeerInfo.Handle(ctx)

	return api.GetPeer200JSONResponse{
		Peer: models.Peer{
			Id:   peer.ID,
			Name: peer.Name,
		},
	}, nil
}
