// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"go.nlx.io/nlx/manager/apps/int/query"
	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
	api "go.nlx.io/nlx/manager/ports/int/rest/api/server"
)

func (s *Server) GetToken(ctx context.Context, req api.GetTokenRequestObject) (api.GetTokenResponseObject, error) {
	s.logger.Info("rest request GetTokenRequest")

	res, err := s.app.Queries.GetToken.Handle(ctx, &query.GetTokenHandlerArgs{
		GrantHash:                   req.Params.GrantHash,
		OutwayCertificateThumbprint: req.Params.OutwayCertificateThumbprint,
	})
	if err != nil {
		s.logger.Warn("could not get token from querier", err)
		return nil, err
	}

	return api.GetToken200JSONResponse{
		Token: res.Token,
		TokenInfo: models.TokenInfo{
			GroupId:                     res.GroupID,
			GrantHash:                   res.GrantHash,
			OutwayPeerId:                res.OutwayPeerID,
			OutwayDelegatorPeerId:       res.OutwayDelegatorPeerID,
			OutwayCertificateThumbprint: res.OutwayCertificateThumbprint,
			ServiceName:                 res.ServiceName,
			ServiceInwayAddress:         res.ServiceInwayAddress,
			ServicePeerId:               res.ServicePeerID,
			ServiceDelegatorPeerId:      res.ServiceDelegatorPeerID,
			ExpiryDate:                  res.ExpiryDate.Unix(),
		},
	}, nil
}
