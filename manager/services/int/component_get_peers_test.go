// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
)

func TestGetPeers(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name())
	defer externalHTTPServer.Close()

	apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, orgA.CertBundle)
	assert.NoError(t, err)

	tests := map[string]struct {
		Want []models.Peer
	}{
		"happy_flow": {
			Want: []models.Peer{
				{
					Id:   orgA.GetPeerID(),
					Name: orgA.GetName(),
				},
				{
					Id:   orgB.GetPeerID(),
					Name: orgB.GetName(),
				},
				{
					Id:   orgC.GetPeerID(),
					Name: orgC.GetName(),
				},
			},
		},
	}

	for testName, test := range tests {
		t.Run(testName, func(t *testing.T) {
			// Act
			res, err := apiClient.GetPeersWithResponse(context.Background())

			// Assert
			assert.NoError(t, err)
			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Equal(t, test.Want, res.JSON200.Peers)
		})
	}
}
