// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

//nolint:unused // most methods are unused but added to comply with the Manager interface
package internalservice_test

import (
	"context"
	"fmt"
	"log"

	"go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/manager/adapters/manager"
	"go.nlx.io/nlx/manager/domain/contract"
)

type testManagerFactory struct {
}

type testManager struct {
	address string
	orgCert *tls.CertificateBundle
}

func (f *testManagerFactory) New(_ context.Context, address string) (manager.Manager, error) {
	if address == orgA.ManagerAddress {
		return &testManager{
			address: address,
			orgCert: orgA.CertBundle,
		}, nil
	}

	if address == orgB.ManagerAddress {
		return &testManager{
			address: address,
			orgCert: orgB.CertBundle,
		}, nil
	}

	if address == orgC.ManagerAddress {
		return &testManager{
			address: address,
			orgCert: orgC.CertBundle,
		}, nil
	}

	if address == orgDirectory.ManagerAddress {
		return &testManager{
			address: address,
			orgCert: orgDirectory.CertBundle,
		}, nil
	}

	return nil, fmt.Errorf("unknown address '%s', unable to setup manager client", address)
}

func (m *testManager) GetCertificates(_ context.Context) (*contract.PeerCertificates, error) {
	peerCert, err := contract.NewPeerCertFromCertificate(m.orgCert.RootCAs(), m.orgCert.Cert().Certificate)
	if err != nil {
		log.Printf("failed to create peer cert: %v", err)
	}

	peerCerts := &contract.PeerCertificates{
		peerCert.CertificateThumbprint(): peerCert,
	}

	return peerCerts, nil
}

func (m *testManager) GetPeers(_ context.Context, _ []contract.PeerID) ([]*contract.Peer, error) {
	orgAPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             orgA.GetPeerID(),
		Name:           orgA.GetName(),
		ManagerAddress: orgA.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to crate new test peer %s", err)
	}

	orgBPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             orgB.GetPeerID(),
		Name:           orgB.GetName(),
		ManagerAddress: orgB.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to crate new test peer %s", err)
	}

	orgCPeer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             orgC.GetPeerID(),
		Name:           orgC.GetName(),
		ManagerAddress: orgC.ManagerAddress,
	})
	if err != nil {
		log.Fatalf("failed to create new test peer %s", err)
	}

	return []*contract.Peer{
		orgAPeer,
		orgBPeer,
		orgCPeer,
	}, nil
}

func (m *testManager) Announce(_ context.Context) error {
	panic(fmt.Sprintf("unintended call to announce %s", m.address))
}

func (m *testManager) GetContracts(_ context.Context, _ *contract.GrantType, _ *[]string) ([]*contract.Contract, error) {
	panic(fmt.Sprintf("unintended call to get contracts %s", m.address))
}

func (m *testManager) SubmitContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	return nil
}

func (m *testManager) AcceptContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	panic(fmt.Sprintf("unintended call to accept contract %s", m.address))
}

func (m *testManager) RejectContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	panic(fmt.Sprintf("unintended call to reject contract %s", m.address))
}

func (m *testManager) RevokeContract(_ context.Context, _ *contract.Content, _ *contract.Signature) error {
	panic(fmt.Sprintf("unintended call to revoke contract %s", m.address))
}

func (m *testManager) GetPeerInfo(_ context.Context) (*contract.Peer, error) {
	panic(fmt.Sprintf("unintended call to get peer info %s", m.address))
}

func (m *testManager) GetServices(_ context.Context, _ *contract.PeerID, _ string) ([]*contract.Service, error) {
	panic(fmt.Sprintf("unintended call to get services %s", m.address))
}

func (m *testManager) GetToken(_ context.Context, _ string) (string, error) {
	panic(fmt.Sprintf("unintended call to get token %s", m.address))
}

func (m *testManager) GetTXLogRecords(_ context.Context) (contract.TXLogRecords, error) {
	panic(fmt.Sprintf("unintended call to get txlog records %s", m.address))
}
