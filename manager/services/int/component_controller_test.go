// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"

	"go.nlx.io/nlx/manager/adapters/controller"
)

type FakeController struct{}

func newFakeController() controller.Controller {
	return &FakeController{}
}

func (c *FakeController) GetService(_ context.Context, _, _ string) (*controller.Service, error) {
	return nil, nil
}

func (c *FakeController) GetInwayAddressForService(_ context.Context, _ string) (string, error) {
	return "", nil
}

func (c *FakeController) GetServiceEndpointURL(_ context.Context, _, _ string) (string, error) {
	return "", nil
}
