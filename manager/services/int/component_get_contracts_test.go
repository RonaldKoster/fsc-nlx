// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package internalservice_test

import (
	"context"
	"go.nlx.io/nlx/manager/domain/contract"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
	testhash "go.nlx.io/nlx/testing/hash"
)

func TestListContracts(t *testing.T) {
	t.Parallel()

	now := time.Date(2023, 5, 23, 0, 0, 0, 0, time.UTC)

	tests := map[string]struct {
		ContractsToCreate []*command.CreateContractHandlerArgs
		Params            *models.GetContractsParams
		Want              []models.Contract
	}{
		"filter_by_content_hash": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: orgC.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "basisregister-fictieve-kentekens",
						PeerId:   orgA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						ID:                "102635ef-7053-4151-8fd1-2537f99bee79",
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: orgC.GetPeerID(),
								ServicePeerID:   orgA.GetPeerID(),
								ServiceName:     "basisregister-fictieve-kentekens",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: now,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType: nil,
				ContentHash: func() *[]string {
					hash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
						ID:             "102635ef-7053-4151-8fd1-2537f99bee79",
						GroupID:        "test-group",
						HashAlgorithm:  1,
						ValidNotBefore: now.Add(-1 * time.Hour).Unix(),
						ValidNotAfter:  now.Add(time.Hour).Unix(),
						ServicePublicationGrants: []testhash.ContractContentServicePublicationGrantArgs{
							{
								DirectoryPeerID: orgC.GetPeerID(),
								ServicePeerID:   orgA.GetPeerID(),
								ServiceName:     "basisregister-fictieve-kentekens",
								ServiceProtocol: string(models.PROTOCOLTCPHTTP11),
							},
						},
						ServiceConnectionGrants:           nil,
						DelegatedServiceConnectionGrants:  nil,
						DelegatedServicePublicationGrants: nil,
						CreatedAt:                         now.Unix(),
					})
					assert.NoError(t, err)

					return &[]string{hash}
				}(),
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: orgC.GetPeerID(),
					},
					Hash: "$1$2$Uth-oG_uLYQSpKE_-3l_7Qh6fKYN3X68yoy7IY2V8JcNpA_7GQF-z3lvuM73Zmxlf6cIPnVQJflC1JHfWzy-DQ",
					Service: models.ServicePublicationPeer{
						PeerId:   orgA.GetPeerID(),
						Name:     "basisregister-fictieve-kentekens",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Id:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        "$1$1$Ly49CiNH6wOjOGbjuTwfaoFbyMZ9yfEDN0B5RQfdmy7eUnaS5kHm1aOgGe--gdJ0aEYkObJSf3D_oz0bWceMYw",
						Peers: map[string]models.Peer{
							orgA.GetPeerID(): {
								Id:   orgA.GetPeerID(),
								Name: orgA.GetName(),
							},
							orgC.GetPeerID(): {
								Id:   orgC.GetPeerID(),
								Name: orgC.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								orgA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   orgA.GetPeerID(),
										Name: orgA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
					},
				}

				return result
			}(),
		},
		"filter_by_grant_type": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: orgC.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "basisregister-fictieve-personen",
						PeerId:   orgA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				id := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						ID:                id.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: orgC.GetPeerID(),
								ServicePeerID:   orgA.GetPeerID(),
								ServiceName:     "basisregister-fictieve-personen",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: now,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   &[]models.GrantType{models.GRANTTYPESERVICEPUBLICATION},
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: orgC.GetPeerID(),
					},
					Hash: "$1$2$HBB__t_EMgqEqSJYC5Aa58qZ1Q0Tgith3seqT1VYwB4SJHC57PzCNdW3YwnAiwAGRLVskc1SO3jqzapa65cbdQ",
					Service: models.ServicePublicationPeer{
						PeerId:   orgA.GetPeerID(),
						Name:     "basisregister-fictieve-personen",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Id:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        "$1$1$k7431-D7KUbmAXyrpJo5CWZx22sHkUH6MEf-moqfPZNiPGBkvWHkSDX3SiukV_eWAa6bvgZzfX_TBy7nnBhNAg",
						Peers: map[string]models.Peer{
							orgA.GetPeerID(): {
								Id:   orgA.GetPeerID(),
								Name: orgA.GetName(),
							},
							orgC.GetPeerID(): {
								Id:   orgC.GetPeerID(),
								Name: orgC.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								orgA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   orgA.GetPeerID(),
										Name: orgA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
					},
				}

				return result
			}(),
		},
		"happy_flow": {
			ContractsToCreate: func() []*command.CreateContractHandlerArgs {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Type: models.GRANTTYPESERVICEPUBLICATION,
					Directory: models.DirectoryPeer{
						PeerId: orgC.GetPeerID(),
					},
					Service: models.ServicePublicationPeer{
						Name:     "parkeerrechten",
						PeerId:   orgA.GetPeerID(),
						Protocol: models.PROTOCOLTCPHTTP11,
					},
				})
				assert.NoError(t, err)

				id := uuid.MustParse("102635ef-7053-4151-8fd1-2537f99bee79")
				assert.NoError(t, err)

				result := []*command.CreateContractHandlerArgs{
					{
						HashAlgorithm:     string(models.HASHALGORITHMSHA3512),
						ID:                id.String(),
						GroupID:           "test-group",
						ContractNotBefore: now.Add(-1 * time.Hour),
						ContractNotAfter:  now.Add(time.Hour),
						Grants: []interface{}{
							&command.GrantServicePublicationArgs{
								DirectoryPeerID: orgC.GetPeerID(),
								ServicePeerID:   orgA.GetPeerID(),
								ServiceName:     "parkeerrechten",
								ServiceProtocol: contract.ServiceProtocolTCPHTTP1_1,
							},
						},
						CreatedAt: now,
					},
				}

				return result
			}(),
			Params: &models.GetContractsParams{
				GrantType:   nil,
				ContentHash: &[]string{},
			},
			Want: func() []models.Contract {
				servicePublicationGrant := models.Grant{}

				err := servicePublicationGrant.FromGrantServicePublication(models.GrantServicePublication{
					Directory: models.DirectoryPeer{
						PeerId: orgC.GetPeerID(),
					},
					Hash: "$1$2$MdEF9br_Qkx1psi3bfyEWnef9ZqzJMXoE0tOL1r1lzA77bP3Ps0tzo8SM816s0ykia_51iV8FgYmG1OBd8AgqA",
					Service: models.ServicePublicationPeer{
						PeerId:   orgA.GetPeerID(),
						Name:     "parkeerrechten",
						Protocol: "PROTOCOL_TCP_HTTP_1.1",
					},
					Type: models.GRANTTYPESERVICEPUBLICATION,
				})
				assert.NoError(t, err)

				result := []models.Contract{
					{
						Content: models.ContractContent{
							CreatedAt: now.Unix(),
							Grants: []models.Grant{
								servicePublicationGrant,
							},
							GroupId:       "test-group",
							HashAlgorithm: "HASH_ALGORITHM_SHA3_512",
							Id:            "102635ef-7053-4151-8fd1-2537f99bee79",
							Validity: models.Validity{
								NotAfter:  1684803600,
								NotBefore: 1684796400,
							},
						},
						HasAccepted: true,
						HasRejected: false,
						HasRevoked:  false,
						Hash:        "$1$1$ZtNWN0ysfWT_9Qpb0MOLothOs2K1P1azbuaFP1bgbtHYM_iPlJqq1DkU2sBwQLg2IvX83BU7gkdl7EtiEWvpZw",
						Peers: map[string]models.Peer{
							orgA.GetPeerID(): {
								Id:   orgA.GetPeerID(),
								Name: orgA.GetName(),
							},
							orgC.GetPeerID(): {
								Id:   orgC.GetPeerID(),
								Name: orgC.GetName(),
							},
						},
						Signatures: models.Signatures{
							Accept: models.SignatureMap{
								orgA.GetPeerID(): models.Signature{
									Peer: models.Peer{
										Id:   orgA.GetPeerID(),
										Name: orgA.GetName(),
									},
									SignedAt: time.Date(nowInUTC.Year(), nowInUTC.Month(), nowInUTC.Day(), nowInUTC.Hour(), nowInUTC.Minute(), nowInUTC.Second(), nowInUTC.Nanosecond(), nowInUTC.Location()).Unix(),
								},
							},
							Reject: models.SignatureMap{},
							Revoke: models.SignatureMap{},
						},
					},
				}

				return result
			}(),
		},
	}

	for testName, test := range tests {
		tc := test

		t.Run(testName, func(t *testing.T) {
			externalHTTPServer, app := newService(t.Name())
			defer externalHTTPServer.Close()

			apiClient, err := createInternalManagerAPIClient(externalHTTPServer.URL, orgA.CertBundle)
			assert.NoError(t, err)

			for _, createContractArgs := range tc.ContractsToCreate {
				_, createErr := app.Commands.CreateContract.Handle(context.Background(), createContractArgs)
				assert.NoError(t, createErr)
			}

			res, err := apiClient.GetContractsWithResponse(context.Background(), tc.Params)
			assert.NoError(t, err)

			assert.Equal(t, http.StatusOK, res.StatusCode())
			assert.Len(t, res.JSON200.Contracts, len(test.Want))
			assert.Equal(t, test.Want, res.JSON200.Contracts)
		})
	}
}
