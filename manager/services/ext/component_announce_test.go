// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
	"go.nlx.io/nlx/testing/testingutils"
)

func TestAnnounce(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name())
	defer externalHTTPServer.Close()

	orgBCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "..", "../", "testing", "pki"), testingutils.NLXTestOrgB)
	assert.NoError(t, err)

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgBCertBundle)
	assert.NoError(t, err)

	// Act
	res, err := client.Announce(context.Background(), &models.AnnounceParams{
		FscManagerAddress: "https://my-manager-address:8443",
	})
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode)

	resPeers, err := client.GetPeersWithResponse(context.Background(), &models.GetPeersParams{
		PeerId: &[]models.FSCCorePeerID{orgB.GetPeerID()},
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, resPeers.StatusCode())

	expectedPeers := []models.FSCCorePeer{
		{
			Id:             orgB.GetPeerID(),
			ManagerAddress: "https://my-manager-address:8443",
			Name:           orgB.GetName(),
		},
	}
	assert.Equal(t, expectedPeers, resPeers.JSON200.Peers)
}
