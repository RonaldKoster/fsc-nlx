// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"crypto/rsa"
	"encoding/base64"
	"math/big"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

// Manager-JSONWebKeySet-1
func TestGetJWKS(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	err = intApp.Commands.CreateCertificate.Handle(context.Background(), orgA.CertBundle.Cert().Certificate)
	assert.NoError(t, err)

	// Act
	res, err := client.GetJSONWebKeySetWithResponse(context.Background())
	assert.NoError(t, err)

	// Assert
	assert.Equal(t, http.StatusOK, res.StatusCode())

	assert.Len(t, res.JSON200.Keys, 1)

	assert.Equal(t, models.RS512, *res.JSON200.Keys[0].Alg)
	assert.Equal(t, models.RSA, res.JSON200.Keys[0].Kty)
	assert.Equal(t, orgA.CertBundle.CertificateThumbprint(), *res.JSON200.Keys[0].Kid)
	assert.Equal(t, orgA.CertBundle.CertificateThumbprint(), *res.JSON200.Keys[0].X5tS256)

	x5c := *res.JSON200.Keys[0].X5c

	for i, cert := range x5c {
		pubKeyPem := base64.StdEncoding.EncodeToString(orgA.CertBundle.Cert().Certificate[i])
		assert.Equal(t, pubKeyPem, cert)
	}

	// Assert RSA Public key specific fields
	jwkRsaPublicKey, err := res.JSON200.Keys[0].AsFSCCoreRsaPublicKey()
	assert.NoError(t, err)

	orgACertBundleRsaPublicKey := orgA.CertBundle.Certificate().PublicKey.(*rsa.PublicKey)
	assert.Equal(t, big.NewInt(int64(orgACertBundleRsaPublicKey.E)).Bytes(), *jwkRsaPublicKey.E)
	assert.Equal(t, orgACertBundleRsaPublicKey.N.Bytes(), *jwkRsaPublicKey.N)
}
