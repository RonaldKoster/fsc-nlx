// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/url"

	"go.nlx.io/nlx/manager/adapters/controller"
)

type FakeController struct{}

func newFakeController() *FakeController {
	return &FakeController{}
}

func (c *FakeController) GetInwayAddressForService(_ context.Context, _ string) (string, error) {
	return "", nil
}

func (c *FakeController) GetService(_ context.Context, _, _ string) (*controller.Service, error) {
	e, err := url.Parse("https://service.endpoint.com:443")
	if err != nil {
		return nil, err
	}

	return &controller.Service{
		Name:         "service-1",
		InwayAddress: "https://inway.address.com:443",
		EndpointURL:  e,
	}, nil
}
