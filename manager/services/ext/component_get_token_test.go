// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/common/accesstoken"
	"go.nlx.io/nlx/manager/adapters/manager/rest"
	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
	internal_models "go.nlx.io/nlx/manager/ports/int/rest/api/models"
)

// Manager-GetToken-1
func TestGetTokenConnectionGrant(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	peerCert, err := contract.NewPeerCertFromCertificate(orgA.CertBundle.RootCAs(), orgA.CertBundle.Cert().Certificate)
	assert.NoError(t, err)

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     testClock.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now(),
			NotAfter:  testClock.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                contractContent.ID().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:          orgB.GetPeerID(),
				ServicePeerID:         orgA.GetPeerID(),
				ServiceName:           contractContent.Grants().ServiceConnectionGrants()[0].Service().Name(),
			},
		},
		CreatedAt: contractContent.CreatedAt(),
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	assert.NoError(t, err)

	resp, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode())

	tokenExpirationDate := testClock.Now().Add(accessTokenTTL)

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
	})

	// Assert
	assert.NoError(t, errReq)
	assert.Equal(t, http.StatusOK, tokenResp.StatusCode())

	receivedToken, errTokenDecode := accesstoken.DecodeFromString(testClock, peerCert, tokenResp.JSON200.AccessToken)
	assert.NoError(t, errTokenDecode)

	assert.Equal(t, &accesstoken.DecodedToken{
		GroupID:                     contractContent.GroupID(),
		GrantHash:                   contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		OutwayPeerID:                orgB.CertBundle.GetOrganizationInfo().SerialNumber,
		OutwayCertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
		ServiceName:                 "parkeerrechten",
		ServiceInwayAddress:         "https://inway.address.com:443",
		ServicePeerID:               orgA.CertBundle.GetOrganizationInfo().SerialNumber,
		ExpiryDate:                  tokenExpirationDate.Add(time.Duration(-1 * tokenExpirationDate.Nanosecond())), // since unix timestamps do not support nanoseconds
	}, receivedToken)
}

// Manager-GetToken-2
func TestGetTokenInvalidContract(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	unsignedContractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     testClock.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now(),
			NotAfter:  testClock.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                unsignedContractContent.ID().String(),
		GroupID:           unsignedContractContent.GroupID(),
		ContractNotBefore: unsignedContractContent.NotBefore(),
		ContractNotAfter:  unsignedContractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: unsignedContractContent.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:          unsignedContractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				ServicePeerID:         unsignedContractContent.Grants().ServiceConnectionGrants()[0].Service().Peer().ID().Value(),
				ServiceName:           unsignedContractContent.Grants().ServiceConnectionGrants()[0].Service().Name(),
			},
		},
		CreatedAt: unsignedContractContent.CreatedAt(),
	})
	assert.NoError(t, err)

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     unsignedContractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
	})

	// Assert
	assert.NoError(t, errReq)
	assert.Equal(t, http.StatusBadRequest, tokenResp.StatusCode())
	assert.Equal(t, "invalid_scope", string(tokenResp.JSON400.Error))
	assert.Equal(t, "no valid contract found for hash", *tokenResp.JSON400.ErrorDescription)
}

// Manager-GetToken-3
func TestGetTokenRevokedContract(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     testClock.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now(),
			NotAfter:  testClock.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                contractContent.ID().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:          contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				ServicePeerID:         contractContent.Grants().ServiceConnectionGrants()[0].Service().Peer().ID().Value(),
				ServiceName:           contractContent.Grants().ServiceConnectionGrants()[0].Service().Name(),
			},
		},
		CreatedAt: contractContent.CreatedAt(),
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	assert.NoError(t, err)

	respAccept, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})

	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, respAccept.StatusCode())

	revokedSignatureOrgB, err := contractContent.Revoke(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	respRevoke, err := clientOrgB.RevokeContractWithResponse(context.Background(), contractContent.Hash().String(), &models.RevokeContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.RevokeContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       revokedSignatureOrgB.JWS(),
	})

	assert.NoError(t, err)
	if !assert.Equal(t, http.StatusCreated, respRevoke.StatusCode()) {
		t.Errorf("token response body: %s", respRevoke.Body)
	}

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
	})

	// Assert
	assert.NoError(t, errReq)
	if !assert.Equal(t, http.StatusBadRequest, tokenResp.StatusCode()) {
		t.Errorf("token response body: %s", tokenResp.Body)
	}

	assert.Equal(t, "invalid_scope", string(tokenResp.JSON400.Error))
	assert.Equal(t, "no valid contract found for hash", *tokenResp.JSON400.ErrorDescription)
}

// Manager-GetToken-4
func TestGetTokenInvalidGrantInScope(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, _ := newService(t.Name())
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     "invalid scope",
	})

	// Assert
	assert.NoError(t, errReq)
	assert.Equal(t, http.StatusBadRequest, tokenResp.StatusCode())
	assert.Equal(t, "invalid_scope", string(tokenResp.JSON400.Error))
	assert.Equal(t, "invalid grant in scope", *tokenResp.JSON400.ErrorDescription)
}

// Manager-GetToken-5
func TestGetTokenUnsupportedGrantType(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     testClock.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now(),
			NotAfter:  testClock.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                contractContent.ID().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:          contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				ServicePeerID:         contractContent.Grants().ServiceConnectionGrants()[0].Service().Peer().ID().Value(),
				ServiceName:           contractContent.Grants().ServiceConnectionGrants()[0].Service().Name(),
			},
		},
		CreatedAt: contractContent.CreatedAt(),
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	assert.NoError(t, err)

	respAccept, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})

	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, respAccept.StatusCode())

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: "authorization_code",
		Scope:     contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
	})

	// Assert
	assert.NoError(t, errReq)
	assert.Equal(t, http.StatusBadRequest, tokenResp.StatusCode())
	assert.Equal(t, "unsupported_grant_type", string(tokenResp.JSON400.Error))
	assert.Equal(t, "provided grant type is not supported. only client_credentials is supported", *tokenResp.JSON400.ErrorDescription)
}

// Manager-GetToken-6
func TestGetTokenDelegatedConnectionGrant(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	peerCert, err := contract.NewPeerCertFromCertificate(orgA.CertBundle.RootCAs(), orgA.CertBundle.Cert().Certificate)
	assert.NoError(t, err)

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	clientOrgC, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgC.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     testClock.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now(),
			NotAfter:  testClock.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgC.GetPeerID(),
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                contractContent.ID().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantDelegatedServiceConnectionArgs{
				OutwayCertificateThumbprint: contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:                contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				ServicePeerID:               contractContent.Grants().DelegatedServiceConnectionGrants()[0].Service().Peer().ID().Value(),
				ServiceName:                 contractContent.Grants().DelegatedServiceConnectionGrants()[0].Service().Name(),
				DelegatorPeerID:             contractContent.Grants().DelegatedServiceConnectionGrants()[0].Delegator().Peer().ID().Value(),
			},
		},
		CreatedAt: contractContent.CreatedAt(),
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	acceptSignatureOrgC, err := contractContent.Accept(orgC.CertBundle.RootCAs(), orgC.CertBundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	assert.NoError(t, err)

	respAcceptOrgB, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, respAcceptOrgB.StatusCode())

	respAcceptOrgC, err := clientOrgC.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgC.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgC.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, respAcceptOrgC.StatusCode())

	tokenExpirationDate := testClock.Now().Add(accessTokenTTL)

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     contractContent.Grants().DelegatedServiceConnectionGrants()[0].Hash().String(),
	})

	// Assert
	assert.NoError(t, errReq)
	assert.Equal(t, http.StatusOK, tokenResp.StatusCode())

	receivedToken, errTokenDecode := accesstoken.DecodeFromString(testClock, peerCert, tokenResp.JSON200.AccessToken)
	assert.NoError(t, errTokenDecode)

	assert.Equal(t, &accesstoken.DecodedToken{
		GroupID:                     contractContent.GroupID(),
		GrantHash:                   contractContent.Grants().DelegatedServiceConnectionGrants()[0].Hash().String(),
		OutwayPeerID:                orgB.CertBundle.GetOrganizationInfo().SerialNumber,
		OutwayCertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
		OutwayDelegatorPeerID:       orgC.CertBundle.GetOrganizationInfo().SerialNumber,
		ServiceName:                 contractContent.Grants().DelegatedServiceConnectionGrants()[0].Service().Name(),
		ServiceInwayAddress:         "https://inway.address.com:443",
		ServicePeerID:               orgA.CertBundle.GetOrganizationInfo().SerialNumber,
		ExpiryDate:                  tokenExpirationDate.Add(time.Duration(-1 * tokenExpirationDate.Nanosecond())), // since unix timestamps do not support nanoseconds
	}, receivedToken)
}

// Manager-GetToken-7
func TestGetTokenConnectionGrantDelegatedPublication(t *testing.T) {
	t.Parallel()

	// Arrange
	externalHTTPServer, intApp := newService(t.Name())
	defer externalHTTPServer.Close()

	peerCert, err := contract.NewPeerCertFromCertificate(orgA.CertBundle.RootCAs(), orgA.CertBundle.Cert().Certificate)
	assert.NoError(t, err)

	clientOrgB, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	clientOrgC, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgC.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     testClock.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now(),
			NotAfter:  testClock.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgC.GetPeerID(),
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                contractContent.ID().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DelegatorPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Delegator().Peer().ID().Value(),
				DirectoryPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServicePeerID:   contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Name(),
				ServiceProtocol: contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Protocol(),
			},
		},
		CreatedAt: contractContent.CreatedAt(),
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err := contractContent.Accept(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	cc, err := rest.ContractContentToAPIModel(contractContent)
	assert.NoError(t, err)

	resp, err := clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode())

	acceptSignatureOrgC, err := contractContent.Accept(orgC.CertBundle.RootCAs(), orgC.CertBundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	resp, err = clientOrgC.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgC.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode())

	contractContent, err = contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     testClock.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: testClock.Now(),
			NotAfter:  testClock.Now().Add(time.Hour),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = intApp.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                contractContent.ID().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:          orgB.GetPeerID(),
				ServicePeerID:         orgA.GetPeerID(),
				ServiceName:           contractContent.Grants().ServiceConnectionGrants()[0].Service().Name(),
			},
		},
		CreatedAt: contractContent.CreatedAt(),
	})
	assert.NoError(t, err)

	acceptSignatureOrgB, err = contractContent.Accept(orgB.CertBundle.RootCAs(), orgB.CertBundle.Cert(), testClock.Now())
	assert.NoError(t, err)

	cc, err = rest.ContractContentToAPIModel(contractContent)
	assert.NoError(t, err)

	resp, err = clientOrgB.AcceptContractWithResponse(context.Background(), contractContent.Hash().String(), &models.AcceptContractParams{
		FscManagerAddress: orgB.ManagerAddress,
	}, models.AcceptContractJSONRequestBody{
		ContractContent: *cc,
		Signature:       acceptSignatureOrgB.JWS(),
	})
	assert.NoError(t, err)
	assert.Equal(t, http.StatusCreated, resp.StatusCode())

	tokenExpirationDate := testClock.Now().Add(accessTokenTTL)

	// Act
	tokenResp, errReq := clientOrgB.GetTokenWithFormdataBodyWithResponse(context.Background(), models.GetTokenFormdataRequestBody{
		GrantType: models.ClientCredentials,
		Scope:     contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
	})

	// Assert
	assert.NoError(t, errReq)
	assert.Equal(t, http.StatusOK, tokenResp.StatusCode())

	receivedToken, errTokenDecode := accesstoken.DecodeFromString(testClock, peerCert, tokenResp.JSON200.AccessToken)
	assert.NoError(t, errTokenDecode)

	assert.Equal(t, &accesstoken.DecodedToken{
		GroupID:                     contractContent.GroupID(),
		GrantHash:                   contractContent.Grants().ServiceConnectionGrants()[0].Hash().String(),
		OutwayPeerID:                orgB.CertBundle.GetOrganizationInfo().SerialNumber,
		OutwayCertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
		ServiceName:                 "parkeerrechten",
		ServiceInwayAddress:         "https://inway.address.com:443",
		ServicePeerID:               orgA.CertBundle.GetOrganizationInfo().SerialNumber,
		ServiceDelegatorPeerID:      orgC.CertBundle.GetOrganizationInfo().SerialNumber,
		ExpiryDate:                  tokenExpirationDate.Add(time.Duration(-1 * tokenExpirationDate.Nanosecond())), // since unix timestamps do not support nanoseconds
	}, receivedToken)
}
