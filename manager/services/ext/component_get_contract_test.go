// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"
	"time"

	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
	internal_models "go.nlx.io/nlx/manager/ports/int/rest/api/models"
)

// Manager-Contracts-1
//
//nolint:funlen // this is a test
func TestGetContractServiceConnectionGrant(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name())
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now(),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                contractContent.ID().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:          contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				ServicePeerID:         contractContent.Grants().ServiceConnectionGrants()[0].Service().Peer().ID().Value(),
				ServiceName:           contractContent.Grants().ServiceConnectionGrants()[0].Service().Name(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	// Act
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantHash: &[]string{contractContent.Grants().ServiceConnectionGrants()[0].Hash().String()},
	})

	// Assert
	// assert all field in the contract, see submit_contract test for asserting 1 field
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.ID().String(), contractResponse.JSON200.Contracts[0].Content.Id)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	// Asserting grants
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	serviceConnectionGrantType, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.Discriminator()
	assert.NoError(t, err)
	assert.Equal(t, "GRANT_TYPE_SERVICE_CONNECTION", serviceConnectionGrantType)

	serviceConnectionGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantServiceConnection()
	assert.NoError(t, err)

	assert.Equal(t, orgB.GetPeerID(), serviceConnectionGrant.Outway.PeerId)
	assert.Equal(t, orgB.CertBundle.CertificateThumbprint(), serviceConnectionGrant.Outway.CertificateThumbprint)
	assert.Equal(t, orgA.GetPeerID(), serviceConnectionGrant.Service.PeerId)
	assert.Equal(t, contractContent.Grants().ServiceConnectionGrants()[0].Service().Name(), serviceConnectionGrant.Service.Name)

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))
}

// Manager-Contracts-2
//
//nolint:funlen,dupl // this is a test
func TestGetContractWithPeerNotInContract(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name())
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgC.CertBundle) // client with peer not part of contract
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now(),
		},
		Grants: []interface{}{
			&contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgB.GetPeerID(),
					},
					CertificateThumbprint: orgB.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: orgA.GetPeerID(),
					},
					Name: "parkeerrechten",
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                contractContent.ID().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServiceConnectionArgs{
				CertificateThumbprint: contractContent.Grants().ServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:          contractContent.Grants().ServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				ServicePeerID:         contractContent.Grants().ServiceConnectionGrants()[0].Service().Peer().ID().Value(),
				ServiceName:           contractContent.Grants().ServiceConnectionGrants()[0].Service().Name(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	// Act
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{})

	// Assert
	// assert that no contracts are returned since the peer requesting the contracts is not part of the contract
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts))
}

//nolint:funlen // this is a test
func TestGetContractMultipleGrants(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name())
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgA.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now(),
		},
		Grants: []interface{}{
			&contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             orgA.GetPeerID(),
						Name:           "test-directory",
						ManagerAddress: "https://test-directory.example.com:443",
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Name: "test-service",
					Peer: &contract.NewPeerArgs{
						ID:   orgA.GetPeerID(),
						Name: orgA.GetName(),
					},
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:   orgB.GetPeerID(),
						Name: orgB.GetName(),
					},
				},
			},
			&contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID:   orgC.GetPeerID(),
						Name: orgC.GetName(),
					},
					CertificateThumbprint: orgC.CertBundle.CertificateThumbprint(),
				},
				Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:   orgA.GetPeerID(),
						Name: orgA.GetName(),
					},
					Name: "test-service",
				},
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID:   orgB.GetPeerID(),
						Name: orgB.GetName(),
					},
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                contractContent.ID().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantDelegatedServicePublicationArgs{
				DirectoryPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Name(),
				ServicePeerID:   contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceProtocol: contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Protocol(),
				DelegatorPeerID: contractContent.Grants().DelegatedServicePublicationGrants()[0].Delegator().Peer().ID().Value(),
			},
			&command.GrantDelegatedServiceConnectionArgs{
				OutwayCertificateThumbprint: contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(),
				OutwayPeerID:                contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().Peer().ID().Value(),
				ServicePeerID:               contractContent.Grants().DelegatedServiceConnectionGrants()[0].Service().Peer().ID().Value(),
				ServiceName:                 contractContent.Grants().DelegatedServiceConnectionGrants()[0].Service().Name(),
				DelegatorPeerID:             contractContent.Grants().DelegatedServiceConnectionGrants()[0].Delegator().Peer().ID().Value(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	// Act
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{})

	// Assert
	// assert all field in the contract, see submit_contract test for asserting 1 field
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.ID().String(), contractResponse.JSON200.Contracts[0].Content.Id)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	// Asserting grants
	assert.Equal(t, 2, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	delegatedServiceConnectionGrantType, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.Discriminator()
	assert.NoError(t, err)
	assert.Equal(t, "GRANT_TYPE_DELEGATED_SERVICE_CONNECTION", delegatedServiceConnectionGrantType)

	delegatedServicePublicationGrantType, err := contractResponse.JSON200.Contracts[0].Content.Grants[1].Data.Discriminator()
	assert.NoError(t, err)
	assert.Equal(t, "GRANT_TYPE_DELEGATED_SERVICE_PUBLICATION", delegatedServicePublicationGrantType)

	delegatedServiceConnectionGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantDelegatedServiceConnection()
	assert.NoError(t, err)

	assert.Equal(t, orgA.GetPeerID(), delegatedServiceConnectionGrant.Service.PeerId)
	assert.Equal(t, contractContent.Grants().DelegatedServiceConnectionGrants()[0].Service().Name(), delegatedServiceConnectionGrant.Service.Name)
	assert.Equal(t, contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().Peer().ID().Value(), delegatedServiceConnectionGrant.Outway.PeerId)
	assert.Equal(t, contractContent.Grants().DelegatedServiceConnectionGrants()[0].Outway().CertificateThumbprint().Value(), delegatedServiceConnectionGrant.Outway.CertificateThumbprint)
	assert.Equal(t, contractContent.Grants().DelegatedServiceConnectionGrants()[0].Delegator().Peer().ID().Value(), delegatedServiceConnectionGrant.Delegator.PeerId)

	serviceDelegatedPublicationGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[1].Data.AsFSCCoreGrantDelegatedServicePublication()
	assert.NoError(t, err)

	assert.Equal(t, orgB.GetPeerID(), serviceDelegatedPublicationGrant.Delegator.PeerId)
	assert.Equal(t, orgA.GetPeerID(), serviceDelegatedPublicationGrant.Service.PeerId)
	assert.Equal(t, contractContent.Grants().DelegatedServicePublicationGrants()[0].Service().Name(), serviceDelegatedPublicationGrant.Service.Name)
	assert.Equal(t, orgB.GetPeerID(), serviceDelegatedPublicationGrant.Directory.PeerId)

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))

}

//nolint:funlen // this is a test
func TestGetContractWithGrantType(t *testing.T) {
	t.Parallel()

	externalHTTPServer, application := newService(t.Name())
	defer externalHTTPServer.Close()

	// Arrange
	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgA.CertBundle)
	assert.NoError(t, err)

	contractID, err := uuid.New().MarshalBinary()
	assert.NoError(t, err)

	contractContent, err := contract.NewContent(&contract.NewContentArgs{
		HashAlgorithm: contract.HashAlgSHA3_512,
		ID:            contractID,
		CreatedAt:     time.Now(),
		GroupID:       "fsc-local",
		Validity: &contract.NewValidityArgs{
			NotBefore: time.Now(),
			NotAfter:  time.Now(),
		},
		Grants: []interface{}{
			&contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID:             orgB.GetPeerID(),
						Name:           "",
						ManagerAddress: "",
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID:             orgA.GetPeerID(),
						Name:           orgA.GetName(),
						ManagerAddress: "",
					},
					Name:     "parkeerrechten",
					Protocol: contract.ServiceProtocolTCPHTTP1_1,
				},
			},
		},
	})
	assert.NoError(t, err)

	_, err = application.Commands.CreateContract.Handle(context.Background(), &command.CreateContractHandlerArgs{
		HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
		ID:                contractContent.ID().String(),
		GroupID:           contractContent.GroupID(),
		ContractNotBefore: contractContent.NotBefore(),
		ContractNotAfter:  contractContent.NotAfter(),
		Grants: []interface{}{
			&command.GrantServicePublicationArgs{
				DirectoryPeerID: contractContent.Grants().ServicePublicationGrants()[0].Directory().Peer().ID().Value(),
				ServicePeerID:   contractContent.Grants().ServicePublicationGrants()[0].Service().Peer().ID().Value(),
				ServiceName:     contractContent.Grants().ServicePublicationGrants()[0].Service().Name(),
				ServiceProtocol: contractContent.Grants().ServicePublicationGrants()[0].Service().Protocol(),
			},
		},
		CreatedAt: time.Now(),
	})
	assert.NoError(t, err)

	// Act
	grantType := models.FSCCoreGrantType("GRANT_TYPE_SERVICE_PUBLICATION")
	contractResponse, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
		GrantType: &grantType,
	})

	// Assert
	// assert all field in the contract, see submit_contract test for asserting 1 field
	assert.NoError(t, err)
	assert.Equal(t, http.StatusOK, contractResponse.StatusCode())
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts))
	assert.Equal(t, contractContent.ID().String(), contractResponse.JSON200.Contracts[0].Content.Id)
	assert.Equal(t, contractContent.GroupID(), contractResponse.JSON200.Contracts[0].Content.GroupId)
	assert.Equal(t, contractContent.NotBefore().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotBefore)
	assert.Equal(t, contractContent.NotAfter().Unix(), contractResponse.JSON200.Contracts[0].Content.Validity.NotAfter)

	// Asserting grants
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Content.Grants))

	assert.NoError(t, err)

	servicePublicationGrant, err := contractResponse.JSON200.Contracts[0].Content.Grants[0].Data.AsFSCCoreGrantServicePublication()
	assert.NoError(t, err)

	assert.Equal(t, contractContent.Grants().ServicePublicationGrants()[0].Directory().Peer().ID().Value(), servicePublicationGrant.Directory.PeerId)
	assert.Equal(t, contractContent.Grants().ServicePublicationGrants()[0].Service().Name(), servicePublicationGrant.Service.Name)
	assert.Equal(t, contractContent.Grants().ServicePublicationGrants()[0].Service().Peer().ID().Value(), servicePublicationGrant.Service.PeerId)
	assert.Equal(t, contractContent.Grants().ServicePublicationGrants()[0].Service().Protocol().String(), string(servicePublicationGrant.Service.Protocol))

	// Asserting signatures
	assert.Equal(t, 1, len(contractResponse.JSON200.Contracts[0].Signatures.Accept))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Revoke))
	assert.Equal(t, 0, len(contractResponse.JSON200.Contracts[0].Signatures.Reject))
}
