// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//go:build integration

package externalservice_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/manager/apps/int/command"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
	internal_models "go.nlx.io/nlx/manager/ports/int/rest/api/models"
	testhash "go.nlx.io/nlx/testing/hash"
	testsignature "go.nlx.io/nlx/testing/signature"
)

//nolint:funlen // this is a test
func TestSuiteAcceptContact(t *testing.T) {
	testCases := map[string]struct {
		Description           string
		ContractToSignArgs    *command.CreateContractHandlerArgs
		RequestURLHash        string
		AcceptContractArgs    models.AcceptContractJSONRequestBody
		WantStatusCode        int
		WantResponseDomain    models.FSCCoreErrorDomain
		WantResponseErrorCode string
	}{
		"Manager-AcceptContract-1": {
			Description: "Place accept signature on a Contract",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				ID:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now(),
				ContractNotAfter:  testClock.Now(),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						OutwayPeerID:          orgA.GetPeerID(),
						ServicePeerID:         orgB.GetPeerID(),
						ServiceName:           "parkeerrechten",
					},
				},
				CreatedAt: testClock.Now(),
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			AcceptContractArgs: func() models.AcceptContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateAccept(contentHash, orgB.CertBundle.CertificateThumbprint(), orgB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						PeerId:                orgA.GetPeerID(),
					},
					Service: models.FSCCoreService{
						Name:   "parkeerrechten",
						PeerId: orgB.GetPeerID(),
					},
				})

				return models.AcceptContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Id:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusCreated,
			WantResponseDomain:    "",
			WantResponseErrorCode: "",
		},
		"Manager-AcceptContract-2": {
			Description: "Place accept signature on a Contract without being a Peer on the Contract",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				ID:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now(),
				ContractNotAfter:  testClock.Now(),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						OutwayPeerID:          orgA.GetPeerID(),
						ServicePeerID:         orgA.GetPeerID(),
						ServiceName:           "parkeerrechten",
					},
				},
				CreatedAt: testClock.Now(),
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgA.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			AcceptContractArgs: func() models.AcceptContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgA.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateAccept(contentHash, orgB.CertBundle.CertificateThumbprint(), orgB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						PeerId:                orgA.GetPeerID(),
					},
					Service: models.FSCCoreService{
						Name:   "parkeerrechten",
						PeerId: orgA.GetPeerID(),
					},
				})

				return models.AcceptContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Id:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODEPEERNOTPARTOFCONTRACT),
		},
		"Manager-AcceptContract-3": {
			Description: "Place accept signature which is not a valid JWS",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				ID:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now(),
				ContractNotAfter:  testClock.Now(),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						OutwayPeerID:          orgA.GetPeerID(),
						ServicePeerID:         orgB.GetPeerID(),
						ServiceName:           "parkeerrechten",
					},
				},
				CreatedAt: testClock.Now(),
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			AcceptContractArgs: func() models.AcceptContractJSONRequestBody {
				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						PeerId:                orgA.GetPeerID(),
					},
					Service: models.FSCCoreService{
						Name:   "parkeerrechten",
						PeerId: orgB.GetPeerID(),
					},
				})

				return models.AcceptContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Id:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: "not-a-valid-jws",
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODESIGNATUREVERIFICATIONFAILED),
		},
		"Manager-AcceptContract-4": {
			Description: "Place an accept signature of which the content hash does not match the contract content you are accepting",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				ID:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now(),
				ContractNotAfter:  testClock.Now(),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						OutwayPeerID:          orgA.GetPeerID(),
						ServicePeerID:         orgB.GetPeerID(),
						ServiceName:           "parkeerrechten",
					},
				},
				CreatedAt: testClock.Now(),
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			AcceptContractArgs: func() models.AcceptContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "a-different-group-id",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateAccept(contentHash, orgB.CertBundle.CertificateThumbprint(), orgB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						PeerId:                orgA.GetPeerID(),
					},
					Service: models.FSCCoreService{
						Name:   "parkeerrechten",
						PeerId: orgB.GetPeerID(),
					},
				})

				return models.AcceptContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Id:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODESIGNATURECONTRACTCONTENTHASHMISMATCH),
		},
		"Manager-AcceptContract-5": {
			Description: "Place accept signature on a Contract with a signature type that is not accept",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				ID:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now(),
				ContractNotAfter:  testClock.Now(),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						OutwayPeerID:          orgA.GetPeerID(),
						ServicePeerID:         orgB.GetPeerID(),
						ServiceName:           "parkeerrechten",
					},
				},
				CreatedAt: testClock.Now(),
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			AcceptContractArgs: func() models.AcceptContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				revokeSig, err := testsignature.CreateRevoke(contentHash, orgB.CertBundle.CertificateThumbprint(), orgB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						PeerId:                orgA.GetPeerID(),
					},
					Service: models.FSCCoreService{
						Name:   "parkeerrechten",
						PeerId: orgB.GetPeerID(),
					},
				})

				return models.AcceptContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Id:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: revokeSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODESIGNATUREVERIFICATIONFAILED),
		},
		"Manager-AcceptContract-6": {
			Description: "The hash in the request URL does not match with the contract hash of the contract in the request body",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				ID:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now(),
				ContractNotAfter:  testClock.Now(),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						OutwayPeerID:          orgA.GetPeerID(),
						ServicePeerID:         orgB.GetPeerID(),
						ServiceName:           "parkeerrechten",
					},
				},
				CreatedAt: testClock.Now(),
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20830",
					GroupID:        "fsc-local-different-group-id",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			AcceptContractArgs: func() models.AcceptContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateAccept(contentHash, orgB.CertBundle.CertificateThumbprint(), orgB.CertBundle.Cert().PrivateKey, testClock.Now())
				assert.NoError(t, err)

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						PeerId:                orgA.GetPeerID(),
					},
					Service: models.FSCCoreService{
						Name:   "parkeerrechten",
						PeerId: orgB.GetPeerID(),
					},
				})

				return models.AcceptContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Id:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: string(models.ERRORCODEURLPATHCONTENTHASHMISMATCH),
		},
		"Manager-AcceptContract-7": {
			Description: "Place accept signature with unsupported algorithm on a Contract",
			ContractToSignArgs: &command.CreateContractHandlerArgs{
				HashAlgorithm:     string(internal_models.HASHALGORITHMSHA3512),
				ID:                "0537b6e0-246f-41ca-b4a0-e021b6a20838",
				GroupID:           "fsc-local",
				ContractNotBefore: testClock.Now(),
				ContractNotAfter:  testClock.Now(),
				Grants: []interface{}{
					&command.GrantServiceConnectionArgs{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						OutwayPeerID:          orgA.GetPeerID(),
						ServicePeerID:         orgB.GetPeerID(),
						ServiceName:           "parkeerrechten",
					},
				},
				CreatedAt: testClock.Now(),
			},
			RequestURLHash: func() string {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				return contentHash
			}(),
			AcceptContractArgs: func() models.AcceptContractJSONRequestBody {
				contentHash, err := testhash.GenerateContractContentHash(&testhash.ContractContentArgs{
					ID:             "0537b6e0-246f-41ca-b4a0-e021b6a20838",
					GroupID:        "fsc-local",
					HashAlgorithm:  int32(contract.HashAlgSHA3_512),
					ValidNotBefore: testClock.Now().Unix(),
					ValidNotAfter:  testClock.Now().Unix(),
					ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
						{
							OutwayPeerID:                orgA.GetPeerID(),
							OutwayCertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
							ServicePeerID:               orgB.GetPeerID(),
							ServiceName:                 "parkeerrechten",
						},
					},
					CreatedAt: testClock.Now().Unix(),
				})
				assert.NoError(t, err)

				acceptSig, err := testsignature.CreateHMACAccept(contentHash, orgB.CertBundle.CertificateThumbprint(), testClock.Now())
				assert.NoError(t, err)

				grant := models.FSCCoreGrant_Data{}

				_ = grant.FromFSCCoreGrantServiceConnection(models.FSCCoreGrantServiceConnection{
					Type: models.GRANTTYPESERVICECONNECTION,
					Outway: models.FSCCoreOutway{
						CertificateThumbprint: orgA.CertBundle.CertificateThumbprint(),
						PeerId:                orgA.GetPeerID(),
					},
					Service: models.FSCCoreService{
						Name:   "parkeerrechten",
						PeerId: orgB.GetPeerID(),
					},
				})

				return models.AcceptContractJSONRequestBody{
					ContractContent: models.FSCCoreContractContent{
						CreatedAt:     testClock.Now().Unix(),
						Grants:        []models.FSCCoreGrant{{Data: grant}},
						GroupId:       "fsc-local",
						HashAlgorithm: models.HASHALGORITHMSHA3512,
						Id:            "0537b6e0-246f-41ca-b4a0-e021b6a20838",
						Validity: models.FSCCoreValidity{
							NotAfter:  testClock.Now().Unix(),
							NotBefore: testClock.Now().Unix(),
						},
					},
					Signature: acceptSig,
				}
			}(),
			WantStatusCode:        http.StatusUnprocessableEntity,
			WantResponseDomain:    models.ERRORDOMAINMANAGER,
			WantResponseErrorCode: "ERROR_CODE_UNKNOWN_ALGORITHM_SIGNATURE",
		},
	}

	externalHTTPServer, intApp := newService(t.Name())

	defer externalHTTPServer.Close()

	client, err := createExternalManagerAPIClient(externalHTTPServer.URL, orgB.CertBundle)
	assert.NoError(t, err)

	for name, testCase := range testCases {
		tc := testCase

		t.Run(name, func(t *testing.T) {
			if tc.ContractToSignArgs != nil {
				_, err := intApp.Commands.CreateContract.Handle(context.Background(), tc.ContractToSignArgs)
				assert.NoError(t, err)
			}

			resp, errSubmit := client.AcceptContractWithResponse(context.Background(), tc.RequestURLHash, &models.AcceptContractParams{
				FscManagerAddress: orgB.ManagerAddress,
			}, tc.AcceptContractArgs)
			assert.NoError(t, errSubmit)

			if !assert.Equal(t, testCase.WantStatusCode, resp.StatusCode()) {
				t.Errorf("response body: %s", resp.Body)
			}

			if testCase.WantResponseDomain != "" || testCase.WantResponseErrorCode != "" {
				switch testCase.WantStatusCode {
				case http.StatusBadRequest:
					assert.Equal(t, testCase.WantResponseErrorCode, resp.JSON400.Code)
					assert.Equal(t, testCase.WantResponseDomain, resp.JSON400.Domain)

				case http.StatusUnprocessableEntity:
					assert.Equal(t, testCase.WantResponseErrorCode, resp.JSON422.Code)
					assert.Equal(t, testCase.WantResponseDomain, resp.JSON422.Domain)

				default:
					t.Fatalf("unexpected status code %d", resp.StatusCode())
				}
			}

			if testCase.WantStatusCode != http.StatusCreated {
				return
			}

			var limit = 20

			contracts, err := client.GetContractsWithResponse(context.Background(), &models.GetContractsParams{
				Limit: &limit,
			})
			assert.NoError(t, err)

			var acceptedContract models.FSCCoreContract

			for _, c := range contracts.JSON200.Contracts {
				if c.Content.Id != testCase.AcceptContractArgs.ContractContent.Id {
					continue
				}

				acceptedContract = c
			}

			assert.NotNil(t, acceptedContract)
			assert.Contains(t, acceptedContract.Signatures.Accept, orgB.GetPeerID())
			assert.Equal(t, testCase.AcceptContractArgs.Signature, acceptedContract.Signatures.Accept[orgB.GetPeerID()])
		})
	}
}
