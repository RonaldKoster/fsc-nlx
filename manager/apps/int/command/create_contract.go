// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package command

import (
	"context"
	"crypto/tls"
	"crypto/x509"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"

	"go.nlx.io/nlx/common/logger"
	internalapp_errors "go.nlx.io/nlx/manager/apps/int/errors"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/internal/peers"
	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
)

type Clock interface {
	Now() time.Time
}

type NewCreateContractHandlerArgs struct {
	TrustedExternalRootCAs *x509.CertPool
	Repository             contract.Repository
	Peers                  *peers.Peers
	Clock                  Clock
	Logger                 *logger.Logger
	SignatureCert          *tls.Certificate
	SelfPeerID             *contract.PeerID
}

type CreateContractHandler struct {
	trustedExternalRootCAs *x509.CertPool
	clock                  Clock
	repository             contract.Repository
	logger                 *logger.Logger
	peers                  *peers.Peers
	signatureCert          *tls.Certificate
	selfPeerID             *contract.PeerID
}

func NewCreateContractHandler(args *NewCreateContractHandlerArgs) (*CreateContractHandler, error) {
	if args.TrustedExternalRootCAs == nil {
		return nil, errors.New("trustedRootCA's is required")
	}

	if args.Repository == nil {
		return nil, errors.New("repository is required")
	}

	if args.Peers == nil {
		return nil, errors.New("peers is required")
	}

	if args.Clock == nil {
		return nil, errors.New("clock is required")
	}

	if args.Logger == nil {
		return nil, errors.New("logger is required")
	}

	if args.SignatureCert == nil {
		return nil, errors.New("signature cert is required")
	}

	return &CreateContractHandler{
		trustedExternalRootCAs: args.TrustedExternalRootCAs,
		repository:             args.Repository,
		peers:                  args.Peers,
		clock:                  args.Clock,
		logger:                 args.Logger,
		signatureCert:          args.SignatureCert,
		selfPeerID:             args.SelfPeerID,
	}, nil
}

type GrantServiceConnectionArgs struct {
	CertificateThumbprint string
	OutwayPeerID          string
	ServicePeerID         string
	ServiceName           string
}

type GrantServicePublicationArgs struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol contract.ServiceProtocol
}

type GrantDelegatedServiceConnectionArgs struct {
	OutwayCertificateThumbprint string
	OutwayPeerID                string
	ServicePeerID               string
	ServiceName                 string
	DelegatorPeerID             string
}

type GrantDelegatedServicePublicationArgs struct {
	DelegatorPeerID string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol contract.ServiceProtocol
}

type CreateContractHandlerArgs struct {
	HashAlgorithm     string
	ID                string
	GroupID           string
	ContractNotBefore time.Time
	ContractNotAfter  time.Time
	Grants            []interface{}
	CreatedAt         time.Time
}

func (h *CreateContractHandler) Handle(ctx context.Context, args *CreateContractHandlerArgs) (string, error) {
	grants := make([]interface{}, len(args.Grants))

	for i, grant := range args.Grants {
		switch g := grant.(type) {
		case *GrantServicePublicationArgs:
			grants[i] = &contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DirectoryPeerID,
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
					Name:     g.ServiceName,
					Protocol: g.ServiceProtocol,
				},
			}
		case *GrantServiceConnectionArgs:
			grants[i] = &contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.OutwayPeerID,
					},
					CertificateThumbprint: g.CertificateThumbprint,
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Name: g.ServiceName,
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
				},
			}
		case *GrantDelegatedServiceConnectionArgs:
			grants[i] = &contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.OutwayPeerID,
					},
					CertificateThumbprint: g.OutwayCertificateThumbprint,
				},
				Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Name: g.ServiceName,
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
				},
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DelegatorPeerID,
					},
				},
			}
		case *GrantDelegatedServicePublicationArgs:
			grants[i] = &contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DirectoryPeerID,
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
					Name:     g.ServiceName,
					Protocol: g.ServiceProtocol,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DelegatorPeerID,
					},
				},
			}
		default:
			return "", fmt.Errorf("unknown grant type %T", g)
		}
	}

	contractID, err := uuid.Parse(args.ID)
	if err != nil {
		return "", fmt.Errorf("could not parse contract ID. %s", err)
	}

	uuidBytes, err := contractID.MarshalBinary()
	if err != nil {
		return "", fmt.Errorf("could not get bytes from uuid. %s", err)
	}

	hashAlg, err := hashAlgToModel(args.HashAlgorithm)
	if err != nil {
		return "", err
	}

	model, err := contract.NewContent(
		&contract.NewContentArgs{
			HashAlgorithm: *hashAlg,
			ID:            uuidBytes,
			GroupID:       args.GroupID,
			Validity: &contract.NewValidityArgs{
				NotBefore: args.ContractNotBefore,
				NotAfter:  args.ContractNotAfter,
			},
			Grants:    grants,
			CreatedAt: args.CreatedAt,
		},
	)
	if err != nil {
		return "", internalapp_errors.NewIncorrectInputError(err)
	}

	if !model.ContainsPeer(*h.selfPeerID) {
		return "", internalapp_errors.NewIncorrectInputError(fmt.Errorf("the peer creating contract is not part of the contract. peerID %q ", h.selfPeerID.Value()))
	}

	// sign own content
	sig, err := model.Accept(h.trustedExternalRootCAs, h.signatureCert, h.clock.Now())
	if err != nil {
		return "", fmt.Errorf("unable to sign content %v", err)
	}

	err = h.repository.UpsertContent(ctx, model)
	if err != nil {
		h.logger.Error("create content", err)
		return "", err
	}

	err = h.repository.UpsertSignature(ctx, sig)
	if err != nil {
		h.logger.Error("could not upsert signature", err)
		return "", err
	}

	// submit contract to all peers
	err = h.peers.SubmitContract(ctx, model, sig)
	if err != nil {
		h.logger.Error("could not submit contract to peers", err)
		return "", err
	}

	return model.Hash().String(), nil
}

func hashAlgToModel(alg string) (*contract.HashAlg, error) {
	var hashAlgorithm contract.HashAlg

	switch alg {
	case string(models.HASHALGORITHMSHA3512):
		hashAlgorithm = contract.HashAlgSHA3_512
	default:
		return nil, internalapp_errors.NewErrorTypeUnknownHashAlgorithm(fmt.Errorf("unknown hash algorithm"))
	}

	return &hashAlgorithm, nil
}
