// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/controller"
)

type GetServiceEndpointURLHandler struct {
	controller controller.Controller
}

func NewGetServiceEndpointURLHandler(repository controller.Controller) (*GetServiceEndpointURLHandler, error) {
	if repository == nil {
		return nil, errors.New("controller repository is required")
	}

	return &GetServiceEndpointURLHandler{
		controller: repository,
	}, nil
}

type GetServiceEndpointURLHandlerArgs struct {
	InwayAddress string
	ServiceName  string
}

func (h *GetServiceEndpointURLHandler) Handle(ctx context.Context, args *GetServiceEndpointURLHandlerArgs) (string, error) {
	service, err := h.controller.GetService(ctx, args.ServiceName, args.InwayAddress)
	if err != nil {
		return "", errors.Wrapf(err, "failed to get service inway: %s and service name: %s", args.InwayAddress, args.ServiceName)
	}

	return service.EndpointURL.String(), nil
}
