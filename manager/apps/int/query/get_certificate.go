// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/domain/contract"
)

type GetCertificatesHandler struct {
	repository contract.Repository
	selfPeerID contract.PeerID
}

type NewGetCertificatesHandlerArg struct {
	Repository contract.Repository
	SelfPeerID contract.PeerID
}

func NewGetCertificatesHandler(args *NewGetCertificatesHandlerArg) (*GetCertificatesHandler, error) {
	if args.Repository == nil {
		return nil, fmt.Errorf("repository is required")
	}

	if args.SelfPeerID.Value() == "" {
		return nil, fmt.Errorf("peer id is required")
	}

	return &GetCertificatesHandler{
		repository: args.Repository,
		selfPeerID: args.SelfPeerID,
	}, nil
}

func (h *GetCertificatesHandler) Handle(ctx context.Context) (*contract.PeerCertificates, error) {
	certs, err := h.repository.GetPeerCerts(ctx, h.selfPeerID)
	if err != nil {
		return nil, errors.Wrap(err, "could not retrieve certificates from repository")
	}

	return certs, nil
}
