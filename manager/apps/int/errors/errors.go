// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package errors

import "fmt"

type Type struct {
	string
}

var (
	ErrorTypeIncorrectInput       = Type{"incorrect-input"}
	ErrorTypeUnknownHashAlgorithm = Type{"hash-algorithm-unknown"}
)

type Error struct {
	message   string
	errorType Type
}

func (s Error) Error() string {
	return s.message
}

func (s Error) ErrorType() Type {
	return s.errorType
}

func NewIncorrectInputError(err error) Error {
	return Error{
		message:   err.Error(),
		errorType: ErrorTypeIncorrectInput,
	}
}

func NewErrorTypeUnknownHashAlgorithm(err error) Error {
	return Error{
		message:   fmt.Sprintf("hash algorithm unknown: %v", err),
		errorType: ErrorTypeUnknownHashAlgorithm,
	}
}
