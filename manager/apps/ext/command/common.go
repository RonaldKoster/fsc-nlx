// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"fmt"
	"time"

	"go.nlx.io/nlx/manager/apps/ext/errors"
	internalapp_errors "go.nlx.io/nlx/manager/apps/int/errors"
	"go.nlx.io/nlx/manager/domain/contract"
	"go.nlx.io/nlx/manager/ports/ext/rest/api/models"
)

type GrantServiceConnectionArgs struct {
	CertificateThumbprint string
	OutwayPeerID          string
	ServicePeerID         string
	ServiceName           string
}

type GrantServicePublicationArgs struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol contract.ServiceProtocol
}

type GrantDelegatedServiceConnectionArgs struct {
	OutwayCertificateThumbprint string
	OutwayPeerID                string
	ServicePeerID               string
	ServiceName                 string
	DelegatorPeerID             string
}

type GrantDelegatedServicePublicationArgs struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
	ServiceProtocol contract.ServiceProtocol
	DelegatorPeerID string
}

type ContractContentArgs struct {
	HashAlgorithm string
	ID            []byte
	GroupID       string
	NotBefore     time.Time
	NotAfter      time.Time
	Grants        []interface{}
	CreatedAt     time.Time
}

type PeerArgs struct {
	ID             string
	Name           string
	ManagerAddress string
}

func contentToModel(c *ContractContentArgs) (*contract.Content, error) {
	contractContentGrants := make([]interface{}, len(c.Grants))

	for i, grant := range c.Grants {
		switch g := grant.(type) {
		case *GrantServicePublicationArgs:
			contractContentGrants[i] = &contract.NewGrantServicePublicationArgs{
				Directory: &contract.NewGrantServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DirectoryPeerID,
					},
				},
				Service: &contract.NewGrantServicePublicationServiceArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
					Name:     g.ServiceName,
					Protocol: g.ServiceProtocol,
				},
			}
		case *GrantServiceConnectionArgs:
			contractContentGrants[i] = &contract.NewGrantServiceConnectionArgs{
				Outway: &contract.NewGrantServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.OutwayPeerID,
					},
					CertificateThumbprint: g.CertificateThumbprint,
				},
				Service: &contract.NewGrantServiceConnectionServiceArgs{
					Name: g.ServiceName,
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
				},
			}
		case *GrantDelegatedServiceConnectionArgs:
			contractContentGrants[i] = &contract.NewGrantDelegatedServiceConnectionArgs{
				Outway: &contract.NewGrantDelegatedServiceConnectionOutwayArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.OutwayPeerID,
					},
					CertificateThumbprint: g.OutwayCertificateThumbprint,
				},
				Service: &contract.NewGrantDelegatedServiceConnectionServiceArgs{
					Name: g.ServiceName,
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
				},
				Delegator: &contract.NewGrantDelegatedServiceConnectionDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DelegatorPeerID,
					},
				},
			}
		case *GrantDelegatedServicePublicationArgs:
			contractContentGrants[i] = &contract.NewGrantDelegatedServicePublicationArgs{
				Directory: &contract.NewGrantDelegatedServicePublicationDirectoryArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DirectoryPeerID,
					},
				},
				Service: &contract.NewGrantDelegatedServicePublicationServiceArgs{
					Name: g.ServiceName,
					Peer: &contract.NewPeerArgs{
						ID: g.ServicePeerID,
					},
					Protocol: g.ServiceProtocol,
				},
				Delegator: &contract.NewGrantDelegatedServicePublicationDelegatorArgs{
					Peer: &contract.NewPeerArgs{
						ID: g.DelegatorPeerID,
					},
				},
			}
		default:
			return nil, fmt.Errorf("unknown grant type %T", g)
		}
	}

	hashAlg, err := hashAlgToModel(c.HashAlgorithm)
	if err != nil {
		return nil, err
	}

	model, err := contract.NewContent(
		&contract.NewContentArgs{
			HashAlgorithm: *hashAlg,
			ID:            c.ID,
			GroupID:       c.GroupID,
			Validity: &contract.NewValidityArgs{
				NotBefore: c.NotBefore,
				NotAfter:  c.NotAfter,
			},
			Grants:    contractContentGrants,
			CreatedAt: c.CreatedAt,
		},
	)
	if err != nil {
		return nil, err
	}

	return model, nil
}

func peerToModel(p *PeerArgs) (*contract.Peer, error) {
	peer, err := contract.NewPeer(&contract.NewPeerArgs{
		ID:             p.ID,
		Name:           p.Name,
		ManagerAddress: p.ManagerAddress,
	})
	if err != nil {
		return nil, internalapp_errors.NewIncorrectInputError(err)
	}

	if peer.ManagerAddress() == "" {
		return nil, internalapp_errors.NewIncorrectInputError(fmt.Errorf("peer manager address cannot be empty"))
	}

	return peer, nil
}

func hashAlgToModel(alg string) (*contract.HashAlg, error) {
	var hashAlgorithm contract.HashAlg

	switch alg {
	case string(models.HASHALGORITHMSHA3512):
		hashAlgorithm = contract.HashAlgSHA3_512
	default:
		return nil, errors.NewErrorTypeUnknownHashAlgorithm(fmt.Errorf("unknown hash algorithm %q", alg))
	}

	return &hashAlgorithm, nil
}
