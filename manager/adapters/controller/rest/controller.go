// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restcontroller

import (
	"context"
	"fmt"
	"net/http"
	"net/url"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/ports/internalrest/api/models"
	api "go.nlx.io/nlx/controller/ports/internalrest/api/server"
	"go.nlx.io/nlx/manager/adapters/controller"
)

type restController struct {
	client  api.ClientWithResponsesInterface
	groupID string
}

func New(ctx context.Context, groupID string, client api.ClientWithResponsesInterface) (*restController, error) {
	if groupID == "" {
		return nil, errors.New("groupID is required")
	}

	if client == nil {
		return nil, errors.New("client is required")
	}

	return &restController{
		groupID: groupID,
		client:  client,
	}, nil
}

func (m *restController) GetService(ctx context.Context, serviceName, inwayAddress string) (*controller.Service, error) {
	res, err := m.client.GetServiceWithResponse(ctx, m.groupID, serviceName, &models.GetServiceParams{InwayAddress: &inwayAddress})
	if err != nil {
		return nil, errors.Wrap(err, "failed to retrieve service")
	}

	if res.StatusCode() != http.StatusOK {
		return nil, fmt.Errorf("could not retrieve service from rest controller, received invalid status code %d: %s", res.StatusCode(), string(res.Body))
	}

	endpointURL, err := url.Parse(res.JSON200.Service.EndpointUrl)
	if err != nil {
		return nil, fmt.Errorf("got invalid endpoint URL from controller: %w", err)
	}

	return &controller.Service{
		Name:         res.JSON200.Service.Name,
		InwayAddress: res.JSON200.Service.InwayAddress,
		EndpointURL:  endpointURL,
	}, nil
}
