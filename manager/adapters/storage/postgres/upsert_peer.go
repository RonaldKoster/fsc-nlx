// Copyright © VNG Realisatie 2022
// Licensed under the EUPL

package postgresadapter

import (
	"context"
	"database/sql"
	"fmt"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/storage/postgres/queries"
	"go.nlx.io/nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) UpsertPeer(ctx context.Context, p *contract.Peer) error {
	name := p.Name().Value()
	if name == "" {
		return fmt.Errorf("could not upsert peer into database: name is required")
	}

	managerAddress := p.ManagerAddress().Value()
	if managerAddress == "" {
		return fmt.Errorf("could not upsert peer into database: manager address is required")
	}

	err := r.queries.UpsertPeer(ctx, &queries.UpsertPeerParams{
		ID:             p.ID().Value(),
		Name:           sql.NullString{Valid: true, String: name},
		ManagerAddress: sql.NullString{Valid: true, String: managerAddress},
	})
	if err != nil {
		return errors.Wrap(err, "could not upsert peer into database")
	}

	return nil
}
