-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListPeersByName :many
SELECT
    p.id,
    p.name,
    p.manager_address
FROM peers.peers as p
WHERE
    p.name IS NOT NULL AND
    p.manager_address IS NOT NULL AND
    lower(p.name) like lower(@name::text) AND
    (
             @pagination_start_id::text = ''
        OR
            (@order_direction::text = 'asc' AND p.id > @pagination_start_id::text)
        OR
            (@order_direction::text = 'desc' AND p.id < @pagination_start_id::text)
    )
ORDER BY
    CASE
        WHEN @order_direction::text = 'asc' THEN p.id END ASC,
    CASE
        WHEN @order_direction::text = 'desc' THEN p.id END DESC
LIMIT $1;
