// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresadapter

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/manager/adapters/storage/postgres/queries"
	"go.nlx.io/nlx/manager/domain/contract"
)

func (r *PostgreSQLRepository) ListPeers(ctx context.Context, paginationStartID string, paginationLimit uint32, paginationSortOrder contract.SortOrder) ([]*contract.Peer, error) {
	var rows []*queries.PeersPeer

	var err error

	rows, err = r.queries.ListPeers(ctx, &queries.ListPeersParams{
		Limit:             int32(paginationLimit),
		PaginationStartID: paginationStartID,
		OrderDirection:    string(paginationSortOrder),
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not list peers from database")
	}

	peers := make([]*contract.Peer, len(rows))

	if len(rows) == 0 {
		return peers, nil
	}

	for i, row := range rows {
		p, errNewPeer := contract.NewPeer(&contract.NewPeerArgs{
			ID:             row.ID,
			Name:           row.Name.String,
			ManagerAddress: row.ManagerAddress.String,
		})
		if errNewPeer != nil {
			return nil, errors.Wrap(errNewPeer, "invalid peer in database")
		}

		peers[i] = p
	}

	return peers, nil
}
