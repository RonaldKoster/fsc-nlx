// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"
	common_tls "go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/manager/adapters/manager"
)

type Factory struct {
	certs       *common_tls.CertificateBundle
	selfAddress string
}

func NewFactory(certs *common_tls.CertificateBundle, selfAddress string) *Factory {
	return &Factory{certs: certs, selfAddress: selfAddress}
}

type NewManagerArgs struct {
}

func (f *Factory) New(ctx context.Context, address string) (manager.Manager, error) {
	return NewClient(f.selfAddress, address, f.certs)
}
