// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/app/apiapp/query"
	"go.nlx.io/nlx/controller/pkg/authentication"
	"go.nlx.io/nlx/controller/ports/rest/api"
	"go.nlx.io/nlx/controller/ports/rest/api/models"
)

func (s *Server) GetServices(ctx context.Context, req api.GetServicesRequestObject) (api.GetServicesResponseObject, error) {
	s.logger.Info("rest request GetServices")

	services, err := s.app.Queries.ListServices.Handle(ctx, &query.ListServicesArgs{
		AuthData: &authentication.NoneData{},
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not obtain available services")
	}

	resp := api.GetServices200JSONResponse{}

	for _, s := range services {
		endpointURL := s.EndpointURL.String()

		resp.Services = append(resp.Services, models.Service{
			Name:         s.Name,
			EndpointUrl:  endpointURL,
			InwayAddress: s.InwayAddress,
		})
	}

	return resp, nil
}
