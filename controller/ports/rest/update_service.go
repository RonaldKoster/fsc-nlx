// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/app/apiapp/command"
	"go.nlx.io/nlx/controller/pkg/authentication"
	"go.nlx.io/nlx/controller/ports/rest/api"
)

func (s *Server) UpdateService(ctx context.Context, req api.UpdateServiceRequestObject) (api.UpdateServiceResponseObject, error) {
	s.logger.Info("rest request UpdateService")

	err := s.app.Commands.UpdateService.Handle(ctx, &command.UpdateServiceArgs{
		AuthData:     &authentication.NoneData{},
		Name:         req.Name,
		EndpointURL:  req.Body.EndpointUrl,
		InwayAddress: req.Body.InwayAddress,
	})
	if err != nil {
		if errors.Is(err, command.ErrServiceNotFound) {
			return api.UpdateService404Response{}, nil
		}

		return nil, errors.Wrap(err, "could not update service")
	}

	return api.UpdateService204Response{}, nil
}
