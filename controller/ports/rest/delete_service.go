// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package rest

import (
	"context"

	"github.com/pkg/errors"

	"go.nlx.io/nlx/controller/app/apiapp/command"
	"go.nlx.io/nlx/controller/pkg/authentication"
	"go.nlx.io/nlx/controller/ports/rest/api"
)

func (s *Server) DeleteService(ctx context.Context, req api.DeleteServiceRequestObject) (api.DeleteServiceResponseObject, error) {
	s.logger.Info("rest request DeleteService")

	err := s.app.Commands.DeleteService.Handle(ctx, &command.DeleteServiceArgs{
		AuthData: &authentication.NoneData{},
		Name:     req.Name,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create service")
	}

	return api.DeleteService204Response{}, nil
}
