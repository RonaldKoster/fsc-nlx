// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalrest

import (
	"context"

	"go.nlx.io/nlx/controller/ports/internalrest/api/models"
	api "go.nlx.io/nlx/controller/ports/internalrest/api/server"
)

func (s *Server) GetService(ctx context.Context, req api.GetServiceRequestObject) (api.GetServiceResponseObject, error) {
	s.logger.Info("rest request GetService")

	var inwayAddress string

	if req.Params.InwayAddress != nil {
		inwayAddress = *req.Params.InwayAddress
	}

	service, err := s.app.Queries.GetService.Handle(ctx, req.GroupId, req.ServiceName, inwayAddress)
	if err != nil {
		s.logger.Error("error executing get service query", err)
		return nil, err
	}

	return api.GetService200JSONResponse{
		Service: models.Service{
			GroupId:      service.GroupID,
			Name:         service.Name,
			InwayAddress: service.InwayAddress,
			EndpointUrl:  service.EndpointURL.String(),
		},
	}, nil
}
