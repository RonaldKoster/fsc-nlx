// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalrest

import (
	"context"

	"go.nlx.io/nlx/controller/app/internalapp/commands"
	api "go.nlx.io/nlx/controller/ports/internalrest/api/server"
)

func (s *Server) RegisterInway(ctx context.Context, req api.RegisterInwayRequestObject) (api.RegisterInwayResponseObject, error) {
	s.logger.Info("rest request RegisterInway")

	err := s.app.Commands.RegisterInway.Handle(ctx, &commands.RegisterInwayArgs{
		GroupID: req.GroupId,
		Name:    req.InwayName,
		Address: req.Body.Address,
	})
	if err != nil {
		s.logger.Error("error executing register inway command", err)
		return nil, err
	}

	return api.RegisterInway204Response{}, nil
}
