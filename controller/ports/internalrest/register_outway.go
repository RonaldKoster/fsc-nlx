// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package internalrest

import (
	"context"

	"go.nlx.io/nlx/controller/app/internalapp/commands"
	api "go.nlx.io/nlx/controller/ports/internalrest/api/server"
)

func (s *Server) RegisterOutway(ctx context.Context, req api.RegisterOutwayRequestObject) (api.RegisterOutwayResponseObject, error) {
	s.logger.Info("rest request RegisterOutway")

	err := s.app.Commands.RegisterOutway.Handle(ctx, &commands.RegisterOutwayArgs{
		GroupID:               req.GroupId,
		Name:                  req.OutwayName,
		CertificateThumbprint: req.Body.CertificateThumbprint,
	})
	if err != nil {
		s.logger.Error("error executing register outway command", err)
		return nil, err
	}

	return api.RegisterOutway204Response{}, nil
}
