// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package authentication

type Data any

type NoneData struct {
	ID string `json:"email"`
}

type OIDCData struct {
	Email string `json:"email"`
}

type AuthenticationError struct {
	m string
}

func newAuthenticationError(message string) *AuthenticationError {
	return &AuthenticationError{
		m: message,
	}
}

func (v *AuthenticationError) Error() string {
	return "authentication error: " + v.m
}

func Authenticate(a Data) error {
	if a == nil {
		return newAuthenticationError("unauthenticated")
	}

	switch a.(type) {
	case *OIDCData:
		return nil
	case *NoneData:
		return nil
	default:
		return newAuthenticationError("unknown data type")
	}
}
