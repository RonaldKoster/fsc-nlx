// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package manager

import (
	api "go.nlx.io/nlx/manager/ports/int/rest/api/server"
)

type Client interface {
	api.ClientWithResponsesInterface
	api.ClientInterface
}
