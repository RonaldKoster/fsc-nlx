// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) DeleteService(ctx context.Context, args *storage.DeleteServiceArgs) error {
	err := p.queries.DeleteService(ctx, &queries.DeleteServiceParams{
		GroupID: args.GroupID,
		Name:    args.Name,
	})
	if err != nil {
		return fmt.Errorf("could not delete service in database: %w", err)
	}

	return nil
}
