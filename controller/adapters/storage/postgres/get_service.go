// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package postgresstorage

import (
	"context"
	"database/sql"
	"fmt"
	"net/url"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/adapters/storage/postgres/queries"
)

func (p *PostgresStorage) GetService(ctx context.Context, groupID, serviceName, inwayAddress string) (*storage.Service, error) {
	iwAddress := sql.NullString{}

	if inwayAddress != "" {
		iwAddress = sql.NullString{
			Valid:  true,
			String: inwayAddress,
		}
	}

	service, err := p.queries.GetService(ctx, &queries.GetServiceParams{
		GroupID:      groupID,
		Name:         serviceName,
		InwayAddress: iwAddress,
	})
	if err != nil {
		return nil, fmt.Errorf("could not get service from database: %w", err)
	}

	endpointURL, err := url.Parse(service.EndpointUrl)
	if err != nil {
		return nil, fmt.Errorf("invalid endpoint URL in database: %w", err)
	}

	return &storage.Service{
		GroupID:      service.GroupID,
		Name:         service.Name,
		InwayAddress: service.InwayAddress,
		EndpointURL:  endpointURL,
	}, nil
}
