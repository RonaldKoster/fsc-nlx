// Code generated by sqlc. DO NOT EDIT.
// versions:
//   sqlc v1.24.0

package queries

import ()

type ControllerInway struct {
	GroupID string
	Name    string
	Address string
}

type ControllerOutway struct {
	GroupID               string
	Name                  string
	CertificateThumbprint string
}

type ControllerService struct {
	GroupID      string
	Name         string
	EndpointUrl  string
	InwayAddress string
}
