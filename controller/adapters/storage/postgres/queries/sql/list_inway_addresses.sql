-- Copyright © VNG Realisatie 2023
-- Licensed under the EUPL

-- name: ListInwayAddresses :many
SELECT
    address
FROM
    controller.inways
WHERE
    group_id = $1
GROUP BY
    address;
