// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"fmt"
	"net/http"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
)

func (m *restManager) CreateContract(ctx context.Context, args *manager.CreateContractArgs) error {
	grants, err := convertGrants(args)
	if err != nil {
		return fmt.Errorf("could not convert args to rest api grant args: %w", err)
	}

	res, err := m.client.CreateContractWithResponse(ctx, models.CreateContractJSONRequestBody{
		ContractContent: models.ContractContent{
			Id:      args.ID,
			GroupId: args.GroupID,
			Validity: models.Validity{
				NotBefore: args.ContractNotBefore.Unix(),
				NotAfter:  args.ContractNotAfter.Unix(),
			},
			Grants:        grants,
			HashAlgorithm: mapHashAlgorithm(args.HashAlgorithm),
			CreatedAt:     args.CreatedAt.Unix(),
		},
	})
	if err != nil {
		return mapError(err, "could not create contract in rest manager")
	}

	return mapResponse(res, http.StatusCreated, "could not create contract", res.Body)
}

func mapHashAlgorithm(a manager.HashAlg) models.HashAlgorithm {
	switch a {
	case manager.HashAlgSHA3_512:
		return models.HASHALGORITHMSHA3512
	default:
		return ""
	}
}

//nolint:gocyclo // complex function because of the number of grant types
func convertGrants(contract *manager.CreateContractArgs) ([]models.Grant, error) {
	grants := make([]models.Grant, 0)

	for _, g := range contract.ServiceConnectionGrants {
		grant, err := convertServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.ServicePublicationGrants {
		grant, err := convertServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.DelegatedServiceConnectionGrants {
		grant, err := convertDelegatedServiceConnectionGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	for _, g := range contract.DelegatedServicePublicationGrants {
		grant, err := convertDelegatedServicePublicationGrant(g)
		if err != nil {
			return nil, err
		}

		grants = append(grants, *grant)
	}

	return grants, nil
}

func convertServicePublicationGrant(grant *manager.ServicePublicationGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantServicePublication(models.GrantServicePublication{
		Type: models.GRANTTYPESERVICEPUBLICATION,
		Directory: models.DirectoryPeer{
			PeerId: grant.DirectoryPeerID,
		},
		Service: models.ServicePublicationPeer{
			Name:     grant.ServiceName,
			PeerId:   grant.ServicePeerID,
			Protocol: models.Protocol(grant.ServiceProtocol),
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertServiceConnectionGrant(grant *manager.ServiceConnectionGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantServiceConnection(models.GrantServiceConnection{
		Type: models.GRANTTYPESERVICECONNECTION,
		Outway: models.OutwayPeer{
			PeerId:                grant.OutwayPeerID,
			CertificateThumbprint: grant.OutwayCertificateThumbprint,
		},
		Service: models.ServicePeer{
			PeerId: grant.ServicePeerID,
			Name:   grant.ServiceName,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertDelegatedServiceConnectionGrant(grant *manager.DelegatedServiceConnectionGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantDelegatedServiceConnection(models.GrantDelegatedServiceConnection{
		Type: models.GRANTTYPEDELEGATEDSERVICECONNECTION,
		Delegator: models.DelegatorPeer{
			PeerId: grant.DelegatorPeerID,
		},
		Service: models.ServicePeer{
			PeerId: grant.ServicePeerID,
			Name:   grant.ServiceName,
		},
		Outway: models.OutwayPeer{
			PeerId:                grant.OutwayPeerID,
			CertificateThumbprint: grant.OutwayCertificateThumbprint,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}

func convertDelegatedServicePublicationGrant(grant *manager.DelegatedServicePublicationGrant) (*models.Grant, error) {
	data := models.Grant{}
	err := data.FromGrantDelegatedServicePublication(models.GrantDelegatedServicePublication{
		Type: models.GRANTTYPEDELEGATEDSERVICEPUBLICATION,
		Delegator: models.DelegatorPeer{
			PeerId: grant.DelegatorPeerID,
		},
		Service: models.ServicePublicationPeer{
			PeerId:   grant.ServicePeerID,
			Name:     grant.ServiceName,
			Protocol: models.Protocol(grant.ServiceProtocol),
		},
		Directory: models.DirectoryPeer{
			PeerId: grant.DirectoryPeerID,
		},
	})

	if err != nil {
		return nil, err
	}

	return &data, nil
}
