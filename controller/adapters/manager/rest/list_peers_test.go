/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package restmanager_test

import (
	"context"
	"net/http"
	"testing"

	"github.com/pkg/errors"
	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/controller/adapters/manager"
	restmanager "go.nlx.io/nlx/controller/adapters/manager/rest"
	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
	api "go.nlx.io/nlx/manager/ports/int/rest/api/server"
)

// nolint:dupl // these tests should not fit 100 lines
func TestListPeers(t *testing.T) {
	t.Parallel()

	testcases := map[string]struct {
		setup   func(context.Context, *mocks)
		want    manager.Peers
		wantErr error
	}{
		"when_manager_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().
					GetPeersWithResponse(ctx).
					Return(nil, errors.New("unexpected error"))
			},
			wantErr: errors.Wrap(errors.New("unexpected error"), "could not retrieve peers from rest manager"),
		},
		"happy_flow_two_results": {
			setup: func(ctx context.Context, m *mocks) {
				m.client.EXPECT().GetPeersWithResponse(ctx).Return(&api.GetPeersResponse{
					HTTPResponse: &http.Response{StatusCode: http.StatusOK},
					JSON200: &struct {
						Peers []models.Peer "json:\"peers\""
					}{
						Peers: []models.Peer{
							{
								Id:   "1",
								Name: "PeerOne",
							},
							{
								Id:   "2",
								Name: "Peer2",
							},
						},
					},
				}, nil)
			},
			want: manager.Peers{
				{
					ID:   "1",
					Name: "PeerOne",
				},
				{
					ID:   "2",
					Name: "Peer2",
				},
			},
		},
	}

	for name, testcase := range testcases {
		testcase := testcase

		t.Run(name, func(t *testing.T) {
			t.Parallel()
			mocks := newMocks(t)

			ctx := context.Background()
			testcase.setup(ctx, mocks)

			restManager, err := restmanager.New(mocks.client, mocks.lgr)
			assert.NoError(t, err)

			actual, err := restManager.ListPeers(ctx)

			if testcase.wantErr == nil {
				assert.NoError(t, err)
				assert.Equal(t, testcase.want, actual)
			} else {
				assert.EqualError(t, err, testcase.wantErr.Error())
			}
		})
	}
}
