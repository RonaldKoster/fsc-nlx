// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"fmt"
	"net/http"

	"go.nlx.io/nlx/controller/adapters/manager"
)

func (m *restManager) RejectContract(ctx context.Context, contentHash string) error {
	res, err := m.client.RejectContractWithResponse(ctx, contentHash)
	if err != nil {
		return fmt.Errorf("could not reject contract with rest manager: %s: %w", err, manager.ErrInternalError)
	}

	return mapResponse(res, http.StatusNoContent, "could not reject contract", res.Body)
}
