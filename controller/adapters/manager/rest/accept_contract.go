// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"context"
	"fmt"
	"net/http"

	"go.nlx.io/nlx/controller/adapters/manager"
)

func (m *restManager) AcceptContract(ctx context.Context, contentHash string) error {
	res, err := m.client.AcceptContractWithResponse(ctx, contentHash)
	if err != nil {
		return fmt.Errorf("could not accept contract with rest manager: %s: %w", err, manager.ErrInternalError)
	}

	return mapResponse(res, http.StatusNoContent, "could not accept contract", res.Body)
}
