// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package restmanager

import (
	"fmt"

	"go.nlx.io/nlx/common/logger"
	api "go.nlx.io/nlx/manager/ports/int/rest/api/server"
)

type restManager struct {
	client api.ClientWithResponsesInterface
	logger *logger.Logger
}

func New(client api.ClientWithResponsesInterface, l *logger.Logger) (*restManager, error) {
	if client == nil {
		return nil, fmt.Errorf("manager rest client is required")
	}

	if l.Logger == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &restManager{
		client: client,
		logger: l,
	}, nil
}
