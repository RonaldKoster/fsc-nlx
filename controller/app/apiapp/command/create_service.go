// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/common/logger"
	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/pkg/authentication"
	"go.nlx.io/nlx/manager/domain/contract"
)

type CreateServiceHandler struct {
	groupID contract.GroupID
	lgr     *logger.Logger
	s       storage.Storage
}

func NewCreateServiceHandler(groupID contract.GroupID, s storage.Storage, l *logger.Logger) (*CreateServiceHandler, error) {
	if groupID == "" {
		return nil, fmt.Errorf("group id is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &CreateServiceHandler{
		groupID: groupID,
		lgr:     l,
		s:       s,
	}, nil
}

type CreateServiceArgs struct {
	AuthData     authentication.Data
	Name         string
	EndpointURL  string
	InwayAddress string
}

func (h *CreateServiceHandler) Handle(ctx context.Context, args *CreateServiceArgs) error {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return mapError(err, "could not authenticate")
	}

	if err := args.Valid(); err != nil {
		return err
	}

	err = h.s.CreateService(ctx, &storage.CreateServiceArgs{
		GroupID:      h.groupID.String(),
		Name:         args.Name,
		EndpointURL:  args.EndpointURL,
		InwayAddress: args.InwayAddress,
	})
	if err != nil {
		h.lgr.Error("failed to create service", err)

		return mapError(err, "could not create service in storage")
	}

	return nil
}

func (c *CreateServiceArgs) Valid() *ValidationError {
	validationErrors := make([]string, 0)

	if c.Name == "" {
		validationErrors = append(validationErrors, "service name cannot be blank")
	}

	if !serviceNameValidator.MatchString(c.Name) {
		validationErrors = append(validationErrors, "service name must contain only letters, numbers, -, _ and .")
	}

	if !serviceEndpointURLValidator.MatchString(c.EndpointURL) {
		validationErrors = append(validationErrors, "endpoint URL cannot be blank, must start with https:// or http:// and cannot contain spaces")
	}

	return mapValidationError(validationErrors)
}
