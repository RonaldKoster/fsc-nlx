// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"
	"time"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/common/logger"
	"go.nlx.io/nlx/controller/adapters/idgenerator"
	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/pkg/authentication"
	"go.nlx.io/nlx/manager/ports/int/rest/api/models"
)

type CreateContractHandler struct {
	manager manager.Manager
	clock   clock.Clock
	id      idgenerator.IDGenerator
	lgr     *logger.Logger
}

func NewCreateContractHandler(m manager.Manager, c clock.Clock, i idgenerator.IDGenerator, l *logger.Logger) (*CreateContractHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	if c == nil {
		return nil, fmt.Errorf("clock is required")
	}

	if i == nil {
		return nil, fmt.Errorf("id generator is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	return &CreateContractHandler{
		manager: m,
		clock:   c,
		id:      i,
		lgr:     l,
	}, nil
}

type CreateContractArgs struct {
	AuthData                          authentication.Data
	HashAlgorithm                     string
	GroupID                           string
	ContractNotBefore                 string
	ContractNotAfter                  string
	ServicePublicationGrants          []*ServicePublicationGrant
	ServiceConnectionGrants           []*ServiceConnectionGrant
	DelegatedServiceConnectionGrants  []*DelegatedServiceConnectionGrant
	DelegatedServicePublicationGrants []*DelegatedServicePublicationGrant
}

type grantType interface {
	ServicePublicationGrant | ServiceConnectionGrant | DelegatedServiceConnectionGrant | DelegatedServicePublicationGrant
}

type grantPointer[T grantType] interface {
	*T
	Validator
}

type ServicePublicationGrant struct {
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

func (g *ServicePublicationGrant) getServiceName() string {
	return g.ServiceName
}

func (g *ServicePublicationGrant) Valid() *ValidationError {
	return mapValidationError(validateGrants(g))
}

type ServiceConnectionGrant struct {
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

func (g *ServiceConnectionGrant) getServiceName() string {
	return g.ServiceName
}

func (g *ServiceConnectionGrant) Valid() *ValidationError {
	return mapValidationError(validateGrants(g))
}

type DelegatedServiceConnectionGrant struct {
	DelegatorPeerID             string
	ServicePeerID               string
	ServiceName                 string
	OutwayPeerID                string
	OutwayCertificateThumbprint string
}

func (g *DelegatedServiceConnectionGrant) getServiceName() string {
	return g.ServiceName
}

func (g *DelegatedServiceConnectionGrant) Valid() *ValidationError {
	return mapValidationError(validateGrants(g))
}

type DelegatedServicePublicationGrant struct {
	DelegatorPeerID string
	DirectoryPeerID string
	ServicePeerID   string
	ServiceName     string
}

func (g *DelegatedServicePublicationGrant) getServiceName() string {
	return g.ServiceName
}

func (g *DelegatedServicePublicationGrant) Valid() *ValidationError {
	return mapValidationError(validateGrants(g))
}

type commonGrantValidation interface {
	getServiceName() string
}

func validateGrants(validation commonGrantValidation) []string {
	validationErrors := make([]string, 0)
	serviceName := validation.getServiceName()

	if serviceName == "" {
		validationErrors = append(validationErrors, "service name cannot be blank")
	}

	if !serviceNameValidator.MatchString(serviceName) {
		validationErrors = append(validationErrors, "service name must contain only letters, numbers, -, _ and .")
	}

	return validationErrors
}

const HashAlgSHA3_512 = "SHA3-512"

var mapHashAlgorithm = map[string]manager.HashAlg{
	HashAlgSHA3_512: manager.HashAlgSHA3_512,
}

func (h *CreateContractHandler) Handle(ctx context.Context, args *CreateContractArgs) error {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return mapError(err, "could not authenticate")
	}

	notBefore, err := time.Parse("2006-01-02", args.ContractNotBefore)
	if err != nil {
		return newValidationError("the not before date should be formatted as 2006-01-02")
	}

	notAfter, err := time.Parse("2006-01-02", args.ContractNotAfter)
	if err != nil {
		return newValidationError("the not after date should be formatted as 2006-01-02")
	}

	servicePublicationGrants, err := mapGrant(args.ServicePublicationGrants, func(grant *ServicePublicationGrant) *manager.ServicePublicationGrant {
		return &manager.ServicePublicationGrant{
			DirectoryPeerID: grant.DirectoryPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
			ServiceProtocol: string(models.PROTOCOLTCPHTTP11),
		}
	})
	if err != nil {
		return err
	}

	serviceConnectionGrants, err := mapGrant(args.ServiceConnectionGrants, func(grant *ServiceConnectionGrant) *manager.ServiceConnectionGrant {
		return &manager.ServiceConnectionGrant{
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	})
	if err != nil {
		return err
	}

	delegatedServiceConnectionGrants, err := mapGrant(args.DelegatedServiceConnectionGrants, func(grant *DelegatedServiceConnectionGrant) *manager.DelegatedServiceConnectionGrant {
		return &manager.DelegatedServiceConnectionGrant{
			DelegatorPeerID:             grant.DelegatorPeerID,
			ServicePeerID:               grant.ServicePeerID,
			ServiceName:                 grant.ServiceName,
			OutwayPeerID:                grant.OutwayPeerID,
			OutwayCertificateThumbprint: grant.OutwayCertificateThumbprint,
		}
	})
	if err != nil {
		return err
	}

	delegatedServicePublicationGrants, err := mapGrant(args.DelegatedServicePublicationGrants, func(grant *DelegatedServicePublicationGrant) *manager.DelegatedServicePublicationGrant {
		return &manager.DelegatedServicePublicationGrant{
			DirectoryPeerID: grant.DirectoryPeerID,
			DelegatorPeerID: grant.DelegatorPeerID,
			ServicePeerID:   grant.ServicePeerID,
			ServiceName:     grant.ServiceName,
			ServiceProtocol: string(models.PROTOCOLTCPHTTP11),
		}
	})
	if err != nil {
		return err
	}

	id, err := h.id.New()
	if err != nil {
		return mapError(err, "failed to generate id")
	}

	hashAlgorithm, ok := mapHashAlgorithm[args.HashAlgorithm]
	if !ok {
		return newValidationError("invalid hash algorithm")
	}

	createContractArgs := &manager.CreateContractArgs{
		ID:                                id,
		HashAlgorithm:                     hashAlgorithm,
		GroupID:                           args.GroupID,
		ContractNotBefore:                 notBefore,
		ContractNotAfter:                  notAfter,
		ServicePublicationGrants:          servicePublicationGrants,
		ServiceConnectionGrants:           serviceConnectionGrants,
		DelegatedServiceConnectionGrants:  delegatedServiceConnectionGrants,
		DelegatedServicePublicationGrants: delegatedServicePublicationGrants,

		CreatedAt: h.clock.Now(),
	}

	err = h.manager.CreateContract(ctx, createContractArgs)
	if err != nil {
		h.lgr.Error("failed to create contract", err)
		return mapError(err, "could not create contract in manager")
	}

	return nil
}

// mapGrant validates the grants and transforms the grant to the corresponding manager grant
// grants is a slice of grantPointer
// grantPointer is introduced as a composing interface containing a pointer to the grantType and Validator interface.
// This enables the various grant structs constraint by the generic grantType to have pointer receiver methods implementing the Validator interface
func mapGrant[T grantType, G grantPointer[T], R manager.Grant](grants []G, mapGrant func(grants G) *R) ([]*R, error) {
	result := make([]*R, len(grants))

	validationErrors := make([]*ValidationError, 0)

	for i, g := range grants {
		validator := g

		if err := validator.Valid(); err != nil {
			validationErrors = append(validationErrors, err)
		}

		result[i] = mapGrant(g)
	}

	validationError := squashValidationErrors(validationErrors)
	if validationError != nil {
		return nil, validationError
	}

	return result, nil

}
