// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package command_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/maps"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/app/apiapp/command"
	"go.nlx.io/nlx/controller/pkg/authentication"
	"go.nlx.io/nlx/manager/domain/contract"
)

// nolint:funlen // these tests should not fit 100 lines
func TestDeleteService(t *testing.T) {
	t.Parallel()

	groupID, err := contract.NewGroupID("my-group.directory.nlx.io")
	assert.NoError(t, err)

	type tests map[string]struct {
		setup   func(context.Context, *mocks)
		args    *command.DeleteServiceArgs
		wantErr error
	}

	validArgs := &command.DeleteServiceArgs{
		AuthData: &authentication.OIDCData{},
		Name:     "TestService",
	}

	flowTests := tests{
		"when_storage_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.storage.EXPECT().
					DeleteService(ctx, &storage.DeleteServiceArgs{
						GroupID: groupID.String(),
						Name:    validArgs.Name,
					}).
					Return(fmt.Errorf("arbitrary"))
			},
			args:    validArgs,
			wantErr: &command.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a command.DeleteServiceArgs) *command.DeleteServiceArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &command.AuthenticationError{},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.storage.EXPECT().
					DeleteService(ctx, &storage.DeleteServiceArgs{
						GroupID: groupID.String(),
						Name:    validArgs.Name,
					}).
					Return(nil)
			},
			wantErr: nil,
		},
	}

	validationTests := tests{
		"invalid_name": {
			args: func(a command.DeleteServiceArgs) *command.DeleteServiceArgs {
				a.Name = ""
				return &a
			}(*validArgs),
			wantErr: &command.ValidationError{},
		},
	}

	allTests := tests{}
	maps.Copy(allTests, flowTests)
	maps.Copy(allTests, validationTests)

	// nolint:dupl // not the same
	for name, tt := range allTests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := command.NewDeleteServiceHandler(groupID, mocks.storage, mocks.logger)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			err = h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}

func TestDeleteServiceArgsValidation(t *testing.T) {
	t.Parallel()

	type tests map[string]struct {
		args       *command.DeleteServiceArgs
		wantErr    error
		wantErrMsg string
	}

	testCases := tests{
		"invalid_name_spaces": {
			args: &command.DeleteServiceArgs{
				Name: "test service",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name must contain only letters, numbers, -, _ and .",
		},
		"invalid_name_blank": {
			args: &command.DeleteServiceArgs{
				Name:        "",
				EndpointURL: "https://example.com",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .",
		},
	}

	for testCase, test := range testCases {
		tt := test

		t.Run(testCase, func(t *testing.T) {
			t.Parallel()

			err := tt.args.Valid()
			assert.ErrorAs(t, err, &tt.wantErr)
			assert.Equal(t, tt.wantErrMsg, err.Error())
		})
	}
}
