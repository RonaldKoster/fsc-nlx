// Copyright © VNG Realisatie 2023
// Licensed under the EUPL
package command_test

import (
	"context"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"golang.org/x/exp/maps"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/app/apiapp/command"
	"go.nlx.io/nlx/controller/pkg/authentication"
	"go.nlx.io/nlx/manager/domain/contract"
)

// nolint:funlen // these tests should not fit 100 lines
func TestCreateService(t *testing.T) {
	t.Parallel()

	groupID, err := contract.NewGroupID("my-group.directory.nlx.io")
	assert.NoError(t, err)

	type tests map[string]struct {
		setup   func(context.Context, *mocks)
		args    *command.CreateServiceArgs
		wantErr error
	}

	validArgs := &command.CreateServiceArgs{
		AuthData:     &authentication.OIDCData{},
		Name:         "TestService",
		EndpointURL:  "https://petstore.io/",
		InwayAddress: "my-inway.local:443",
	}

	flowTests := tests{
		"when_storage_errors": {
			setup: func(ctx context.Context, m *mocks) {
				m.storage.EXPECT().
					CreateService(ctx, &storage.CreateServiceArgs{
						GroupID:      groupID.String(),
						Name:         validArgs.Name,
						EndpointURL:  validArgs.EndpointURL,
						InwayAddress: validArgs.InwayAddress,
					}).
					Return(fmt.Errorf("arbitrary"))
			},
			args:    validArgs,
			wantErr: &command.InternalError{},
		},
		"when_unauthenticated": {
			args: func(a command.CreateServiceArgs) *command.CreateServiceArgs {
				a.AuthData = nil
				return &a
			}(*validArgs),
			wantErr: &command.AuthenticationError{},
		},
		"happy_flow": {
			args: validArgs,
			setup: func(ctx context.Context, m *mocks) {
				m.storage.EXPECT().
					CreateService(ctx, &storage.CreateServiceArgs{
						GroupID:      groupID.String(),
						Name:         validArgs.Name,
						EndpointURL:  validArgs.EndpointURL,
						InwayAddress: validArgs.InwayAddress,
					}).
					Return(nil)
			},
			wantErr: nil,
		},
	}

	validationTests := tests{
		"invalid_name": {
			args: func(a command.CreateServiceArgs) *command.CreateServiceArgs {
				a.Name = ""
				return &a
			}(*validArgs),
			wantErr: &command.ValidationError{},
		},
		"invalid_endpoint_url": {
			args: func(a command.CreateServiceArgs) *command.CreateServiceArgs {
				a.EndpointURL = ""
				return &a
			}(*validArgs),
			wantErr: &command.ValidationError{},
		},
	}

	allTests := tests{}
	maps.Copy(allTests, flowTests)
	maps.Copy(allTests, validationTests)

	for name, tt := range allTests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			mocks := newMocks(t)

			h, err := command.NewCreateServiceHandler(groupID, mocks.storage, mocks.logger)
			require.NoError(t, err)

			ctx := context.Background()

			if tt.setup != nil {
				tt.setup(ctx, mocks)
			}

			err = h.Handle(ctx, tt.args)

			if tt.wantErr == nil {
				assert.NoError(t, err)
			} else {
				assert.ErrorAs(t, err, &tt.wantErr)
			}
		})
	}
}

func TestCreateServiceArgsValidation(t *testing.T) {
	t.Parallel()

	type tests map[string]struct {
		args       *command.CreateServiceArgs
		wantErr    error
		wantErrMsg string
	}

	testCases := tests{
		"invalid_name_spaces": {
			args: &command.CreateServiceArgs{
				Name:        "test service",
				EndpointURL: "https://example.com",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name must contain only letters, numbers, -, _ and .",
		},
		"invalid_name_blank": {
			args: &command.CreateServiceArgs{
				Name:        "",
				EndpointURL: "https://example.com",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .",
		},
		"invalid_url_blank": {
			args: &command.CreateServiceArgs{
				Name:        "testservice",
				EndpointURL: "",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: endpoint URL cannot be blank, must start with https:// or http:// and cannot contain spaces",
		},
		"invalid_name_and_url": {
			args: &command.CreateServiceArgs{
				Name:        "",
				EndpointURL: "",
			},
			wantErr:    &command.ValidationError{},
			wantErrMsg: "validation error: service name cannot be blank\nservice name must contain only letters, numbers, -, _ and .\nendpoint URL cannot be blank, must start with https:// or http:// and cannot contain spaces",
		},
	}

	for testCase, test := range testCases {
		tt := test

		t.Run(testCase, func(t *testing.T) {
			t.Parallel()

			err := tt.args.Valid()
			assert.ErrorAs(t, err, &tt.wantErr)
			assert.Equal(t, tt.wantErrMsg, err.Error())
		})
	}
}
