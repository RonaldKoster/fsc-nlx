// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"errors"
	"fmt"

	"go.nlx.io/nlx/common/logger"
	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/pkg/authentication"
	"go.nlx.io/nlx/manager/domain/contract"
)

type UpdateServiceHandler struct {
	groupID contract.GroupID
	lgr     *logger.Logger
	s       storage.Storage
}

func NewUpdateServiceHandler(groupID contract.GroupID, s storage.Storage, l *logger.Logger) (*UpdateServiceHandler, error) {
	if groupID == "" {
		return nil, fmt.Errorf("group id is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &UpdateServiceHandler{
		groupID: groupID,
		lgr:     l,
		s:       s,
	}, nil
}

type UpdateServiceArgs struct {
	AuthData     authentication.Data
	Name         string
	EndpointURL  string
	InwayAddress string
}

func (h *UpdateServiceHandler) Handle(ctx context.Context, args *UpdateServiceArgs) error {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return mapError(err, "could not authenticate")
	}

	if err := args.Valid(); err != nil {
		return err
	}

	err = h.s.UpdateService(ctx, &storage.UpdateServiceArgs{
		GroupID:      h.groupID.String(),
		Name:         args.Name,
		EndpointURL:  args.EndpointURL,
		InwayAddress: args.InwayAddress,
	})
	if err != nil {
		if errors.Is(err, storage.ErrServiceNotFound) {
			return ErrServiceNotFound
		}

		h.lgr.Error("failed to update service", err)

		return mapError(err, "could not update service in storage")
	}

	return nil
}

func (c *UpdateServiceArgs) Valid() *ValidationError {
	validationErrors := make([]string, 0)

	if c.Name == "" {
		validationErrors = append(validationErrors, "service name cannot be blank")
	}

	if !serviceNameValidator.MatchString(c.Name) {
		validationErrors = append(validationErrors, "service name must contain only letters, numbers, -, _ and .")
	}

	if !serviceEndpointURLValidator.MatchString(c.EndpointURL) {
		validationErrors = append(validationErrors, "endpoint URL cannot be blank, must start with https:// or http:// and cannot contain spaces")
	}

	return mapValidationError(validationErrors)
}
