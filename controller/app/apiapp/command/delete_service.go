// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package command

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/common/logger"
	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/pkg/authentication"
	"go.nlx.io/nlx/manager/domain/contract"
)

type DeleteServiceHandler struct {
	groupID contract.GroupID
	lgr     *logger.Logger
	s       storage.Storage
}

func NewDeleteServiceHandler(groupID contract.GroupID, s storage.Storage, l *logger.Logger) (*DeleteServiceHandler, error) {
	if groupID == "" {
		return nil, fmt.Errorf("group id is required")
	}

	if l == nil {
		return nil, fmt.Errorf("logger is required")
	}

	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &DeleteServiceHandler{
		groupID: groupID,
		lgr:     l,
		s:       s,
	}, nil
}

type DeleteServiceArgs struct {
	AuthData     authentication.Data
	Name         string
	EndpointURL  string
	InwayAddress string
}

func (h *DeleteServiceHandler) Handle(ctx context.Context, args *DeleteServiceArgs) error {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return mapError(err, "could not authenticate")
	}

	if err := args.Valid(); err != nil {
		return err
	}

	err = h.s.DeleteService(ctx, &storage.DeleteServiceArgs{
		GroupID: h.groupID.String(),
		Name:    args.Name,
	})
	if err != nil {
		h.lgr.Error("failed to delete service", err)

		return mapError(err, "could not delete service in storage")
	}

	return nil
}

func (c *DeleteServiceArgs) Valid() *ValidationError {
	validationErrors := make([]string, 0)

	if c.Name == "" {
		validationErrors = append(validationErrors, "service name cannot be blank")
	}

	if !serviceNameValidator.MatchString(c.Name) {
		validationErrors = append(validationErrors, "service name must contain only letters, numbers, -, _ and .")
	}

	return mapValidationError(validationErrors)
}
