// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/manager"
	"go.nlx.io/nlx/controller/pkg/authentication"
)

type GetPeerInfoHandler struct {
	manager manager.Manager
}

func NewGetPeerInfoHandler(ctx context.Context, m manager.Manager) (*GetPeerInfoHandler, error) {
	if m == nil {
		return nil, fmt.Errorf("manager is required")
	}

	return &GetPeerInfoHandler{
		manager: m,
	}, nil
}

type GetPeerInfoArgs struct {
	AuthData authentication.Data
}

func (h *GetPeerInfoHandler) Handle(ctx context.Context, args *GetPeerInfoArgs) (*Peer, error) {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return nil, mapError(err, "could not authenticate")
	}

	p, err := h.manager.GetPeerInfo(ctx)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", newInternalError("could not get peer info from manager"), err)
	}

	return &Peer{
		ID:   p.ID,
		Name: p.Name,
	}, nil
}
