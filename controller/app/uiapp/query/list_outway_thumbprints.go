// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/controller/pkg/authentication"
	"go.nlx.io/nlx/manager/domain/contract"
)

type ListOutwayThumbprintsHandler struct {
	storage storage.Storage
	groupID contract.GroupID
}

func NewListOutwayThumbprintsHandler(groupID contract.GroupID, s storage.Storage) (*ListOutwayThumbprintsHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	if groupID == "" {
		return nil, fmt.Errorf("group ID is required")
	}

	return &ListOutwayThumbprintsHandler{
		groupID: groupID,
		storage: s,
	}, nil
}

type ListOutwayThumbprintsArgs struct {
	AuthData authentication.Data
}

func (h *ListOutwayThumbprintsHandler) Handle(ctx context.Context, args *ListOutwayThumbprintsArgs) (map[string][]string, error) {
	err := authentication.Authenticate(args.AuthData)
	if err != nil {
		return nil, mapError(err, "could not authenticate")
	}

	result, err := h.storage.ListOutwayThumbprints(ctx, h.groupID.String())
	if err != nil {
		return nil, mapError(err, "could not get outway thumbprints from storage")
	}

	return result, nil
}
