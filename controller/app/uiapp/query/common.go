// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package query

import (
	"errors"
	"fmt"

	"go.nlx.io/nlx/controller/pkg/authentication"
)

type InternalError struct {
	m string
}

func newInternalError(message string) *InternalError {
	return &InternalError{
		m: message,
	}
}

func (v *InternalError) Error() string {
	return "internal error: " + v.m
}

type AuthenticationError struct {
	m string
}

func newAuthenticationError(message string) *AuthenticationError {
	return &AuthenticationError{
		m: message,
	}
}

func (v *AuthenticationError) Error() string {
	return "authentication error: " + v.m
}

func mapError(err error, message string) error {
	var a *authentication.AuthenticationError
	if errors.As(err, &a) {
		return fmt.Errorf("%s: %s: %w", message, newAuthenticationError(a.Error()), err)
	}

	return fmt.Errorf("%s: %w", newInternalError(message), err)
}
