// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package commands

import (
	"context"
	"fmt"

	"go.nlx.io/nlx/controller/adapters/storage"
	"go.nlx.io/nlx/manager/domain/contract"
)

type RegisterOutwayHandler struct {
	storage storage.Storage
}

func NewRegisterOutwayHandler(s storage.Storage) (*RegisterOutwayHandler, error) {
	if s == nil {
		return nil, fmt.Errorf("storage is required")
	}

	return &RegisterOutwayHandler{
		storage: s,
	}, nil
}

type RegisterOutwayArgs struct {
	GroupID               string
	Name                  string
	CertificateThumbprint string
}

func (h *RegisterOutwayHandler) Handle(ctx context.Context, args *RegisterOutwayArgs) error {
	err := args.Validate()
	if err != nil {
		return fmt.Errorf("%w: invalid register outway args: %s", ErrValidationError, err)
	}

	err = h.storage.RegisterOutway(ctx, args.GroupID, args.Name, args.CertificateThumbprint)
	if err != nil {
		return fmt.Errorf("%w: could not register outway in storage: %s", ErrInternalError, err)
	}

	return nil
}

func (a *RegisterOutwayArgs) Validate() error {
	_, err := contract.NewGroupID(a.GroupID)
	if err != nil {
		return fmt.Errorf("%w: invalid groupID in args: %w", ErrValidationError, err)
	}

	if a.Name == "" {
		return fmt.Errorf("%w: Name is required", ErrValidationError)
	}

	if a.CertificateThumbprint == "" {
		return fmt.Errorf("%w: CertificateThumbprint is required", ErrValidationError)
	}

	return nil
}
