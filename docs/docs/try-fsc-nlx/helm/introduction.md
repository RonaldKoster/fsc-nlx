---
id: introduction
title: Introduction
---

# Introduction

:::caution
Please note that the result of this guide is an FSC NLX environment that connects with the NLX demo environment. This environment is not suitable for production purposes.
The configuration described in this guide should not be used in production.
:::

In this guide, we work towards offering an API via an FSC NLX Inway. We request (and grant) access to that API and query that API with a client via an FSC NLX Outway. All FSC NLX components are installed on a Kubernetes cluster ([Haven](https://haven.commonground.nl)).
