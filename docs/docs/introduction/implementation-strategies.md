---
id: implementation-strategies
title: Implementation strategies
---

While designing the FSC standard, we have put extra effort in removing as much functionality from the Inway and Outway as possibile.
This makes it easier to make other gateways FSC compliant. The Manager contains most of the FSC standard functionality.

We suggest three strategies to adopt the FSC standard:

1. Implement the standard writing your own software
1. Use the open source FSC NLX implementation
1. Use a hybrid implementation where you combine option 1 and 2: use your current gateway combined with the open source FSC NLX Manager.

Option 2 is made available to help with a quick adoption of the standard. We guarantee maintenance support for FSC NLX until at least the end of 2024.
Depending on the demand, this period might be extended.

Option 3 will probably be the strategy most integrators will use. It fits their current pattern of using add-ons on
their gateway software, implementing multiple connectivity practices and minimizing the impact on the gateway itself.
