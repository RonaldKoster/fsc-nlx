# Copyright © VNG Realisatie 2022
# Licensed under the EUPL

FROM node:20.10.0-alpine AS build

ENV CI=true

# First copy only package.json and yarn.lock to make the dependency fetching step optional.
COPY website/package.json \
    website/package-lock.json \
    /go/src/go.nlx.io/nlx/docs/website/

WORKDIR /go/src/go.nlx.io/nlx/docs/website
RUN npm ci --no-progress --color=false --quiet

# Now copy the whole workdir for the build step.
COPY . /go/src/go.nlx.io/nlx/docs

RUN npm run build

# Create version.json
FROM alpine:3.19.0 AS version

RUN apk add --update jq

ARG GIT_TAG_NAME=undefined
ARG GIT_COMMIT_HASH=undefined
RUN jq -ncM --arg tag $GIT_TAG_NAME --arg commit $GIT_COMMIT_HASH  '{tag: $tag, commit: $commit}' | tee /version.json

# Copy static docs to alpine-based nginx container.
FROM nginxinc/nginx-unprivileged:alpine3.18
EXPOSE 8080

COPY docker/default.conf /etc/nginx/conf.d/default.conf
COPY docker/nginx.conf /etc/nginx/nginx.conf

COPY --from=build /go/src/go.nlx.io/nlx/docs/website/build /usr/share/nginx/html

# This is a workaround to https://github.com/moby/moby/issues/37965
RUN true

COPY --from=version /version.json /usr/share/nginx/html/
