// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package testhash_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	testhash "go.nlx.io/nlx/testing/hash"
)

func TestGenerateServiceConnectionGrantHash(t *testing.T) {
	const sha3_512 = 1

	tests := map[string]struct {
		args    *testhash.ServiceConnectionGrantArgs
		want    string
		wantErr bool
	}{
		"invalid_uuid": {
			args: &testhash.ServiceConnectionGrantArgs{
				ContractContentID:           "invalid",
				ContractGroupID:             "fsc-test",
				ContractHashAlgorithm:       sha3_512,
				OutwayPeerID:                "12345678901234567899",
				OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
				ServicePeerID:               "12345678901234567890",
				ServiceName:                 "parkeerrechten",
			},
			want:    "",
			wantErr: true,
		},
		"happy_flow": {
			args: &testhash.ServiceConnectionGrantArgs{
				ContractContentID:           "495d29af-8004-4add-a329-9c0327d10f05",
				ContractGroupID:             "fsc-test",
				ContractHashAlgorithm:       sha3_512,
				OutwayPeerID:                "12345678901234567899",
				OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
				ServicePeerID:               "12345678901234567890",
				ServiceName:                 "parkeerrechten",
			},
			want:    "$1$3$R4xCXemYojv7FJKTBa9sKQ6lEU4RcllOxHWhMvmlEDJuFyHI2ft91XlQukU5cnqUWMuQYnEaPlFATHLEa4SErA",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testhash.GenerateServiceConnectionGrantHash(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateServiceConnectionGrantHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
