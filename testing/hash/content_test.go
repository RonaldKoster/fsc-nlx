// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package testhash_test

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	testhash "go.nlx.io/nlx/testing/hash"
)

//nolint:funlen,dupl // this is a test
func TestGenerateContractContentHash(t *testing.T) {
	const sha3_512 = 1

	tests := map[string]struct {
		args    *testhash.ContractContentArgs
		want    string
		wantErr bool
	}{
		"invalid_id": {
			args: &testhash.ContractContentArgs{
				ID:                      "invalid",
				GroupID:                 "fsc-test",
				HashAlgorithm:           sha3_512,
				ValidNotBefore:          time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:           time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{},
				CreatedAt:               time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "",
			wantErr: true,
		},
		"happy_flow_service_publication_grant": {
			args: &testhash.ContractContentArgs{
				ID:             "495d29af-8004-4add-a329-9c0327d10f05",
				GroupID:        "fsc-test",
				HashAlgorithm:  sha3_512,
				ValidNotBefore: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:  time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ServicePublicationGrants: []testhash.ContractContentServicePublicationGrantArgs{
					{
						DirectoryPeerID: "12345678901234567899",
						ServicePeerID:   "12345678901234567890",
						ServiceName:     "parkeerrechten",
					},
				},
				CreatedAt: time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "$1$1$5mRyVh-1dq6zKxO-eiXyY7oiBOinouLezijx8ZT-aehO9euFbgsUlc7Mwqo9eBkJHldHSZQjU2oSRBj35ivExA",
			wantErr: false,
		},
		"happy_flow_service_connection_grant": {
			args: &testhash.ContractContentArgs{
				ID:             "495d29af-8004-4add-a329-9c0327d10f05",
				GroupID:        "fsc-test",
				HashAlgorithm:  sha3_512,
				ValidNotBefore: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:  time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
					{
						OutwayPeerID:                "12345678901234567890",
						OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
						ServicePeerID:               "12345678901234567891",
						ServiceName:                 "parkeerrechten",
					},
				},
				CreatedAt: time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "$1$1$kimpA-kq-8HGrCO_ty280D_rzxr8uA0NTQbqaYKp1BEeNV771n8KyBu3zULLweLvRl17Rf3hp-51nKIhfSWgqw",
			wantErr: false,
		},
		"happy_flow_delegated_service_connection_grant": {
			args: &testhash.ContractContentArgs{
				ID:             "495d29af-8004-4add-a329-9c0327d10f05",
				GroupID:        "fsc-test",
				HashAlgorithm:  sha3_512,
				ValidNotBefore: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:  time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				DelegatedServiceConnectionGrants: []testhash.ContractContentDelegatedServiceConnectionGrantArgs{
					{
						DelegatorPeerID:             "12345678901234567892",
						OutwayPeerID:                "12345678901234567890",
						OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
						ServicePeerID:               "12345678901234567891",
						ServiceName:                 "parkeerrechten",
					},
				},
				CreatedAt: time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "$1$1$FEtkzVqDdECwbUz0nbpumSn4tceOpLXlL1yGKCv7Nl61xsp9e11TEtkx1B-mDWx93FY9_N1Fh7WNQl8533OAkg",
			wantErr: false,
		},
		"happy_flow_delegated_service_publication_grant": {
			args: &testhash.ContractContentArgs{
				ID:             "495d29af-8004-4add-a329-9c0327d10f05",
				GroupID:        "fsc-test",
				HashAlgorithm:  sha3_512,
				ValidNotBefore: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:  time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				DelegatedServicePublicationGrants: []testhash.ContractContentDelegatedServicePublicationGrantArgs{
					{
						DelegatorPeerID: "12345678901234567891",
						DirectoryPeerID: "12345678901234567899",
						ServicePeerID:   "12345678901234567892",
						ServiceName:     "parkeerrechten",
					},
				},
				CreatedAt: time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "$1$1$fNZAsVCV00rfT0alCP-mTwBQsKzlCrhwJ97HvAUs-QvztksdL8_ZCisgt02zu8UECVo_wgtQMkwpVs63ixkzPg",
			wantErr: false,
		},
		"happy_flow_combination": {
			args: &testhash.ContractContentArgs{
				ID:             "495d29af-8004-4add-a329-9c0327d10f05",
				GroupID:        "fsc-test",
				HashAlgorithm:  sha3_512,
				ValidNotBefore: time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ValidNotAfter:  time.Date(2024, 1, 2, 3, 4, 5, 6, time.UTC).Unix(),
				ServicePublicationGrants: []testhash.ContractContentServicePublicationGrantArgs{
					{
						DirectoryPeerID: "12345678901234567899",
						ServicePeerID:   "12345678901234567890",
						ServiceName:     "parkeerrechten",
					},
				},
				ServiceConnectionGrants: []testhash.ContractContentServiceConnectionGrantArgs{
					{
						OutwayPeerID:                "12345678901234567890",
						OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
						ServicePeerID:               "12345678901234567891",
						ServiceName:                 "parkeerrechten",
					},
				},
				DelegatedServiceConnectionGrants: []testhash.ContractContentDelegatedServiceConnectionGrantArgs{
					{
						DelegatorPeerID:             "12345678901234567892",
						OutwayPeerID:                "12345678901234567890",
						OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
						ServicePeerID:               "12345678901234567891",
						ServiceName:                 "parkeerrechten",
					},
				},
				DelegatedServicePublicationGrants: []testhash.ContractContentDelegatedServicePublicationGrantArgs{
					{
						DelegatorPeerID: "12345678901234567891",
						DirectoryPeerID: "12345678901234567899",
						ServicePeerID:   "12345678901234567892",
						ServiceName:     "parkeerrechten",
					},
				},
				CreatedAt: time.Date(2023, 2, 3, 4, 5, 6, 7, time.UTC).Unix(),
			},
			want:    "$1$1$R1g9Nnx7yhG--lfzDRKmy3vgYqwb0LFWPAxqjeB5nfUmBSrTKjZnaeiBan5BjvPZc0SKDhBgjT2ROZIhmWxZpg",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testhash.GenerateContractContentHash(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateContractContentHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
