// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package testhash_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	testhash "go.nlx.io/nlx/testing/hash"
)

func TestGenerateDelegatedServiceConnectionGrantHash(t *testing.T) {
	const sha3_512 = 1

	tests := map[string]struct {
		args    *testhash.DelegatedServiceConnectionGrantArgs
		want    string
		wantErr bool
	}{
		"invalid_uuid": {
			args: &testhash.DelegatedServiceConnectionGrantArgs{
				ContractContentID:           "invalid",
				ContractGroupID:             "fsc-test",
				ContractHashAlgorithm:       sha3_512,
				DelegatorPeerID:             "12345678901234567891",
				OutwayPeerID:                "12345678901234567899",
				OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
				ServicePeerID:               "12345678901234567890",
				ServiceName:                 "parkeerrechten",
			},
			want:    "",
			wantErr: true,
		},
		"happy_flow": {
			args: &testhash.DelegatedServiceConnectionGrantArgs{
				ContractContentID:           "495d29af-8004-4add-a329-9c0327d10f05",
				ContractGroupID:             "fsc-test",
				ContractHashAlgorithm:       sha3_512,
				DelegatorPeerID:             "12345678901234567891",
				OutwayPeerID:                "12345678901234567899",
				OutwayCertificateThumbprint: "sG4y7jik0N86x4i8z8saGs3GFm9WSqyDAazykgRnwRU",
				ServicePeerID:               "12345678901234567890",
				ServiceName:                 "parkeerrechten",
			},
			want:    "$1$4$VO8YsztbA4P1MQ8c_zjN2b2CbMTPzztJt6FudQSwU-oKSJKrScDg9yA_bcZaswhwG7EUP028TYYDQ_0qBt0ApA",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testhash.GenerateDelegatedServiceConnectionGrantHash(tt.args)
			if (err != nil) != tt.wantErr {
				t.Errorf("GenerateDelegatedServiceConnectionGrantHash() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
