// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package testsignature

import (
	"crypto"
	"encoding/json"
	"errors"
	"time"

	"gopkg.in/square/go-jose.v2"

	"go.nlx.io/nlx/manager/domain/contract"
)

const (
	typeAccept   = "accept"
	typeReject   = "reject"
	typeRevoke   = "revoke"
	jwsX5tHeader = "x5t#S256"
)

func CreateAccept(contentHash, certificateThumbprint string, privateKey crypto.PrivateKey, signedAt time.Time) (string, error) {
	return createRSA(typeAccept, contentHash, certificateThumbprint, privateKey, signedAt)
}

func CreateReject(contentHash, certificateThumbprint string, privateKey crypto.PrivateKey, signedAt time.Time) (string, error) {
	return createRSA(typeReject, contentHash, certificateThumbprint, privateKey, signedAt)
}

func CreateRevoke(contentHash, certificateThumbprint string, privateKey crypto.PrivateKey, signedAt time.Time) (string, error) {
	return createRSA(typeRevoke, contentHash, certificateThumbprint, privateKey, signedAt)
}

func CreateHMACAccept(contentHash, certificateThumbprint string, signedAt time.Time) (string, error) {
	return createHmac(typeAccept, contentHash, certificateThumbprint, signedAt)
}

func CreateHMACReject(contentHash, certificateThumbprint string, signedAt time.Time) (string, error) {
	return createHmac(typeReject, contentHash, certificateThumbprint, signedAt)
}

func CreateHMACRevoke(contentHash, certificateThumbprint string, signedAt time.Time) (string, error) {
	return createHmac(typeRevoke, contentHash, certificateThumbprint, signedAt)
}

func createRSA(signatureType, contentHash, certificateThumbprint string, privateKey crypto.PrivateKey, signedAt time.Time) (string, error) {
	options := &jose.SignerOptions{}
	options.WithHeader(jwsX5tHeader, certificateThumbprint)

	signer, err := jose.NewSigner(jose.SigningKey{
		Algorithm: jose.RS512,
		Key:       privateKey,
	}, options)
	if err != nil {
		return "", errors.Join(err, errors.New("failed to create RSA signer"))
	}

	return create(signatureType, contentHash, signer, signedAt)
}

func createHmac(signatureType, contentHash, certificateThumbprint string, signedAt time.Time) (string, error) {
	options := &jose.SignerOptions{}
	options.WithHeader(jwsX5tHeader, certificateThumbprint)

	signer, err := jose.NewSigner(jose.SigningKey{Algorithm: jose.HS256, Key: []byte("testkey")}, options)
	if err != nil {
		return "", errors.Join(err, errors.New("failed to create HMAC signer"))
	}

	return create(signatureType, contentHash, signer, signedAt)
}

func create(signatureType, contentHash string, signer jose.Signer, signedAt time.Time) (string, error) {
	payload := &contract.SignaturePayload{
		ContentHash: contentHash,
		SigType:     signatureType,
		SignedAt:    signedAt.Unix(),
	}

	payloadData, err := json.Marshal(payload)
	if err != nil {
		return "", errors.Join(err, errors.New("failed marshall content into json"))
	}

	jws, err := signer.Sign(payloadData)
	if err != nil {
		return "", errors.Join(err, errors.New("failed to sign"))
	}

	signatureJWS, err := jws.CompactSerialize()
	if err != nil {
		return "", errors.Join(err, errors.New("failed to serialize signature"))
	}

	return signatureJWS, nil
}
