// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

//nolint:dupl // this is a test
package testsignature_test

import (
	"crypto"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	testsignature "go.nlx.io/nlx/testing/signature"
)

func TestCreateRevoke(t *testing.T) {
	type args struct {
		contentHash           string
		certificateThumbprint string
		privateKey            crypto.PrivateKey
		signedAt              time.Time
	}

	tests := map[string]struct {
		args    args
		want    string
		wantErr bool
	}{
		"happy_flow": {
			args: args{
				contentHash:           "my-content-hash",
				certificateThumbprint: getCertBundle(t).CertificateThumbprint(),
				privateKey:            getCertBundle(t).Cert().PrivateKey,
				signedAt:              time.Date(2023, 1, 2, 3, 4, 5, 6, time.UTC),
			},
			want:    "eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2Ijoia3R4MVlPdHh4ZlU4Ykl6NExoM3pIaXBrSDFueFpYLWI3WWJuUFNBQzNIRSJ9.eyJjb250ZW50SGFzaCI6Im15LWNvbnRlbnQtaGFzaCIsInR5cGUiOiJyZXZva2UiLCJzaWduZWRBdCI6MTY3MjYyODY0NX0.BYE1liDBbI2mTFe2PW3SBCC72oHeszC9bGILc5fbUpyq7n1O5y8-jCPPlB3dCduNmR3DRd0mr2OsAkfnmoC-DsHWD2H4jkmQUKNUQPDp9HUcuWkPwdMbsZa3UQNU5OXjdOGo4oi82SGcYnEixTy8pa2kRZ7B-RyABeFFo1QN0Gk2oMLkVXX-gWTEbKhEsMBMiD5E-u8LcWV-zIsRpEibFyFs5Inug860kJfVWCgUhcJN55U-xv7OgK7S98ZxbiVAoUEWWJtlqPcOLGM7mxYEmhWznZa4lKaIUEcOgZjCysp0pcb_3BFma6x-9uzg14ZvRmRF6_PNldK_rXhOKRrvb3oFsHQydu4TBNu5eIhwEG_CruQ7r9EJA6rYNc-AzL65jC7uUF19UKaXml4HE58ZFatyjB0XkaOvKu-secOBtJiLQpPHhj21WGCG16foXLp-sfd2LsOPC4v_N2O7m6OHsJr7uXx3zEAlsdYuOhkPooZbYFLv7DspcA4BpjNpDuitisefctthpXGbn5G9E1YQM2vbaANhQNvZ2FF-pMt4umWJDV8m6TYgWZH1bHVZvdWbijtTTiotKRnByagHCPxMpRO9_8SWJslaI6yazkdTZX9Lo14L7r0g4rkJVuuyEyOOt6pN2nMp8AncSzIRODeMK2Y7b7JET85Wt2twpO6sCcM",
			wantErr: false,
		},
	}

	for name, tt := range tests {
		t.Run(name, func(t *testing.T) {
			got, err := testsignature.CreateRevoke(tt.args.contentHash, tt.args.certificateThumbprint, tt.args.privateKey, tt.args.signedAt)
			if (err != nil) != tt.wantErr {
				t.Errorf("CreateRevoke() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			assert.Equal(t, tt.want, got)
		})
	}
}
