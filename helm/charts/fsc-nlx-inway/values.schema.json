{
    "title": "Chart Values",
    "type": "object",
    "properties": {
        "global": {
            "type": "object",
            "properties": {
                "imageRegistry": {
                    "type": "string",
                    "description": "Global Docker Image registry",
                    "default": ""
                },
                "imageTag": {
                    "type": "string",
                    "description": "Global Docker Image tag",
                    "default": ""
                },
                "groupID": {
                    "type": "string",
                    "description": "Global FSC Group ID",
                    "default": ""
                },
                "certificates": {
                    "type": "object",
                    "properties": {
                        "group": {
                            "type": "object",
                            "properties": {
                                "caCertificatePEM": {
                                    "type": "string",
                                    "description": "Global FSC NLX CA root certificate. If not set the value of 'tls.organization.rootCertificatePEM' is used",
                                    "default": ""
                                }
                            }
                        },
                        "internal": {
                            "type": "object",
                            "properties": {
                                "caCertificatePEM": {
                                    "type": "string",
                                    "description": "Global root certificate of your internal PKI. If not set the value of 'tls.internal.rootCertificatePEM' is used",
                                    "default": ""
                                }
                            }
                        }
                    }
                }
            }
        },
        "image": {
            "type": "object",
            "properties": {
                "registry": {
                    "type": "string",
                    "description": "Image registry (ignored if 'global.imageRegistry' is set)",
                    "default": "docker.io"
                },
                "repository": {
                    "type": "string",
                    "description": "Image repository of the docs API.",
                    "default": "nlxio/fsc-inway"
                },
                "tag": {
                    "type": "string",
                    "description": "Image tag (ignored if 'global.imageTag' is set). When set to null, the AppVersion from the chart is used",
                    "default": ""
                },
                "pullPolicy": {
                    "type": "string",
                    "description": "Image pull policy",
                    "default": "Always"
                },
                "pullSecrets": {
                    "type": "array",
                    "description": "Secrets for the image repository",
                    "default": [],
                    "items": {}
                }
            }
        },
        "replicaCount": {
            "type": "number",
            "description": "Number of controller replicas",
            "default": 1
        },
        "serviceAccount": {
            "type": "object",
            "properties": {
                "create": {
                    "type": "boolean",
                    "description": "Specifies whether a service account should be created",
                    "default": true
                },
                "annotations": {
                    "type": "object",
                    "description": "Annotations to add to the service account",
                    "default": {}
                },
                "name": {
                    "type": "string",
                    "description": "The name of the service account to use. If not set and create is true, a name is generated using the fullname template",
                    "default": ""
                }
            }
        },
        "securityContext": {
            "type": "object",
            "properties": {
                "runAsNonRoot": {
                    "type": "boolean",
                    "description": "Run container as a non-root user",
                    "default": true
                },
                "runAsUser": {
                    "type": "number",
                    "description": "Run container as specified user",
                    "default": 1001
                },
                "capabilities": {
                    "type": "object",
                    "properties": {
                        "drop": {
                            "type": "array",
                            "description": "Drop all capabilities by default",
                            "default": [
                                "ALL"
                            ],
                            "items": {
                                "type": "string"
                            }
                        }
                    }
                }
            }
        },
        "podSecurityContext": {
            "type": "object",
            "properties": {
                "fsGroup": {
                    "type": "number",
                    "description": "GroupID under which the pod should be started",
                    "default": 1001
                }
            }
        },
        "resources": {
            "type": "object",
            "description": "Pod resource requests & limits",
            "default": {}
        },
        "nodeSelector": {
            "type": "object",
            "description": "Node labels for pod assignment",
            "default": {}
        },
        "affinity": {
            "type": "object",
            "description": "Node affinity for pod assignment",
            "default": {}
        },
        "tolerations": {
            "type": "array",
            "description": "Node tolerations for pod assignment",
            "default": [],
            "items": {}
        },
        "extraEnv": {
            "type": "array",
            "description": "Extra env items for pod assignment",
            "default": [],
            "items": {}
        },
        "nameOverride": {
            "type": "string",
            "description": "Override deployment name",
            "default": ""
        },
        "fullnameOverride": {
            "type": "string",
            "description": "Override full deployment name",
            "default": ""
        },
        "config": {
            "type": "object",
            "properties": {
                "logType": {
                    "type": "string",
                    "description": "Possible values 'live', 'local'. Affects the log output. See NewProduction and NewDevelopment at https://godoc.org/go.uber.org/zap#Logger",
                    "default": "live"
                },
                "logLevel": {
                    "type": "string",
                    "description": "Possible values 'debug', 'warn', 'info'. Override the default logLevel set by 'config.logType'",
                    "default": "info"
                },
                "groupID": {
                    "type": "string",
                    "description": "FSC Group ID",
                    "default": ""
                },
                "name": {
                    "type": "string",
                    "description": "Name of the Inway",
                    "default": ""
                },
                "selfAddress": {
                    "type": "string",
                    "description": "The Address that can be used by the FSC NLX network to reach this Inway",
                    "default": ""
                },
                "managerInternalUnauthenticatedAddress": {
                    "type": "string",
                    "description": "Internal unauthenticated address of the Manager",
                    "default": ""
                },
                "controllerApiAddress": {
                    "type": "string",
                    "description": "The address of the Controller API",
                    "default": ""
                },
                "transactionLogApiAddress": {
                    "type": "string",
                    "description": "The Address of the Transaction Log API",
                    "default": ""
                },
                "authorizationService": {
                    "type": "object",
                    "properties": {
                        "enabled": {
                            "type": "boolean",
                            "description": "If 'true', the Inway will use the authorization service",
                            "default": false
                        },
                        "url": {
                            "type": "string",
                            "description": "URL of the authorization service to use",
                            "default": ""
                        }
                    }
                }
            }
        },
        "certificates": {
            "type": "object",
            "properties": {
                "group": {
                    "type": "object",
                    "properties": {
                        "caCertificatePEM": {
                            "type": "string",
                            "description": "The CA certificate of the Group",
                            "default": ""
                        },
                        "certificatePEM": {
                            "type": "string",
                            "description": "The Group certificate",
                            "default": ""
                        },
                        "keyPEM": {
                            "type": "string",
                            "description": "Private Key of 'certificates.group.certificatePEM'",
                            "default": ""
                        },
                        "existingSecret": {
                            "type": "string",
                            "description": "Use existing secret with your FSC NLX keypair (`certificates.group.certificatePEM` and `certificates.group.keyPEM` will be ignored and picked up from the secret)",
                            "default": ""
                        }
                    }
                },
                "internal": {
                    "type": "object",
                    "properties": {
                        "caCertificatePEM": {
                            "type": "string",
                            "description": "The CA root certificate of your internal PKI",
                            "default": ""
                        },
                        "certificatePEM": {
                            "type": "string",
                            "description": "The certificate signed by your internal PKI",
                            "default": ""
                        },
                        "keyPEM": {
                            "type": "string",
                            "description": "the private key of 'certificates.internal.certificatePEM'",
                            "default": ""
                        },
                        "existingSecret": {
                            "type": "string",
                            "description": "Use of existing secret with your FSC NLX keypair ('certificates.internal.certificatePEM' and 'certificates.internal.keyPEM'. will be ingored and picked up from this secret)",
                            "default": ""
                        }
                    }
                },
                "serviceCA": {
                    "type": "object",
                    "properties": {
                        "certificatePEM": {
                            "type": "string",
                            "description": "If a service offered by this Inway uses certificates signed by your own CA you can use this value to mount the CA certificate on the Inway so the Inway is able to validate the TLS connection",
                            "default": ""
                        }
                    }
                }
            }
        },
        "service": {
            "type": "object",
            "properties": {
                "type": {
                    "type": "string",
                    "description": "Service Type (ClusterIP, NodePort, LoadBalancer)",
                    "default": "LoadBalancer"
                },
                "annotations": {
                    "type": "object",
                    "description": "Annotations to be included in the",
                    "default": {}
                },
                "port": {
                    "type": "number",
                    "description": "Port exposed by the service",
                    "default": 443
                },
                "nodePort": {
                    "type": "number",
                    "description": "Port exposed for Inway traffic if 'service.type' is 'NodePort'",
                    "default": 443
                },
                "loadBalancerIP": {
                    "type": "string",
                    "description": "Only applies when using 'service.type' 'LoadBalancer'. A loadBalancer will get created with the IP specified in this field. This feature depends on whether the underlying cloud-provider supports specifying the loadbalancerIP when a load balancer is created. This field will be ignored if the cloud-provider does not support the feature.",
                    "default": ""
                }
            }
        }
    }
}