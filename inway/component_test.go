/*
 * Copyright © VNG Realisatie 2023
 * Licensed under the EUPL
 *
 */

package inway_test

import (
	"context"
	"log"
	"net/http"
	"net/http/httptest"
	"net/url"
	"path/filepath"
	"testing"
	"time"

	"go.uber.org/zap"

	"go.nlx.io/nlx/common/clock"
	"go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/transactionlog"
	"go.nlx.io/nlx/inway"
	"go.nlx.io/nlx/inway/domain/config"
	"go.nlx.io/nlx/testing/testingutils"
)

var (
	orgACertBundle *tls.CertificateBundle
	orgBCertBundle *tls.CertificateBundle
	orgCCertBundle *tls.CertificateBundle
	testClock      = clock.NewMock(time.Date(2023, 11, 6, 14, 10, 5, 0, time.UTC))
)

const mockService = "mock-service"

func TestMain(m *testing.M) {
	var err error

	orgACertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestOrgA) //probably need another name since access token is
	if err != nil {
		log.Fatal(err)
	}

	orgBCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestOrgB)
	if err != nil {
		log.Fatal(err)
	}

	orgCCertBundle, err = testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestOrgC)
	if err != nil {
		log.Fatal(err)
	}

	m.Run()
}

func newInway(configRepository config.Repository, transactionLogger transactionlog.TransactionLogger) *httptest.Server {
	inwayURL, _ := url.Parse("https://localhost")

	if transactionLogger == nil {
		transactionLogger = newFakeTransactionLogger()
	}

	if configRepository == nil {
		configRepository = newFakeConfigRepository("")
	}

	testInway, err := inway.NewInway(&inway.Params{
		Context:                      context.Background(),
		Logger:                       zap.NewNop(),
		Txlogger:                     transactionLogger,
		Name:                         "testInway",
		GroupID:                      "fsc-local",
		Address:                      inwayURL,
		OrgCertBundle:                orgACertBundle,
		Controller:                   nil,
		AuthServiceURL:               "",
		AuthCAPath:                   "",
		ConfigRepository:             configRepository,
		ServiceEndpointCacheDuration: 0,
		ServiceProxyCacheSize:        100,
		Clock:                        testClock,
	})
	if err != nil {
		log.Fatal(err, "could not create new Inway from parameters")
	}

	srv := httptest.NewUnstartedServer(testInway.Handler())
	srv.TLS = orgACertBundle.TLSConfig(orgACertBundle.WithTLSClientAuth())
	srv.StartTLS()

	return srv
}

func createInwayAPIClient(certBundle *tls.CertificateBundle) *http.Client {
	transport := &http.Transport{
		TLSClientConfig: certBundle.TLSConfig(certBundle.WithTLSClientAuth()),
	}

	return &http.Client{
		Transport: transport,
	}
}
