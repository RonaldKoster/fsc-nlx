// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package config

import (
	"context"
	"net/url"

	"go.nlx.io/nlx/manager/domain/contract"
)

type Repository interface {
	GetServiceEndpointURL(ctx context.Context, serviceName string) (*url.URL, error)
	GetCertificates(ctx context.Context) (*contract.PeerCertificates, error)
}
