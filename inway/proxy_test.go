// Copyright © VNG Realisatie 2018
// Licensed under the EUPL

package inway_test

import (
	"crypto/x509"
	"encoding/json"
	"io"
	"log"
	"net/http"
	"net/http/httptest"
	"path/filepath"
	"testing"
	"time"

	"github.com/stretchr/testify/assert"

	"go.nlx.io/nlx/common/accesstoken"
	"go.nlx.io/nlx/common/httperrors"
	"go.nlx.io/nlx/common/tls"
	"go.nlx.io/nlx/common/transactionlog"
	"go.nlx.io/nlx/testing/testingutils"
	"go.nlx.io/nlx/txlog-api/domain/record"
)

const (
	mockPath              = "/" + mockService
	validFSCTransactionID = "018bb8c2-7fee-75a5-9618-4c5728647726"
	errorSource           = "inway"
	errorLocation         = "O1"
)

// FSC Test suite test cases: `Inway-Incoming-Request-1`, `Inway-Incoming-Request-5`, `Inway-TransactionLog-1`, `Inway-TransactionLog-2`
func TestHappyFlow(t *testing.T) {
	t.Parallel()

	// Arrange
	accessToken, err := generateValidToken()
	assert.NoError(t, err)

	var apiServerRequestReceivedAt time.Time
	testAPIServer := newMockAPI(t, &apiServerRequestReceivedAt, accessToken)

	defer testAPIServer.Close()

	configRepository := newFakeConfigRepository(testAPIServer.URL)
	transactionLogger := newFakeTransactionLogger()

	inwayServer := newInway(configRepository, transactionLogger)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	request := createValidRequest(inwayServer.URL + mockPath)

	// Act
	response, err := client.Do(request)
	assert.NoError(t, err)

	defer response.Body.Close()

	// Assert
	if !assert.Equal(t, http.StatusOK, response.StatusCode) {
		responseBody, err := io.ReadAll(response.Body)
		assert.NoError(t, err)

		t.Logf("response body: %s", responseBody)
	}

	records := transactionLogger.records
	assert.True(t, apiServerRequestReceivedAt.After(transactionLogger.time)) // The testAPIServer must be called after the TxLogAPI

	assert.Equal(t, 1, len(transactionLogger.records))
	assert.Equal(t, orgBCertBundle.GetOrganizationInfo().SerialNumber, records[0].Source.(*transactionlog.RecordSource).OutwayPeerID)
	assert.Equal(t, orgACertBundle.GetOrganizationInfo().SerialNumber, records[0].Destination.(*transactionlog.RecordDestination).ServicePeerID)
	assert.Equal(t, record.DirectionIn, records[0].Direction)
	assert.Equal(t, "mock-service", records[0].ServiceName)
	assert.Equal(t, validFSCTransactionID, records[0].TransactionID.String())
	assert.Equal(t, mockPath, records[0].Data["request-path"])
}

// FSC Test suite test cases: `Inway-TransactionLog-4`
//
//nolint:dupl // looks like  TestHappyFlow but uses different token and assertions
func TestInwayValidTokenDelegatedServiceConnection(t *testing.T) {
	t.Parallel()

	validDelegatedAccessToken, err := generateValidDelegationToken(orgCCertBundle.GetOrganizationInfo(), nil)
	assert.NoError(t, err)

	// Arrange
	var apiServerRequestReceivedAt time.Time
	testAPIServer := newMockAPI(t, &apiServerRequestReceivedAt, validDelegatedAccessToken)

	defer testAPIServer.Close()

	configRepository := newFakeConfigRepository(testAPIServer.URL)
	transactionLogger := newFakeTransactionLogger()

	inwayServer := newInway(configRepository, transactionLogger)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)
	request := createRequestWithCustomToken(inwayServer.URL+mockPath, validDelegatedAccessToken)

	// Act
	response, err := client.Do(request)
	assert.NoError(t, err)

	defer response.Body.Close()

	// Assert
	if !assert.Equal(t, http.StatusOK, response.StatusCode) {
		responseBody, err := io.ReadAll(response.Body)
		assert.NoError(t, err)

		t.Logf("response body: %s", responseBody)
	}

	records := transactionLogger.records
	assert.True(t, apiServerRequestReceivedAt.After(transactionLogger.time)) // The testAPIServer must be called after the TxLogAPI

	assert.Equal(t, 1, len(transactionLogger.records))
	assert.Equal(t, orgBCertBundle.GetOrganizationInfo().SerialNumber, records[0].Source.(*transactionlog.RecordDelegatedSource).OutwayPeerID)
	assert.Equal(t, orgCCertBundle.GetOrganizationInfo().SerialNumber, records[0].Source.(*transactionlog.RecordDelegatedSource).DelegatorPeerID)
	assert.Equal(t, orgACertBundle.GetOrganizationInfo().SerialNumber, records[0].Destination.(*transactionlog.RecordDestination).ServicePeerID)
	assert.Equal(t, record.DirectionIn, records[0].Direction)
	assert.Equal(t, "mock-service", records[0].ServiceName)
	assert.Equal(t, validFSCTransactionID, records[0].TransactionID.String())
	assert.Equal(t, mockPath, records[0].Data["request-path"])
}

// FSC Test suite test cases: `Inway-TransactionLog-5`
//
//nolint:dupl // looks like  TestInwayValidTokenDelegatedServiceConnection but uses different token and assertions
func TestInwayValidTokenDelegatedServicePublication(t *testing.T) {
	t.Parallel()

	validDelegatedAccessToken, err := generateValidDelegationToken(nil, orgACertBundle.GetOrganizationInfo())
	assert.NoError(t, err)

	// Arrange
	var apiServerRequestReceivedAt time.Time
	testAPIServer := newMockAPI(t, &apiServerRequestReceivedAt, validDelegatedAccessToken)

	defer testAPIServer.Close()

	configRepository := newFakeConfigRepository(testAPIServer.URL)
	transactionLogger := newFakeTransactionLogger()

	inwayServer := newInway(configRepository, transactionLogger)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)
	request := createRequestWithCustomToken(inwayServer.URL+mockPath, validDelegatedAccessToken)

	// Act
	response, err := client.Do(request)
	assert.NoError(t, err)

	defer response.Body.Close()

	// Assert
	if !assert.Equal(t, http.StatusOK, response.StatusCode) {
		responseBody, err := io.ReadAll(response.Body)
		assert.NoError(t, err)

		t.Logf("response body: %s", responseBody)
	}

	records := transactionLogger.records
	assert.True(t, apiServerRequestReceivedAt.After(transactionLogger.time)) // The testAPIServer must be called after the TxLogAPI

	assert.Equal(t, 1, len(transactionLogger.records))
	assert.Equal(t, orgBCertBundle.GetOrganizationInfo().SerialNumber, records[0].Source.(*transactionlog.RecordSource).OutwayPeerID)
	assert.Equal(t, orgACertBundle.GetOrganizationInfo().SerialNumber, records[0].Destination.(*transactionlog.RecordDelegatedDestination).ServicePeerID)
	assert.Equal(t, orgACertBundle.GetOrganizationInfo().SerialNumber, records[0].Destination.(*transactionlog.RecordDelegatedDestination).DelegatorPeerID)
	assert.Equal(t, record.DirectionIn, records[0].Direction)
	assert.Equal(t, "mock-service", records[0].ServiceName)
	assert.Equal(t, validFSCTransactionID, records[0].TransactionID.String())
	assert.Equal(t, mockPath, records[0].Data["request-path"])
}

// FSC Test suite test case: `Inway-Incoming-Request-2`
func TestInwayNoToken(t *testing.T) {
	t.Parallel()

	// Arrange
	testServer := newErrorMockAPI()
	defer testServer.Close()

	inwayServer := newInway(nil, nil)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest := createValidRequest(inwayServer.URL + mockPath)
	validRequest.Header.Del("Fsc-Authorization")

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.MissingAuthHeader()
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, "Bearer", response.Header.Get("WWW-Authenticate"))
	assert.Equal(t, httperrors.MissingAuthHeaderErr.String(), response.Header.Get("Fsc-Error-Code"))
}

// FSC Test suite test cases: `Inway-TransactionLog-3`
func TestInwayNoTransactionID(t *testing.T) {
	t.Parallel()

	// Arrange
	testServer := newErrorMockAPI()
	defer testServer.Close()

	configRepository := newFakeConfigRepository(testServer.URL)

	inwayServer := newInway(configRepository, nil)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest := createValidRequest(inwayServer.URL + mockPath)
	validRequest.Header.Del("Fsc-Transaction-Id")

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.MissingLogRecordID()
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.MissingLogRecordIDErr.String(), response.Header.Get("Fsc-Error-Code"))
}

func TestInwayInvalidTransactionID(t *testing.T) {
	t.Parallel()

	// Arrange
	testServer := newErrorMockAPI()
	defer testServer.Close()

	configRepository := newFakeConfigRepository(testServer.URL)

	inwayServer := newInway(configRepository, nil)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest := createValidRequest(inwayServer.URL + mockPath)
	validRequest.Header.Set("Fsc-Transaction-Id", "I am not valid")

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.InvalidLogRecordID()
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusBadRequest, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.InvalidLogRecordIDErr.String(), response.Header.Get("Fsc-Error-Code"))
}

// FSC test suite test case: `Inway-Incoming-Request-3`
//
//nolint:dupl // looks like TestInwayWronglySignedToken but uses different token and assertions
func TestInwayWronglyBoundToken(t *testing.T) {
	t.Parallel()

	invalidTokenArgs := &accesstoken.NewTokenArgs{
		GroupID:                     "fsc-local",
		GrantHash:                   "$1$4$5h6QgvF1IDMrGjnC1orUC2nbS2NfEkbROggi2q-Kcs_STXlOQcGXzbA_bdgwQqc6MJu_Xh2qGSDRJL_LoeEEAQ",
		OutwayPeerID:                orgBCertBundle.GetOrganizationInfo().SerialNumber,
		OutwayCertificateThumbprint: orgBCertBundle.CertificateThumbprint(),
		ServiceName:                 "mock-service",
		ServiceInwayAddress:         "https://inway.organization-a.nlx.local:443",
		ServicePeerID:               orgACertBundle.GetOrganizationInfo().SerialNumber,
		ExpiryDate:                  testClock.Now().Add(time.Hour),
		NotBefore:                   testClock.Now().Add(-time.Hour * 2),
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
			PrivateKey:            orgACertBundle.PrivateKey(),
		},
	}

	invalidToken, err := accesstoken.New(invalidTokenArgs)
	assert.NoError(t, err)

	testServer := newErrorMockAPI()
	defer testServer.Close()

	inwayServer := newInway(nil, nil)
	defer inwayServer.Close()

	requestingOrg := orgCCertBundle

	client := createInwayAPIClient(requestingOrg)

	validRequest := createRequestWithCustomToken(inwayServer.URL+mockPath, invalidToken.Value())

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.AccessDenied(requestingOrg.GetOrganizationInfo().SerialNumber, requestingOrg.CertificateThumbprint())
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusForbidden, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.AccessDeniedErr.String(), response.Header.Get("Fsc-Error-Code"))
}

// FSC test suite test case: `Inway-Incoming-Request-4`
//
//nolint:dupl // looks like TestInwayWronglyBoundToken but is different
func TestInwayWronglySignedToken(t *testing.T) {
	t.Parallel()

	// need to hardcode the token in order to force a difference between the x5t#S256 and the private key singing the token
	invalidToken := "eyJhbGciOiJSUzUxMiIsIng1dCNTMjU2IjoiTmtNZkJGSmxobGMyQUFfdXdlRldWWmt0TS10b1RFNDdybDVZbi1scVp1MCJ9.eyJnaWQiOiJmc2MtbG9jYWwiLCJndGgiOiIkMSQ0JDVoNlFndkYxSURNckdqbkMxb3JVQzJuYlMyTmZFa2JST2dnaTJxLUtjc19TVFhsT1FjR1h6YkFfYmRnd1FxYzZNSnVfWGgycUdTRFJKTF9Mb2VFRUFRIiwic3ViIjoiMDAwMDAwMDAwMDAwMDAwMDAwMDIiLCJzdmMiOiJtb2NrLXNlcnZpY2UiLCJhdWQiOiJodHRwczovL2lud2F5Lm9yZ2FuaXphdGlvbi1hLm5seC5sb2NhbDo0NDMiLCJpc3MiOiIwMDAwMDAwMDAwMDAwMDAwMDAwMSIsImV4cCI6MTY5OTI4MTU1MCwibmJmIjoxNjk5Mjc3OTUwLCJjbmYiOnsieDV0I1MyNTYiOiJWeTkyeFJsck8wdW1wZ2YxMGpBakcwU0JfSkw4V01FYnA1YXprZ2Naa3U4In19.hT6eyYNvXni2RYl0KcJth-FqTer6_PEuOP6EFeMWouQbKJezdR-iro77qYHThMQllNUU03eCynDjYjOLOCc9i7fVKHv_zXYlhM8ydw1EkxR0EGLCJfkwJaIxceFK7XDfzGsOdz8QQZnUEk8mtX7d6ybmAShlUMouP038nkU_PD8tDwv85mPwza0E50Sr-Yttz3Wg8VyiyKU7n0r-khOVDIadVY6MpoNuQHbsrIYgqiNPIqYevX96TlsO3jO3tkf2n7PtJHz2pOraGjiYrVNqnIu66eX1XbWx1sMhzRP0iV2ngVpz0kFoaMZeQ8iZyvwQj1b0jSijHXstcldOq8JmccWH22f461lPSuDEoRF_zjvmrfmv5ZqEzhGOSvZntlkaLNS8xc5y8Z_dtdqlau3UFwloQp2ZaENz8y9gaNdOclgXFlbAFddxNZ3IUGQgQe1X3G6o_GkAeVaSpPawjvZhHCQNxGH1si3NwiNfw6RN30od_AcjglAXpLB8oSDHt3k1pHLzv44fXZCSpgUG-xsk0xusPv9kfKclM9OPHXslqT1TT_Em7xoPy6jNGYONgVKK88MkyS1zd6H_iZKU90SzqxUnCnsQOydwwQ7NSVqyy_nEld8sbQyry3_ElF8fjf6AQQu4xJ9FmfLJ5aciAEp2f1CMy-_fgY9WamOW335qo_E" //nolint:gosec // test token

	testServer := newErrorMockAPI()
	defer testServer.Close()

	inwayServer := newInway(nil, nil)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest, err := http.NewRequest(http.MethodGet, inwayServer.URL+mockPath, http.NoBody)
	assert.Nil(t, err)

	validRequest.Header.Set("Fsc-Authorization", "Bearer"+invalidToken)

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.InvalidAuthorizationToken("could not verify jws token in signature: square/go-jose: error in cryptographic primitive")
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.InvalidAuthorizationTokenErr.String(), response.Header.Get("Fsc-Error-Code"))
}

// FSC test suite test case: `Inway-Incoming-Request-6`
//
//nolint:dupl // looks like TestInwayWronglySignedToken but is different
func TestExpiredToken(t *testing.T) {
	t.Parallel()

	expiredTokenArgs := &accesstoken.NewTokenArgs{
		GroupID:                     "fsc-local",
		GrantHash:                   "$1$4$5h6QgvF1IDMrGjnC1orUC2nbS2NfEkbROggi2q-Kcs_STXlOQcGXzbA_bdgwQqc6MJu_Xh2qGSDRJL_LoeEEAQ",
		OutwayPeerID:                orgBCertBundle.GetOrganizationInfo().SerialNumber,
		OutwayCertificateThumbprint: orgBCertBundle.CertificateThumbprint(),
		ServiceName:                 "mock-service",
		ServiceInwayAddress:         "https://inway.organization-a.nlx.local:443",
		ServicePeerID:               orgACertBundle.GetOrganizationInfo().SerialNumber,
		ExpiryDate:                  testClock.Now().Add(-time.Hour),
		NotBefore:                   testClock.Now().Add(-time.Hour * 2),
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
			PrivateKey:            orgACertBundle.PrivateKey(),
		},
	}

	expiredToken, err := accesstoken.New(expiredTokenArgs)
	assert.NoError(t, err)

	testServer := newErrorMockAPI()
	defer testServer.Close()

	inwayServer := newInway(nil, nil)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest := createRequestWithCustomToken(inwayServer.URL+mockPath, expiredToken.Value())
	validRequest.Header.Set("Fsc-Authorization", "Bearer"+expiredToken.Value())

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.ExpiredAuthorizationToken("invalid token, expired on: 2023-11-06 13:10:05 +0000 UTC")
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusUnauthorized, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, "Bearer", response.Header.Get("WWW-Authenticate"))
	assert.Equal(t, httperrors.ExpiredAuthorizationTokenErr.String(), response.Header.Get("Fsc-Error-Code"))
}

func TestLogWriteError(t *testing.T) {
	t.Parallel()

	accessToken, err := generateValidToken()
	assert.NoError(t, err)

	testServer := newErrorMockAPI()
	defer testServer.Close()

	inwayServer := newInway(nil, &errorTransactionLogger{})
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	validRequest := createRequestWithCustomToken(inwayServer.URL+mockPath, accessToken)

	// Act
	response, err := client.Do(validRequest)
	assert.NoError(t, err)

	defer response.Body.Close()

	responseBody, err := io.ReadAll(response.Body)
	assert.NoError(t, err)

	gotError := &httperrors.FSCNetworkError{}
	err = json.Unmarshal(responseBody, gotError)
	assert.NoError(t, err)

	expectedError := httperrors.LogRecordWriteError()
	expectedError.Location = errorLocation
	expectedError.Source = errorSource

	// Assert
	assert.Equal(t, http.StatusInternalServerError, response.StatusCode)
	assert.EqualExportedValues(t, *expectedError, *gotError)
	assert.Equal(t, httperrors.LogRecordWriteErr.String(), response.Header.Get("Fsc-Error-Code"))
}

// test invalid client certificate
func TestInwayInvalidClientCert(t *testing.T) {
	t.Parallel()

	inwayServer := newInway(nil, nil)
	defer inwayServer.Close()

	invalidCertBundle, err := testingutils.GetCertificateBundle(filepath.Join("..", "testing", "pki"), testingutils.NLXTestInternal)
	if err != nil {
		log.Panic(err)
	}

	client := createInwayAPIClient(invalidCertBundle)

	request := createValidRequest(inwayServer.URL + mockPath)

	// Act
	_, err = client.Do(request) //nolint:bodyclose // there is no body to close

	// Assert
	wantErr := x509.UnknownAuthorityError{}
	assert.ErrorAs(t, err, &wantErr)
	assert.ErrorContains(t, err, "tls: failed to verify certificate: x509: certificate signed by unknown authority")
}

func TestRequestPath(t *testing.T) {
	t.Parallel()

	// Arrange
	testAPIReceivedURLPaths := []string{}

	testAPIServer := httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			testAPIReceivedURLPaths = append(testAPIReceivedURLPaths, r.URL.Path)
		}))

	defer testAPIServer.Close()

	serviceEndpointURL := testAPIServer.URL + "/api"

	configRepository := newFakeConfigRepository(serviceEndpointURL)
	transactionLogger := newFakeTransactionLogger()

	inwayServer := newInway(configRepository, transactionLogger)
	defer inwayServer.Close()

	client := createInwayAPIClient(orgBCertBundle)

	requests := []*http.Request{
		createValidRequest(inwayServer.URL),
		createValidRequest(inwayServer.URL + "/"),
		createValidRequest(inwayServer.URL + "/foobar"),
	}

	// Act
	for _, request := range requests {
		response, err := client.Do(request)
		assert.NoError(t, err)

		err = response.Body.Close()
		assert.NoError(t, err)
	}

	// Assert
	assert.Equal(t, []string{"/api/", "/api/", "/api/foobar"}, testAPIReceivedURLPaths)
}

func newMockAPI(t *testing.T, apiServerRequestReceivedAt *time.Time, accessToken string) *httptest.Server {
	return httptest.NewServer(
		http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			*apiServerRequestReceivedAt = time.Now()
			w.WriteHeader(http.StatusOK)
			assert.Equal(t, mockPath, r.URL.Path)
			assert.Equal(t, "Bearer "+accessToken, r.Header.Get("Fsc-Authorization"))
			assert.Equal(t, validFSCTransactionID, r.Header.Get("Fsc-Transaction-Id"))
			assert.Equal(t, orgBCertBundle.GetOrganizationInfo().SerialNumber, r.Header.Get("Fsc-Request-Peer-Id"))
		}))
}

// Certain test scenarios mandate the test server not to be called. This is forced by throwing an error
func newErrorMockAPI() *httptest.Server {
	return httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		log.Fatal("this test server should not be called")
	}))
}

func createValidRequest(url string) *http.Request {
	accessToken, err := generateValidToken()
	if err != nil {
		log.Fatal(err)
	}

	return createRequestWithCustomToken(url, accessToken)
}

func createRequestWithCustomToken(url, accessToken string) *http.Request {
	request, err := http.NewRequest(http.MethodGet, url, http.NoBody)
	if err != nil {
		log.Fatal(err)
	}

	request.Header.Set("Fsc-Authorization", "Bearer "+accessToken)
	request.Header.Set("Fsc-Transaction-Id", validFSCTransactionID)

	return request
}

func generateValidToken() (string, error) {
	return generateValidDelegationToken(nil, nil)
}

func generateValidDelegationToken(outwayDelegator, serviceDelegator *tls.OrganizationInformation) (string, error) {
	tokenArgs := &accesstoken.NewTokenArgs{
		GroupID:                     "fsc-local",
		GrantHash:                   "$1$4$5h6QgvF1IDMrGjnC1orUC2nbS2NfEkbROggi2q-Kcs_STXlOQcGXzbA_bdgwQqc6MJu_Xh2qGSDRJL_LoeEEAQ",
		OutwayPeerID:                orgBCertBundle.GetOrganizationInfo().SerialNumber,
		OutwayCertificateThumbprint: orgBCertBundle.CertificateThumbprint(),
		ServiceName:                 "mock-service",
		ServiceInwayAddress:         "https://inway.organization-a.nlx.local:443",
		ServicePeerID:               orgACertBundle.GetOrganizationInfo().SerialNumber,
		ExpiryDate:                  testClock.Now().Add(time.Minute),
		NotBefore:                   testClock.Now().Add(-time.Second),
		SignWith: &accesstoken.SignWith{
			CertificateThumbprint: orgACertBundle.CertificateThumbprint(),
			PrivateKey:            orgACertBundle.PrivateKey(),
		},
	}
	if outwayDelegator != nil {
		tokenArgs.OutwayDelegatorPeerID = outwayDelegator.SerialNumber
	}

	if serviceDelegator != nil {
		tokenArgs.ServiceDelegatorPeerID = serviceDelegator.SerialNumber
	}

	signedToken, err := accesstoken.New(tokenArgs)
	if err != nil {
		return "", err
	}

	return signedToken.Value(), nil
}
