// Copyright © VNG Realisatie 2023
// Licensed under the EUPL

package inway_test

import (
	"context"
	"errors"
	"time"

	"go.nlx.io/nlx/common/transactionlog"
)

type fakeTransactionLogger struct {
	records []*transactionlog.Record
	time    time.Time
}

func newFakeTransactionLogger() *fakeTransactionLogger {
	return &fakeTransactionLogger{}
}

func (m *fakeTransactionLogger) AddRecords(_ context.Context, records []*transactionlog.Record) error {
	m.records = append(m.records, records...)
	m.time = time.Now()

	return nil
}

func (m *fakeTransactionLogger) Close() error {
	return nil
}

type errorTransactionLogger struct{}

func (m *errorTransactionLogger) AddRecords(_ context.Context, records []*transactionlog.Record) error {
	return errors.New("cannot write record to transaction log")
}

func (m *errorTransactionLogger) Close() error {
	return nil
}
